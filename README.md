# Quality Tags Rewrite
## Iteration 3

[![License: Apache-2.0](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

#### Contents

- [SMTP client configuration](#markdown-header-smtp-client-settings)
- [Database configuration](#markdown-header-configuring-the-database)
- [Initializing user accounts and roles](#markdown-header-user-accounts)
- [Deploying with Docker](#markdown-header-deployment)

#### New in this iteration 

- customized user authentication rules
- user account management without leaving the app
- tag reports that can be edited
- the web container has its own `postfix` mail transfer agent
- integration test suite (partial coverage at this point)

### SMTP client settings

- use the same property names as below (see [Startup.cs](HoldTagSignOff/Startup.cs), about line 113)

```json
  "EmailConfiguration": {
    "SmtpServer": /* SMTP_DOMAIN */,
    "SmtpPort": 25 | 587 | 465 ,
    "SmtpUsername": /* ACCOUNT_EMAIL_ADDRESS */,
    "SmtpPassword": /* ACCOUNT_PASSWORD | API_KEY */,
    "MailerName": /* NAME_TO_PUT_ON_OUTGOING_MAIL" */,
	"MailerAddress": /* EMAIL_TO_SEND_FROM */
  }
```

#### How many parameters will you have to set?

It depends on the kind of relay your mailer uses for delivery:

|              |  e.g.                     |SmtpServer|SmtpPort|MailerName|MailerAddress|SmtpUserName|SmtpPassword|
|--------------|---------------------------|:--------:|:------:|:--------:|:-----------:|:----------:|:----------:|
| local relay  |`sendmail`,`exim`,`postfix`| X        | X      | X        | X           |            |            |
| remote relay |`smtp.gmail.com`           | X        | X      | X        |             | X          | X          |

Basically, if your mail agent stores credentials on the host machine, ignore `SmtpUserName` and `SmtpPassword`.

If using a third-party e-mail service, provide your account e-mail and password as `SmtpUserName` and `SmtpPassword`, respectively.

##### Caution
> If using a remote relay, your `SmtpUserName` **must** be a valid e-mail address. The mailer will prefer this to any ``MailerAddress` you provided.

Put all configuration values together in your `appsettings.json` (place it in main project folder: `HoldTagSignOff/`):

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=quality_tags_external;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360",
    "IdentityStoresConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=quality_tags_users;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360"
  },
  "EmailConfiguration": {
    "SmtpServer": /* SMTP_DOMAIN */,
    "SmtpPort": 25 | 587 | 465 ,
    "SmtpUsername": /* ACCOUNT_EMAIL_ADDRESS */,
    "SmtpPassword": /* ACCOUNT_PASSWORD | API_KEY */,
	"MailerName": /* NAME_TO_PUT_ON_OUTGOING_MAIL" */,
	"MailerAddress": /* EMAIL_TO_SEND_FROM */
  },
	"Logging": {
		"IncludeScopes": false,
		"LogLevel": {
			"Default": "Information",
			"Microsoft": "Warning",
			"Microsoft.Hosting.Lifetime": "Information"
		}
	},
  "AllowedHosts": "*"
}
```

## Deployment

- provide valid [MySQL environment variables](Docker/FILL_ME_IN.env); save these to a `.env` file
- if your firewall is running, open the port the `db` service will listen on. At the moment, this is `13306` (modify as needed):

```bash
sudo ufw allow 13306
sudo ufw reload
```

- make sure you're in the **repository** root, i.e. 

```
total 44
drwxrwx--- 1 root vboxsf  4096 Apr 13 07:37 Docker
drwxrwx--- 1 root vboxsf  4096 Apr 13 07:20 docs
drwxrwx--- 1 root vboxsf  8192 Apr 13 07:29 HoldTagSignOff
drwxrwx--- 1 root vboxsf  4096 Apr 11 21:36 HoldTagSignOff.Tests
-rwxrwx--- 1 root vboxsf 11558 Mar 11 07:10 LICENSE.txt
-rwxrwx--- 1 root vboxsf   301 Apr 12 07:10 NOTICE.txt
-rwxrwx--- 1 root vboxsf  7305 Apr 13 07:34 README.md
```

- start the services 

```
DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose -f Docker/compose.yml up

```

### Run tests

Start the services as above, then run

```
docker exec -it quality-tags-dev-host bash -c "cd HoldTagSignOff && dotnet test"
```

## Configuring the database

#### Connection strings

**Notes.**

- Depending on your set-up, the `DOCKER_HOST_ADDRESS` may be your machine's LAN IP
- Use the **same** property names as below (they must match the keys found in [Startup.cs](HoldTagSignOff/Startup.cs)):

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=hold_tags_signoff;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360",
		
	"IdentityStoresConnection": "Server=/* DOCKER_HOST_ADDRESS */;port=13306;user=/* MYSQL_USER */;password=/* MYSQL_PASSWORD */;database=quality_tags_users;Convert Zero Datetime=True;Allow Zero Datetime=True;TreatTinyAsBoolean=True;Default Command Timeout=360"
  }
}
```

- go to `DOCKER_HOST_ADDRESS:6117`
- log in to server `db` as `MYSQL_ROOT_USER` using `MYSQL_ROOT_PASSWORD`
- import the [schemas and data](HoldTagSignOff/Data/hold_tags_signoff.sql) by file upload
- if you prefer a standard user to `root`,  execute:

```mysql
GRANT SELECT, INSERT, UPDATE, DELETE, INDEX, REFERENCES ON `hold_tags_signoff`.* TO 'MYSQL_USER'@'%';
GRANT SELECT, INSERT, UPDATE, DELETE, INDEX, REFERENCES, CREATE ON `quality_tags_users`.* TO 'MYSQL_USER'@'%';
FLUSH PRIVILEGES;
SHOW GRANTS FOR 'MYSQL_USER'@'%';
```

## User accounts

- go to `DOCKER_HOST_ADDRESS` and register as a new user

- when prompted, click **Apply Migrations**:

![auto-migration-screen](docs/img/first-login.png)

- refresh the page and you should be redirected to the following:

![registered](docs/img/new-registration-screen.png)

- if you've set up the SMTP client, the e-mail should reach you user account's email:

![](docs/img/new-registration-email-received.png)

**Note**. If you own the SMTP server, we recommend looking in the *Sent* folder for your confirmation

- the provided link returns you to the site:


![](docs/img/new-registration-confirmed.png)

#### Failsafe

If your SMTP client doesn't work, or you're just impatient, you can inspect the source of the for a hidden copy of the link. This is made available for demonstration purposes only.

### Logging in

- return to `DOCKER_HOST_ADDRESS:6117`
- create the user roles and add the first user (i.e. yourself) to the `admin` role by uploading or executing [the provided script](HoldTagSignOff/Data/create_roles.sql):

- if you're an admin user, you should see the full complement of links:

![logged-in-user](docs/img/all-access.png)

- all other users will be denied the **Site Administration** heading


### Documentation
Domain classes and some controller actions are documented [here](docs/README.md).