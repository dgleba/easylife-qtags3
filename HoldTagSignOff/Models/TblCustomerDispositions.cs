﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblCustomerDispositions
    {
        public int Id { get; set; }
        public string Disposition { get; set; }
    }
}
