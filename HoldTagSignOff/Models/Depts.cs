﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class Depts
    {
        public int DeptId { get; set; }
        public string Department { get; set; }
        public string DepartmentState { get; set; }
    }
}
