﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblPlantNames
    {
        public int PlantId { get; set; }
        public string PlantName { get; set; }
        public string PlantDescrpition { get; set; }
        public int? Area { get; set; }
    }
}
