﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblProblemSource
    {
        public string ProblemSourceCode { get; set; }
        public string ProblemSource { get; set; }
    }
}
