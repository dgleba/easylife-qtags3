﻿/**
 * OpenHoldTagMetaData.cs
 * 
 * @brief 
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.12: Created
 */

using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static System.String;

namespace HoldTagSignOff.Models
{
    [ModelMetadataType(typeof(OpenHoldTagMetaData))]
    public partial class OpenHoldTag : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!IsNullOrEmpty(UseAsIsBy?.Trim()) || UseAsIsAmt.GetValueOrDefault() > 0
                && IsNullOrEmpty(Comment?.Trim()))
            {
                yield return new ValidationResult(
                "If the parts are OK, you must state the reason why in the Comment field.",
                new[] { nameof(Comment) });
            }

            yield return ValidationResult.Success;
        }
    }
    public class OpenHoldTagMetaData
    {
        [Required(ErrorMessage = "Please select a tag.")]
        [Display(Name = "Tag Number")]
        public int? TagNumber { get => TagNumber; set => TagNumber = value; }

        [Required(ErrorMessage = "You must give the date when the tag was closed.")]
        [Display(Name = "Date Closed")]
        [DataType(DataType.Date)]
        public DateTime? DateClosed { get => DateClosed; set => DateClosed = value; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public int? SortAmt { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public int? ReworkAmt { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public int? ScrapAmt { get; set; }

        [Range(0, Int32.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public int? UseAsIsAmt { get; set; }

        [Display(Name = "Quantity")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public int? Qty { get; set; }
    }
}
