﻿/**
 * CustomerDispositionsMetadata.cs
 * 
 * Useful properties for faster queries on the Customer Dispositions table
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.18: Created
 */

using System.Linq;

namespace HoldTagSignOff.Models
{
    public partial class TblCustomerDispositions
    {
        public readonly int[] STACKPOLE_DISPOSITION_IDS = Enumerable.Range(1, 6).ToArray();
        public readonly int[] CUSTOMER_DISPOSITION_IDS = Enumerable.Range(1, 23).ToArray();
    }
}
