﻿/**
 * QualityIssuesMetadata.cs
 * 
 * Annotations, display formatters and validations for Quality Tag object fields
 *
 * Revision History
 *     Robert Di Pardo, 2020.02.28: Created
 *     " ",             2020.03.14:  - Implemented validation interface
 *                                   - Forced all date information to be read from the `Date` field
 */

using Microsoft.AspNetCore.Mvc;
using HoldTagSignOff.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static System.String;

namespace HoldTagSignOff.Models
{
    [ModelMetadataType(typeof(QualityIssuesMetadata))]
    public partial class TblQualityIssues : IValidatableObject
    {
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            #region Validate universally required fields
            if (IsNullOrEmpty(PartId?.Trim()))
            {
                yield return new ValidationResult(
                    "Please select a part number!",
                    new[] { nameof(PartId) });
            }

            if (IsNullOrEmpty(Reason?.Trim()))
            {
                yield return new ValidationResult(
                    "Please select a reason!",
                    new[] { nameof(Reason) });
            }

            if (IsNullOrEmpty(IssuedBy?.Trim()))
            {
                yield return new ValidationResult(
                    "Please fill in 'Issued By'!",
                    new[] { nameof(IssuedBy) });
            }
            #endregion

            #region Validate customer issue fields
            if (IsExternalQualityAlert && IsNullOrEmpty(DispositionCustomer?.Trim()))
            {
                yield return new ValidationResult(
                    "Please select a disposition at customer!",
                    new[] { nameof(DispositionCustomer) });
            }

            if (IsNullOrEmpty(Operation?.Trim())
                && (IsInternalAlert
                    || IsExternalHoldTag
                    || IsExternalTPCTag
                    || IsExternalSpecialInstruction))
            {
                yield return new ValidationResult(
                    "Please select a process step!",
                    new[] { nameof(Operation) });
            }
            #endregion

            yield return ValidationResult.Success;
        }

        #region Properties for View display 
        public string LongId => $"{ProblemType} {Date.ToString("yy")} {Id}";
        public DateTime ExpiresOn => Date.AddDays(LengthOfChange > 0 ? LengthOfChange : 1.0).Date;

        /// <summary>
        /// Returns <see langword="true"/> if this tag if has a <see cref="ProblemType"/> of "EX", or false if:
        /// 1) <see cref="ProblemType"/> is something else; 
        /// 2) <see cref="ProblemType"/> is <see langword="null"/> .
        /// </summary>
        public bool IsExternalAlert => (ProblemType?.Equals("EX", StringComparison.OrdinalIgnoreCase)).GetValueOrDefault();

        /// <summary>
        /// Returns <see langword="true"/> if this tag if has a <see cref="ProblemType"/> of "IN", or false if:
        /// 1) <see cref="ProblemType"/> is something else; 
        /// 2) <see cref="ProblemType"/> is <see langword="null"/> .
        /// </summary>
        public bool IsInternalAlert => (ProblemType?.Equals("IN", StringComparison.OrdinalIgnoreCase)).GetValueOrDefault();
        #endregion

        #region Properties for model validation (reflecting form input state)
        private bool IsExternalQualityAlert => IsExternalAlert && QualityAtag.GetValueOrDefault();

        private bool IsExternalHoldTag => IsExternalAlert && HoldTag.GetValueOrDefault();

        private bool IsExternalTPCTag => IsExternalAlert && TpcTag.GetValueOrDefault();

        private bool IsExternalSpecialInstruction => IsExternalAlert && SpecialInstWritten.GetValueOrDefault();
        #endregion
    }

    public class QualityIssuesMetadata
    {
        private DateTime _creationDate;
        private DateTime? _issueDate;

        [Display(Name = "Tag Number")]
        public string LongId { get; }

        [Display(Name = "Part Number")]
        [Required(ErrorMessage = "Please select a part number!")]
        public string PartId { get; set; }

        [Display(Name = "Reason")]
        [Required(ErrorMessage = "Please select a reason!")]
        public string Reason { get; set; }

        [Display(Name = "Issued By")]
        [Required(ErrorMessage = "Please fill in 'Issued By'!")]
        public string IssuedBy { get; set; }

        [Display(Name = "Process Step")]
        public string Operation { get; set; }

        [Display(Name = "Machine Number")]
        public int? MachineId { get; set; }

        [Display(Name = "Date/Time Issued")]
        [DataType(DataType.Date)]
        public DateTime Date
        {
            get => _issueDate ?? _creationDate;
            set { _creationDate = value; }
        }

        [Display(Name = "Date Issued")]
        [DataType(DataType.Date)]
        public DateTime? DateIssued { set => _issueDate = value; }

        [Display(Name = "Length of Change")]
        [Range(1.0, Double.MaxValue, ErrorMessage = "Length of change can't be less than 1!")]
        public double LengthOfChange { get; set; }

        [Display(Name = "Expires On")]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime ExpiresOn { get; }

        [Display(Name = "Customer")]
        public int? CustomerId { get; set; }

        [Display(Name = "Est. Cost")]
        [DataType(DataType.Currency)]
        [Range(0.0, Double.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public decimal? Cost { get; set; }

        [Display(Name = "Quantity")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public int? Qty { get; set; }

        [Display(Name = "# of Pieces")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Must be a positive amount!")]
        public int? NofPiecesQa { get; set; }

        [Display(Name = "Additional Information")]
        public string Body { get; set; }

        [Display(Name = "Quality Alert")]
        public bool? QualityAtag { get; set; }

        [Display(Name = "Hold Tag")]
        public bool? HoldTag { get; set; }

        [Display(Name = "TPC Tag")]
        public bool? TpcTag { get; set; }

        [Display(Name = "Special Instruction")]
        public bool? SpecialInstWritten { get; set; }

        [Display(Name = "Quality Alert - Internal")]
        public bool? QualityMemo { get; set; }

        [Display(Name = "Mod Written")]
        public bool? ModWritten { get; set; }

        [Display(Name = "Certified Material Tag Issued")]
        public bool? CertTag { get; set; }

        [Display(Name = "Closed?")]
        public bool? Closed { get; set; }

        [Display(Name = "Disposition Customer")]
        public string DispositionCustomer { get; set; }

        [Display(Name = "Disposition Stackpole")]
        public string DispositionStackpole { get; set; }

        [Display(Name = "Customer Ref. Number")]
        public string CustomerRefNum { get; set; }

        [Display(Name = "Supplier Issue")]
        public bool? SupplierIssue { get; set; }

        [Display(Name = "Control Plan")]
        public bool? ControlPlan { get; set; }

        [Display(Name = "Layered Audit")]
        public bool? LayeredAudit { get; set; }

        [Display(Name = "Approved By")]
        public string OkdBy { get; set; }
    }
}
