﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblPartsCustomer
    {
        public int PartCustId { get; set; }
        public string PartId { get; set; }
        public int? CustId { get; set; }
    }
}
