﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblHtpcReasons
    {
        public int Id { get; set; }
        public string Reason { get; set; }
    }
}
