﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class Attachments
    {
        public int Id { get; set; }
        public int Path { get; set; }
        public string Key { get; set; }
        public string Filename { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual StorageLocations PathNavigation { get; set; }
    }
}
