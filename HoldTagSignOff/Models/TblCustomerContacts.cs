﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblCustomerContacts
    {
        public int CustContId { get; set; }
        public int? CustomerId { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string PhoneNumber { get; set; }
        public string CellNumber { get; set; }
        public string ContactEmail { get; set; }
    }
}
