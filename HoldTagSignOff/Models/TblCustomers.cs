﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblCustomers
    {
        public int CustomerId { get; set; }
        public string CustName { get; set; }
        public string CustLocation { get; set; }
        public bool? Current { get; set; }
    }
}
