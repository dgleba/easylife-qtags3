﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblCustomerSorters
    {
        public int CustSortId { get; set; }
        public int? CustomerId { get; set; }
        public string SortCompanyName { get; set; }
        public string SortContactFirstName { get; set; }
        public string SortContactLastName { get; set; }
        public string SortPhoneNumber { get; set; }
        public string SortEmail { get; set; }
    }
}
