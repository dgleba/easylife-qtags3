﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class MailingList
    {
        public int MailingListId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
    }
}
