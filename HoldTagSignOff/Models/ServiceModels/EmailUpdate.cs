﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HoldTagSignOff.Models
{
    public class EmailUpdate : IValidatableObject
    {
        [Required]
        [EmailAddress]
        [Display(Name = "New Email")]
        [RegularExpression(@"^.+\@\w+(\.\w{2,})+$", ErrorMessage = "Invalid e-mail address.")]
        public string NewEmail { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            if (string.IsNullOrEmpty(NewEmail?.Trim()))
            {
                yield return new ValidationResult("Please enter an e-mail address.",
                    new[] { nameof(NewEmail) });
            }

            yield return ValidationResult.Success;
        }
    }
}