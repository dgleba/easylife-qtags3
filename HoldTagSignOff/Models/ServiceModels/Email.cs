﻿/**
 * Email.cs
 *
 * Encapsulates e-mail message data.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.13: Created
 */

using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public class Email
    {
        public Email()
        {
            RecipientList = new List<string>();
            SenderList = new List<string>();
        }

        public List<string> RecipientList { get; set; }
        public List<string> SenderList { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}
