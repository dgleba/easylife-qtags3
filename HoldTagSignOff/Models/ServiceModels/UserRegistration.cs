﻿/**
 * UserRegistration.cs
 * 
 * Encapsulates a user's registration details.
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.02: Created
 *     "    ",          2020.04.08: Added validation logic
 */

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HoldTagSignOff.Models
{
    /// <summary>
    /// Summary description for Class1
    /// </summary>
    public class UserRegistration : IValidatableObject
    {
        [Required]
        [Display(Name = "User Name")]
        [StringLength(30)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        [RegularExpression(@"^.+\@\w+(\.\w{2,})+$", ErrorMessage = "Invalid e-mail address.")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [StringLength(30)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [StringLength(30)]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            string userName = UserName?.Trim();
            string email = Email?.Trim();
            string password = Password?.Trim();
            string confirmPassword = ConfirmPassword?.Trim();

            if (string.IsNullOrEmpty(userName) || userName.Length > 30)
            {
                yield return new ValidationResult("User name must be 1-30 characters.",
                    new[] { nameof(UserName) });
            }

            if (string.IsNullOrEmpty(email))
            {
                yield return new ValidationResult("Please enter an e-mail address.",
                    new[] { nameof(Email) });
            }

            if (string.IsNullOrEmpty(password) || password.Length > 30)
            {
                yield return new ValidationResult("Passwords must be 1-30 characters.",
                    new[] { nameof(Password) });
            }

            if (string.IsNullOrEmpty(confirmPassword) || confirmPassword.Length > 30)
            {
                yield return new ValidationResult("Passwords must be 1-30 characters.",
                    new[] { nameof(ConfirmPassword) });
            }

            yield return ValidationResult.Success;
        }
    }
}