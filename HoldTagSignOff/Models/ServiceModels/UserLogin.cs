﻿/**
 * UserLogin.cs
 * 
 * @brief 
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.02: Created
 */

using System.ComponentModel.DataAnnotations;

namespace HoldTagSignOff.Models
{
    public class UserLogin
    {
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
