﻿/**
 * EmailAddress.cs
 *
 * Encapsulates the details of an e-mail sender or recipient.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.13: Created
 */
 
namespace HoldTagSignOff.Models
{
    public class EmailAddress
    {
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
