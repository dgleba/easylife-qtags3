﻿/**
 * CustomerInfo.cs
 * 
 * Encapsulates a listing of customers and associated contacts.
 *
 * Revision History
 *     Xinghua Li, 2020.04.09: Created
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HoldTagSignOff.Models
{
    public class CustomerInfo
    {
        [Display(Name = "Contact Name")]
        public string ContactName => $"{ContactFirstName} {ContactLastName}";
        [Display(Name = "Phone")]
        public string ContactPhoneNumber { get; set; }
        [Display(Name = "Cell")]
        public string ContactCellNumber { get; set; }
        public int CustomerId { get; set; }
        public string CustName { get; set; }
        public string CustLocation { get; set; }
        public bool? Current { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }

        public string SalesRepFirstName { get; set; }
        public string SalesRepLastName { get; set; }
        public string SalesRepPhoneNumber { get; set; }
        public string SortCompanyName { get; set; }
        public string SortContactFirstName { get; set; }
        public string SortContactLastName { get; set; }
        public string SortPhoneNumber { get; set; }
    }
}
