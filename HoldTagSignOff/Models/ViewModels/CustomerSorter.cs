﻿/**
 * CustSorter.cs
 * 
 * Sorting list of customers
 * 
 * Revision History:
 * Bao Khanh Nguyen, 2020.04.10: Created
 * 
 */

namespace HoldTagSignOff.Models
{
    public class CustSorter
    {
        public string SorterName => $"{SortContactFirstName} {SortContactLastName}";
        public int CustomerId { get; set; }
        public string CustName { get; set; }
        public string CustLocation { get; set; }
        public bool? Current { get; set; }
        public string SortCompanyName { get; set; }
        public string SortContactFirstName { get; set; }
        public string SortContactLastName { get; set; }
        public string SortPhoneNumber { get; set; }
    }
}
