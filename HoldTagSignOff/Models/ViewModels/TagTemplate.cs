﻿/**
 * TagTemplate.cs
 * 
 * Enumerates tag categories for simpler template selection
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.16: Created
 */

namespace HoldTagSignOff.Models
{
    public enum TagTemplate
    {
        None,
        QualityAlertInternal,
        QualityAlertExternal,
        Hold,
        TPC,
        Special
    }
}
