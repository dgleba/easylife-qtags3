﻿/**
 * TagReport.cs
 * 
 * Extends the tag model with extra display properties.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.14: Created
 */

using Microsoft.AspNetCore.Html;
using static System.String;

namespace HoldTagSignOff.Models
{
    public class TagReport : TblQualityIssues
    {
        public TagReport(TblQualityIssues tag)
        {
            Id = tag.Id;
            Date = tag.Date;
            DateIssued = tag.DateIssued;
            ProblemType = tag.ProblemType;
            LengthOfChange = tag.LengthOfChange;
            PartId = tag.PartId;
            MachineId = tag.MachineId;
            Reason = tag.Reason;
            Operation = tag.Operation;
            IssuedBy = tag.IssuedBy;
            OkdBy = tag.OkdBy;
            CustomerRefNum = tag.CustomerRefNum;
            DispositionCustomer = tag.DispositionCustomer;
            DispositionStackpole = tag.DispositionStackpole;
            Body = tag.Body;
            Feature = tag.Feature;
            Comment = tag.Comment;
            Cost = tag.Cost;
            Qty = tag.Qty;
            QualityAtag = tag.QualityAtag;
            HoldTag = tag.HoldTag;
            TpcTag = tag.TpcTag;
            SpecialInstWritten = tag.SpecialInstWritten;
            QualityMemo = tag.QualityMemo;
            SupplierIssue = tag.SupplierIssue;
            ModWritten = tag.ModWritten;
            LayeredAudit = tag.LayeredAudit;
            Picture01 = tag.Picture01;
            Picture02 = tag.Picture02;
            Attachments = tag.Attachments;
            ActiveStatus = tag.ActiveStatus;
            Closed = tag.Closed;
        }

        public TagTemplate Template
        {
            get
            {
                if (IsInternalAlert && QualityMemo.GetValueOrDefault())
                {
                    return TagTemplate.QualityAlertInternal;
                }
                else if (IsExternalAlert && QualityAtag.GetValueOrDefault())
                {
                    return TagTemplate.QualityAlertExternal;
                }
                else if (SpecialInstWritten.GetValueOrDefault())
                {
                    return TagTemplate.Special;
                }
                else if (HoldTag.GetValueOrDefault())
                {
                    return TagTemplate.Hold;
                }
                else if (TpcTag.GetValueOrDefault())
                {
                    return TagTemplate.TPC;
                }

                return TagTemplate.None;
            }
        }

        public HtmlString Title
        {
            get
            {
                string normalTemplate = "<h1><span class=\"tag-title tag-title-center\">{0}</span></h1>";
                string bannerTemplate = "<h1><span class=\"tag-title tag-title-banner\" style=\"background-color:#000\">{0}{1}</span></h1>";
                string title = Format(normalTemplate, "Quality Alert");

                if (Template == TagTemplate.QualityAlertInternal || (Template == TagTemplate.QualityAlertExternal))
                {
                    // TODO: Customize the QA External header
                    ;

                }
                else if (Template == TagTemplate.Special)
                {
                    title = Format(normalTemplate, "Special Instruction Sheet");
                }
                else if (Template == TagTemplate.Hold)
                {
                    title = Format(bannerTemplate, PartId, "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ON HOLD");
                }
                else if (Template == TagTemplate.TPC)
                {
                    title = Format(bannerTemplate,
                                          "TEMPORARY PROCESS CHANGE",
                                          "<span class=\"tpc-tag-title-inset\">FOR:" +
                                            "<span class=\"tag-title tag-title-banner tpc-tag-title-info\">" +
                                             $"<strong style=\"font-size:1.1em\">{PartId}" +
                                            "</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                            $"{LongId}</span></span>");
                }

                return new HtmlString(title);
            }
        }
    }
}
