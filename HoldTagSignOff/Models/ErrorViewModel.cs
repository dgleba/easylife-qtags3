/**
 * ErrorViewModel.cs
 * 
 * Default encapsulation scheme for error messages
 *
 */

namespace HoldTagSignOff.Models
{
    /// <summary>
    ///  Provides a customized View for reporting errors to the user.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        ///  Gets or sets the id of the request that was referred to the <see cref="ErrorViewModel"/>.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        ///  Returns true if the <see cref="RequestId"/> property of this <see cref="ErrorViewModel"/> is not <see langword="null"/>.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
