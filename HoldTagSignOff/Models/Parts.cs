﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class Parts
    {
        public string PartId { get; set; }
        public string Description { get; set; }
        public bool? RptScrap { get; set; }
        public int? PlantNumber { get; set; }
    }
}
