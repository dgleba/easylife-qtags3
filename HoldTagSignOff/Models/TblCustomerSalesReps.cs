﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class TblCustomerSalesReps
    {
        public int CustSalesId { get; set; }
        public int? CustomerId { get; set; }
        public string SalesRepFirstName { get; set; }
        public string SalesRepLastName { get; set; }
        public string SalesRepPhoneNumber { get; set; }
        public string SalesRepEmail { get; set; }
    }
}
