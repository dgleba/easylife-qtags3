﻿using Microsoft.EntityFrameworkCore;
using System;

namespace HoldTagSignOff.Models
{
    public partial class QualityTagsContext : DbContext
    {
        public QualityTagsContext()
        {
        }

        public QualityTagsContext(DbContextOptions<QualityTagsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Attachments> Attachments { get; set; }
        public virtual DbSet<Blobs> Blobs { get; set; }
        public virtual DbSet<Depts> Depts { get; set; }
        public virtual DbSet<Machines> Machines { get; set; }
        public virtual DbSet<MailingList> MailingList { get; set; }
        public virtual DbSet<PartDepts> PartDepts { get; set; }
        public virtual DbSet<Parts> Parts { get; set; }
        public virtual DbSet<StorageLocations> StorageLocations { get; set; }
        public virtual DbSet<TblCustomerContacts> TblCustomerContacts { get; set; }
        public virtual DbSet<TblCustomerDispositions> TblCustomerDispositions { get; set; }
        public virtual DbSet<TblCustomerSalesReps> TblCustomerSalesReps { get; set; }
        public virtual DbSet<TblCustomerSorters> TblCustomerSorters { get; set; }
        public virtual DbSet<TblCustomers> TblCustomers { get; set; }
        public virtual DbSet<TblHoldTagSo> TblHoldTagSo { get; set; }
        public virtual DbSet<TblHtpcReasons> TblHtpcReasons { get; set; }
        public virtual DbSet<TblPartsCustomer> TblPartsCustomer { get; set; }
        public virtual DbSet<TblPlantNames> TblPlantNames { get; set; }
        public virtual DbSet<TblProblemSource> TblProblemSource { get; set; }
        public virtual DbSet<TblQualityIssues> TblQualityIssues { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                throw new InvalidOperationException(
                    $"No database connection was configured for {typeof(QualityTagsContext).Name}.");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Attachments>(entity =>
            {
                entity.HasIndex(e => e.Path)
                    .HasName("fk_attachments_storage_directory");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Path).HasColumnType("int(11)");

                entity.HasOne(d => d.PathNavigation)
                    .WithMany(p => p.Attachments)
                    .HasForeignKey(d => d.Path)
                    .HasConstraintName("fk_attachments_storage_directory");
            });

            modelBuilder.Entity<Blobs>(entity =>
            {
                entity.HasIndex(e => e.Path)
                    .HasName("fk_blobs_storage_directory");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Bytes)
                    .HasColumnType("bigint(20)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ContentType)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Path).HasColumnType("int(11)");

                entity.HasOne(d => d.PathNavigation)
                    .WithMany(p => p.Blobs)
                    .HasForeignKey(d => d.Path)
                    .HasConstraintName("fk_blobs_storage_directory");
            });

            modelBuilder.Entity<Depts>(entity =>
            {
                entity.HasKey(e => e.DeptId)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.Department)
                    .HasName("Department");

                entity.Property(e => e.DeptId)
                    .HasColumnName("DeptID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Department)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.DepartmentState)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<Machines>(entity =>
            {
                entity.HasKey(e => e.MachineId)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.DeptId)
                    .HasName("DeptID");

                entity.HasIndex(e => e.PlantId)
                    .HasName("PlantID");

                entity.HasIndex(e => e.Stamp)
                    .HasName("Stamp");

                entity.Property(e => e.MachineId)
                    .HasColumnName("MachineID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.DeptId)
                    .HasColumnName("DeptID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.MachineDesc)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.PlantId)
                    .HasColumnName("PlantID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Stamp)
                    .HasColumnType("varchar(16)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<MailingList>(entity =>
            {
                entity.HasIndex(e => e.EmailAddress)
                    .HasName("EmailAddress")
                    .IsUnique();

                entity.HasIndex(e => e.LastName)
                    .HasName("LastName");

                entity.Property(e => e.MailingListId)
                    .HasColumnName("MailingListID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.EmailAddress)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.FirstName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.LastName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<PartDepts>(entity =>
            {
                entity.HasKey(e => e.PartDeptId)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.Cost)
                    .HasName("Cost");

                entity.HasIndex(e => e.DeptId)
                    .HasName("DeptID");

                entity.HasIndex(e => e.PartId)
                    .HasName("PartID");

                entity.HasIndex(e => e.WeightLbs)
                    .HasName("WeightLbs");

                entity.Property(e => e.PartDeptId)
                    .HasColumnName("PartDeptID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Area)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.Cost).HasColumnType("decimal(9,2)");

                entity.Property(e => e.DateCost).HasColumnType("datetime");

                entity.Property(e => e.DeptId)
                    .HasColumnName("DeptID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PartId)
                    .HasColumnName("PartID")
                    .HasColumnType("varchar(16)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Sequence)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<Parts>(entity =>
            {
                entity.HasKey(e => e.PartId)
                    .HasName("PRIMARY");

                entity.HasIndex(e => e.PlantNumber)
                    .HasName("PlantNumber");

                entity.HasIndex(e => e.RptScrap)
                    .HasName("RptScrap");

                entity.Property(e => e.PartId)
                    .HasColumnName("PartID")
                    .HasColumnType("varchar(16)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Description)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.PlantNumber)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");
            });

            modelBuilder.Entity<StorageLocations>(entity =>
            {
                entity.HasIndex(e => e.Path)
                    .HasName("Path");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblCustomerContacts>(entity =>
            {
                entity.HasKey(e => e.CustContId)
                    .HasName("PRIMARY");

                entity.ToTable("tblCustomerContacts");

                entity.HasIndex(e => e.ContactLastName)
                    .HasName("ContactLastName");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("CustomerID");

                entity.Property(e => e.CustContId)
                    .HasColumnName("CustContID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CellNumber)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ContactEmail)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ContactFirstName)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ContactLastName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblCustomerDispositions>(entity =>
            {
                entity.ToTable("tblCustomerDispositions");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Disposition)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblCustomerSalesReps>(entity =>
            {
                entity.HasKey(e => e.CustSalesId)
                    .HasName("PRIMARY");

                entity.ToTable("tblCustomerSalesReps");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("CustomerID");

                entity.HasIndex(e => e.SalesRepLastName)
                    .HasName("SalesRepLastName");

                entity.Property(e => e.CustSalesId)
                    .HasColumnName("CustSalesID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.SalesRepEmail)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SalesRepFirstName)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SalesRepLastName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SalesRepPhoneNumber)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblCustomerSorters>(entity =>
            {
                entity.HasKey(e => e.CustSortId)
                    .HasName("PRIMARY");

                entity.ToTable("tblCustomerSorters");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("CustomerID");

                entity.HasIndex(e => e.SortCompanyName)
                    .HasName("SortCompanyName");

                entity.HasIndex(e => e.SortContactLastName)
                    .HasName("SortContactLastName");

                entity.Property(e => e.CustSortId)
                    .HasColumnName("CustSortID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.SortCompanyName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SortContactFirstName)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SortContactLastName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SortEmail)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SortPhoneNumber)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblCustomers>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("PRIMARY");

                entity.ToTable("tblCustomers");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("CustomerID");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Current).HasDefaultValueSql("'0'");

                entity.Property(e => e.CustLocation)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CustName)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblHoldTagSo>(entity =>
            {
                entity.HasKey(e => e.HtsoId)
                    .HasName("PRIMARY");

                entity.ToTable("tblHoldTagSO");

                entity.HasIndex(e => e.CreatedBy)
                    .HasName("CreatedBy");

                entity.HasIndex(e => e.TagNumber)
                    .HasName("TagNumber")
                    .IsUnique();

                entity.HasIndex(e => e.UpdatedBy)
                    .HasName("UpdatedBy");

                entity.Property(e => e.HtsoId).HasColumnType("int(11)");

                entity.Property(e => e.AuthorizedBy)
                    .HasColumnType("varchar(8)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .HasColumnType("varchar(85)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.DateClosed).HasColumnType("datetime");

                entity.Property(e => e.OkToProcessBy)
                    .HasColumnType("varchar(8)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ReworkAmt)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ReworkBy)
                    .HasColumnType("varchar(8)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ScrapAmt)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ScrapBy)
                    .HasColumnType("varchar(8)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SortAmt)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.SortBy)
                    .HasColumnType("varchar(8)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.TagNumber)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnType("varchar(85)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.UseAsIsAmt).HasColumnType("int(11)");

                entity.Property(e => e.UseAsIsBy)
                    .HasColumnType("varchar(8)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblHtpcReasons>(entity =>
            {
                entity.ToTable("tblHtpcReasons");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Reason)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblPartsCustomer>(entity =>
            {
                entity.HasKey(e => e.PartCustId)
                    .HasName("PRIMARY");

                entity.ToTable("tblPartsCustomer");

                entity.HasIndex(e => e.CustId)
                    .HasName("CustID");

                entity.HasIndex(e => e.PartId)
                    .HasName("PartID");

                entity.Property(e => e.PartCustId)
                    .HasColumnName("PartCustID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CustId)
                    .HasColumnName("CustID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PartId)
                    .HasColumnName("PartID")
                    .HasColumnType("varchar(16)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblPlantNames>(entity =>
            {
                entity.HasKey(e => e.PlantId)
                    .HasName("PRIMARY");

                entity.ToTable("tblPlantNames");

                entity.HasIndex(e => e.PlantId)
                    .HasName("PlantID");

                entity.Property(e => e.PlantId)
                    .HasColumnName("PlantID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Area)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PlantDescrpition)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.PlantName)
                    .IsRequired()
                    .HasColumnType("varchar(25)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblProblemSource>(entity =>
            {
                entity.HasKey(e => e.ProblemSourceCode)
                    .HasName("PRIMARY");

                entity.ToTable("tblProblemSource");

                entity.Property(e => e.ProblemSourceCode)
                    .HasColumnType("varchar(3)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ProblemSource)
                    .IsRequired()
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");
            });

            modelBuilder.Entity<TblQualityIssues>(entity =>
            {
                entity.ToTable("tblQualityIssues");

                entity.HasIndex(e => e.Attachments)
                    .HasName("Attachments");

                entity.HasIndex(e => e.Closed)
                    .HasName("Closed");

                entity.HasIndex(e => e.Cost)
                    .HasName("Cost");

                entity.HasIndex(e => e.CreatedBy)
                    .HasName("CreatedBy");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("CustomerID");

                entity.HasIndex(e => e.CustomerRefNum)
                    .HasName("CustomerRefNum");

                entity.HasIndex(e => e.Date)
                    .HasName("Date");

                entity.HasIndex(e => e.DispositionCustomer)
                    .HasName("DispositionCustomer");

                entity.HasIndex(e => e.DispositionStackpole)
                    .HasName("DispositionStackpole");

                entity.HasIndex(e => e.IssuedBy)
                    .HasName("IssuedBy");

                entity.HasIndex(e => e.MachineId)
                    .HasName("MachineID");

                entity.HasIndex(e => e.NofPiecesHt)
                    .HasName("NofPiecesHT");

                entity.HasIndex(e => e.NofPiecesQa)
                    .HasName("NofPiecesQA");

                entity.HasIndex(e => e.Operation)
                    .HasName("Operation");

                entity.HasIndex(e => e.PartId)
                    .HasName("PartID");

                entity.HasIndex(e => e.Picture01)
                    .HasName("Picture01");

                entity.HasIndex(e => e.Picture02)
                    .HasName("Picture02");

                entity.HasIndex(e => e.ProblemType)
                    .HasName("ProblemType");

                entity.HasIndex(e => e.Reason)
                    .HasName("Reason");

                entity.HasIndex(e => e.UpdatedBy)
                    .HasName("UpdatedBy");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ActiveStatus).HasColumnType("int(2)");

                entity.Property(e => e.Attachments).HasColumnType("int(11)");

                entity.Property(e => e.Body)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Changed)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Comment)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Cost)
                    .HasColumnType("decimal(9,2)")
                    .HasDefaultValueSql("'0.00'");

                entity.Property(e => e.CreatedAt).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .HasColumnType("varchar(85)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.CustomerId)
                    .HasColumnName("CustomerID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CustomerRefNum)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Date).HasColumnType("datetime");

                entity.Property(e => e.DateIssued).HasColumnType("datetime");

                entity.Property(e => e.DispositionCustomer)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.DispositionStackpole)
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Feature)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.FeatureNumber)
                    .HasColumnType("varchar(25)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.IssuedBy)
                    .IsRequired()
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.LengthOfChange).HasDefaultValueSql("'1'");

                entity.Property(e => e.MachineId)
                    .HasColumnName("MachineID")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.NofPiecesHt)
                    .HasColumnName("NofPiecesHT")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.NofPiecesQa)
                    .HasColumnName("NofPiecesQA")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.OkdBy)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Operation)
                    .HasColumnType("varchar(222)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.OperationNumber)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.OpertionHd)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.OpertionQa)
                    .HasColumnName("OpertionQA")
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.OpertionSp)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.OpertionTp)
                    .HasColumnType("varchar(50)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.PartId)
                    .IsRequired()
                    .HasColumnName("PartID")
                    .HasColumnType("varchar(16)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Picture01).HasColumnType("int(11)");

                entity.Property(e => e.Picture02).HasColumnType("int(11)");

                entity.Property(e => e.ProblemType)
                    .HasColumnType("varchar(3)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Qty).HasColumnType("int(6)");

                entity.Property(e => e.QualityAlert)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.QualityAlertMemo)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.QualityAtag).HasColumnName("QualityATag");

                entity.Property(e => e.Reason)
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.ReasonNote)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.SpecialInst)
                    .HasColumnType("text")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.UpdatedAt).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasColumnType("varchar(85)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_general_ci");

                entity.Property(e => e.Year).HasColumnType("int(11)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
