﻿using System;
using System.Collections.Generic;

namespace HoldTagSignOff.Models
{
    public partial class Machines
    {
        public int MachineId { get; set; }
        public int? DeptId { get; set; }
        public string MachineDesc { get; set; }
        public int? PlantId { get; set; }
        public string Stamp { get; set; }
    }
}
