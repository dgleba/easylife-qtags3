/**-------------------------------------------------------
 * Program.cs
 *
 * TEMPLATE FILE -- DO NOT EDIT!
--------------------------------------------------------*/

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace HoldTagSignOff
{
    public class Program
    {
        /// <summary>
        /// The application's entry point.
        /// </summary>
        /// <param name="args">Command-line arguments for the application runner</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        /// <summary>
        /// Configures the application's hosting environment.
        /// </summary>
        /// <param name="args">Collection of any command-line arguments passed to the application runner.
        /// </param>
        /// <returns>An object representation of this application's startup parameters.</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls("http://*:5000");
                });
    }
}
