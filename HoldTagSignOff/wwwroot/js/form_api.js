/**
 * form_api.js
 *
 * Event handlers for all input controls.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.17: Created
 */

/**
 * Encapsulates all input event handlers used by the app.
 * @namespace FormAPI
 */
const FormAPI = (function () {
	"use strict";

	const AUTO_FILL_BOXES = $('input').filter(function () { return $(this).attr('list') });
	const TAG_TYPE_CHECKBOXES = $('.tagSummary').find('[type="checkbox"]').not('[disabled]');

	/**
	 * Shows a prompt below any empty input box with a populated `datalist` attached.
	 * @private
	 */
	function showPromptWhenOptionsAvailable() {
		AUTO_FILL_BOXES.siblings('datalist').each(function () {
			if (!$(this).siblings('input').filter(function () { return $(this).val() }).length
					&& $(this)[0].options.length) {
				$(this).siblings('.list-input-prompt').show();
			} else {
				$(this).siblings('.list-input-prompt').hide();
			}
		});
	}

	/**
	 * Queries the `TblPartsCustomer` model for parts associated with the selected customer.
	 * @private
	 */
	function populatePartsList() {
		const listName = '#' + $(this).attr('list');
		const listSelection = ' [value="' + $(this).val() + '"]';
		const itemKey = $(listName + listSelection).data('value');

		$.ajax({
			type: "POST",
			url: "/api/CustomerPartFinder",
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			data: JSON.stringify({
				CustomerId: itemKey
			}),
			success: function (result) {
				$('#partList').html(result);
			},
			error: function (jqXHR, textStatus) {
				flashMessage(textStatus.toUpperCase() + ": " + jqXHR.status, true);
			}
		});
	}

	/**
	 * Queries the `TblQualityIssues` model when a new open tag is selected.
	 * @private
	 */
	function fetchOpenTagInfo() {
		const listName = '#' + $(this).attr('list');
		const listSelection = ' [value="' + $(this).val() + '"]';
		const itemKey = $(listName + listSelection).data('value');

		console.table(JSON.stringify({
			Id: itemKey,
			PartId: $(holdTagPartId).val() || "0",
			Reason: $('#tagReason').val() || "none",
			Operation: $('#tagProcess').val() || "scrap",
			IssuedBy: $('#tagIssuer').val() || "your's truly",
			LengthOfChange: parseFloat($('#tagLengthOfChange').val()) || 1.0
		}));

		$.ajax({
			type: "POST",
			url: "/api/OpenTagFinder",
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			cache: true,
			// yes, even JS knows what fields are required of a valid `TblQualityIssues` model
			data: JSON.stringify({
				Id: itemKey,
				PartId: $(holdTagPartId).val() || "0",
				Reason: $('#tagReason').val() || "none",
				Operation: $('#tagProcess').val() || "scrap",
				IssuedBy: $('#tagIssuer').val() || "your's truly",
				LengthOfChange: parseFloat($('#tagLengthOfChange').val()) || 1.0
			}),
			success: function (result) {
				if (result) {
					$('#holdTagPartId').text(result.partId || $('#holdTagPartId').text());

					$('#tagLongId').text(result.longId || $('#tagLongId').text());
					$('#tagDate').text(result.date || $('#tagDate').text());
					$('#tagProcess').text(result.operation || $('#tagProcess').text());
					$('#tagIssuer').text(result.issuedBy || $('#tagIssuer').text());
					$('#tagReason').text(result.date || $('#tagReason').text());
					$('#tagBody').text(result.date || $('#tagBody').text());

					$('#Qty').text(result.qty || $('#Qty').text());
					$('#Comment').text(result.comment || $('#Comment').text());
				}

			},
			error: function (jqXHR, textStatus) {
				flashMessage(textStatus.toUpperCase() + ": " + jqXHR.status, true);
			}
		});
	}

	/**
	 * Swaps the displayed value with the associated primary key, if different, so
	 * that fields of the expected data type are passed to `TblQualityIssues`.
	 * 
	 * If the input has a value not among the options, THAT value is passed instead.
	 * Any type conversion errors will be caught and reported by the Create or Edit
	 * action of `QualityIssuesController`.
	 * 
	 * TODO: trigger update of the associated table when unlisted values are passed.
	 * @private
	 */
	function collectPrimaryKeys() {
		AUTO_FILL_BOXES.each(function () {
			const listName = '#' + $(this).attr('list');
			const listSelection = ' [value="' + $(this).val() + '"]';
			const itemKey = $(listName + listSelection).data('value');

			if (itemKey) {
				$(this).siblings('[type="hidden"]').val(itemKey);
			} else if ($(this).val()) {
				$(this).siblings('[type="hidden"]').val($(this).val().trim());
			}
		});
	}

	/**
	 * @callback tagSummary_OnCheckChanged
	 * Makes tag type selection boxes behave like radio buttons.
	 * @private
	 */
	function tagSummary_OnCheckChanged() {
		/**
		 * The `check on` handler.
		 * @param {HTMLInputElement(type=checkbox)} activeMode The currently selected checkbox.
		 */
		function toggleOthers(activeMode) {
			const fieldsToShow = $(activeMode).parent().parent().data('target');
						
			$(fieldsToShow).each(function () { $(this).show(); });

			//console.log('Showing ' + $(fieldsToShow).length + ' ' + fieldsToShow.replace('.', '') + ' fields');

			$('.tagSummary').find('[type="checkbox"]').not('[disabled]').each(function () {
				if ($(this).attr('id') !== $(activeMode).attr('id') && $(activeMode).prop('checked')) {
					$(this).prop('checked', false);
					turnOff(this);
				}
			});
		}

		/**
		 * The `check off` handler.
		 * @param {HTMLInputElement(type=checkbox)} inactiveMode The un-selected checkbox.
		 */
		function turnOff(inactiveMode) {
			const fieldsToHide = $(inactiveMode).parent().parent().data('target');
			const activeClass =
				TAG_TYPE_CHECKBOXES.filter(function () {
					return $(this).prop('checked')
				}).first().parent().parent().data('target');
			if (activeClass) {
				$(fieldsToHide).filter(function () {
					return $.inArray(activeClass.replace('.', ''), $(this).attr('class').split(/\s+/)) < 0;
				}).hide();
			}
		}

		if ($('#QualityMemo').prop('checked')) {
			$('#lblBody').text('Additional Information');
			$('#lblSubForm').text('Quality Alert ' + unescape('%u2013') + ' Internal');
			toggleOthers(this);
		}

		if ($('#QualityAtag').prop('checked')) {
			$('#lblBody').text('Additional Information');
			$('#lblSubForm').text('Quality Alert');
			toggleOthers(this);
		} else {
			turnOff($('#QualityAtag'));
		}

		if ($('#HoldTag').prop('checked')) {
			$('#lblSubForm').text('Hold Tag');
			$('#lblBody').text('Reason');
			toggleOthers(this);
		} else {
			turnOff($('#HoldTag'));
		}

		if ($('#TpcTag').prop('checked')) {
			$('#lblSubForm').text('TPC Tag');
			$('#lblBody').text('Changed');
			toggleOthers(this);
		} else {
			turnOff($('#TpcTag'));
		}

		if ($('#SpecialInstWritten').prop('checked')) {
			$('#lblSubForm').text('Special Instruction');
			$('#lblBody').text('Additional Information');
			toggleOthers(this);
		} else {
			turnOff($('#SpecialInstWritten'));
		}

		// no active selection? select `Quality Tag`
		if (!TAG_TYPE_CHECKBOXES.filter(function () {
				return $(this).prop('checked')
			}).length) {
			if ($('#ProblemType').val() === 'IN') {
				$('#QualityMemo').prop('checked', true).trigger('change');
			} else {
				$('#QualityAtag').prop('checked', true).trigger('change');
			}
		}
	}

	/**
	 * @callback lengthOfChange_OnTextChanged
	 * Updates the display value of a tag's expiry date.
	 */
	function lengthOfChange_OnTextChanged() {
		/**
		 * Returns a new expiry date based on the given number of `daysForward`.
		 * @param {string} current Current display value as a date string.
		 * @param {number} daysForward Number of days till new expiry date.
		 * @returns {Date} Date object representing new expiry date.
		 */
		function setExpiryDate(current, daysForward) {
			const dateFrom = new Date(current);

			$('#frmEditTag').find('#newDuration').html(daysForward);
			$('#frmEditTag').find('#newDays').html(daysForward > 1 ? 'days' : 'day');

			return new Date(Date.UTC(dateFrom.getFullYear(),
				dateFrom.getMonth(),
				dateFrom.getDate() + (daysForward || 0),
				dateFrom.getDay())).toLocaleDateString('en-CA', {
				year: 'numeric',
				month: 'long',
				day: 'numeric'
			});
		}

		const newExpiry = setExpiryDate(new Date(), Math.round(parseFloat($('#LengthOfChange').val().trim(), 10)));
		if (newExpiry) {
			$('#ExpiresOn').html($('<strong>').text(newExpiry));
		}
	}

	/**
	* @callback totalToDisposition_OnChange
	* Updates the display value of a Hold Tag's "Total Amount" field (read-only)
	*/
	function totalToDisposition_OnChange() {
		const sortAmt = $('#frmSignOffHoldTag').find("#SortAmt").val() || '0';
		const reworkAmt = $('#frmSignOffHoldTag').find("#ReworkAmt").val() || '0';
		const useAsIsAmt = $('#frmSignOffHoldTag').find("#UseAsIsAmt").val() || '0';
		const scrapAmt = $('#frmSignOffHoldTag').find("#ScrapAmt").val() || '0';
		const quantity = $('#frmSignOffHoldTag').find("#Qty").val() || '0';
		const subTotal = (parseInt(sortAmt) + parseInt(reworkAmt) + parseInt(useAsIsAmt)) - parseInt(quantity);
		const sum = (subTotal) - parseInt(scrapAmt);

		if (sum >= 0) 
			$('#totalToDisposition').html($('<strong>').text(sum));
		else
			$('#totalToDisposition').html($('<strong>').text(0));
	}

	/**
	* @callback holdTagTotalAmt_OnChange
	* Updates the display value of a Hold Tag's "Total Amount to be Dispositioned" field (read-only)
	*/
	function holdTagTotalAmt_OnChange() {
		const sortAmt = $('#frmSignOffHoldTag').find("#SortAmt").val() || '0';
		const reworkAmt = $('#frmSignOffHoldTag').find("#ReworkAmt").val() || '0';
		const useAsIsAmt = $('#frmSignOffHoldTag').find("#UseAsIsAmt").val() || '0';

		const sum = parseInt(sortAmt) + parseInt(reworkAmt) + parseInt(useAsIsAmt);
		if (sum >= 0)
			$('#holdTagTotalAmt').html($('<strong>').text(sum));
		else
			$('#holdTagTotalAmt').html($('<strong>').text(0));
	}
	/**
	 * Initialization  
	 */
	AUTO_FILL_BOXES.each(function () {
		$(this).on('focus click mouseup keyup', function () {
			showPromptWhenOptionsAvailable();
		});
	});
	TAG_TYPE_CHECKBOXES.on('change', tagSummary_OnCheckChanged);
	$('#LengthOfChange').on('input', lengthOfChange_OnTextChanged);

	// Make "Total Scrap Amount" dynamic
	$('#frmSignOffHoldTag').find("#SortAmt").on('input', holdTagTotalAmt_OnChange);
	$('#frmSignOffHoldTag').find("#ReworkAmt").on('input', holdTagTotalAmt_OnChange);
	$('#frmSignOffHoldTag').find("#UseAsIsAmt").on('input', holdTagTotalAmt_OnChange);

	// Make "Total to be Dispositioned" dynamic
	$('#frmSignOffHoldTag').find("#SortAmt").on('input', totalToDisposition_OnChange);
	$('#frmSignOffHoldTag').find("#ReworkAmt").on('input', totalToDisposition_OnChange);
	$('#frmSignOffHoldTag').find("#UseAsIsAmt").on('input', totalToDisposition_OnChange);
	$('#frmSignOffHoldTag').find("#ScrapAmt").on('input', totalToDisposition_OnChange);
	$('#frmSignOffHoldTag').find("#Qty").on('input', totalToDisposition_OnChange);

	$('#frmNewTag, #frmEditTag').find('input[list="customerList"]').on('input', populatePartsList);
	$('#frmSignOffHoldTag').find('input[list="openTagList"]').on('input', fetchOpenTagInfo);
	$('#frmNewTag, #frmEditTag, #frmSignOffHoldTag').on('submit', collectPrimaryKeys);

	// automatically highlight input contents 
	$('#frmNewTag, #frmEditTag, #frmSignOffHoldTag').find('input[type=text], input[type=number]').each(function () {
		$(this).on('click focus', function () { $(this).select() });
	});

	// tailor prompts for Firefox users
	if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
		AUTO_FILL_BOXES.siblings('.list-input-prompt').text("Double-click to see options");
	}

	return {
		get autoFills() {
			return AUTO_FILL_BOXES;
		},
		get tagSelectors() {
			return TAG_TYPE_CHECKBOXES;
		},
		/**
		 * Prepares form on first page load.
		 */
		initForm: function () {
			if ($('#ProblemType').val() === 'IN') {
				$('#lblSubForm').text('Quality Alert ' + unescape('%u2013') + ' Internal');
			} else {
				$('#lblSubForm').text('Quality Alert');
			}

			showPromptWhenOptionsAvailable();
		},
		initSignOffHoldTagForm: function () {
			//fetchOpenTagInfo();
			showPromptWhenOptionsAvailable();
		},
		/**
		 * Restores submitted values to a form that reloads after validation failure.
		 */
		restoreDisplayValuesOnError: function () {
			// hide what's not relevant to the selected tag type
			TAG_TYPE_CHECKBOXES.filter(function () { return $(this).prop('checked') })
				.first().trigger('change');

			if ($('.field-validation-error').length > 0) {
				const custList = $('#frmNewTag').find('[list=customerList]');

				AUTO_FILL_BOXES.not('[list=partList]').each(function () {
					const listName = '#' + $(this).attr('list');
					const listSelection = ' [data-value="' + $(this).val() + '"]';
					const displayVal = $(listName + listSelection).val();

					if (displayVal !== $(this).val()) {
						$(this).val(displayVal);
					}

					if (custList.val()) {
						// make implicit call to `populatePartsList()`
						custList.trigger('input');
					}
				});

				// reveal invalid fields that may be hidden
				$('.field-validation-error').parent().show();
			}

			showPromptWhenOptionsAvailable();
		}
	};
})();