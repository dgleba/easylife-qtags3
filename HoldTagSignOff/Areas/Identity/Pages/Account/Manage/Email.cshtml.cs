﻿using HoldTagSignOff.Controllers;
using HoldTagSignOff.Models;
using HoldTagSignOff.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace HoldTagSignOff.Areas.Identity.Pages.Account.Manage
{
    public partial class EmailModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IViewRenderService _viewRenderService;

        public EmailModel(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IEmailSender emailSender,
            IViewRenderService viewRenderService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _viewRenderService = viewRenderService;
        }

        public string Username { get; set; }

        public string Email { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public EmailUpdate emailUpdate { get; set; }

        private async Task LoadAsync(IdentityUser user)
        {
            var email = await _userManager.GetEmailAsync(user);
            Email = email;

            emailUpdate = new EmailUpdate
            {
                NewEmail = email,
            };

            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(user);
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostChangeEmailAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            var email = await _userManager.GetEmailAsync(user);

            if (emailUpdate.NewEmail != email)
            {
                var userId = await _userManager.GetUserIdAsync(user);
                var code = await _userManager.GenerateChangeEmailTokenAsync(user, emailUpdate.NewEmail);
                var callbackUrl = Url.Page(
                    "/Account/ConfirmEmail",
                    pageHandler: null,
                    values: new { area = "Identity", userId, code },
                    protocol: Request.Scheme);

                var viewModel = new UserRegistration() { UserName = user.UserName, Email = user.Email };
                string confirmationUrl = HtmlEncoder.Default.Encode(callbackUrl);
                string body = await new EmailController(_viewRenderService)
                                .FetchBody("Emails/ChangeEmail", viewModel);
                // WARNING 
                // Don't try to render the `confirmationUrl` directly in the body of the email View:
                // it won't be valid 
                body = body.Replace("href=\"#\"", $"href=\"{confirmationUrl}\"", StringComparison.OrdinalIgnoreCase);

                await _emailSender.SendEmailAsync(emailUpdate.NewEmail, "Confirm your new email", body);

                StatusMessage = "Confirmation link to change email sent. Please check your email.";

                return RedirectToPage();
            }

            StatusMessage = "Your email is unchanged.";
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            var userId = await _userManager.GetUserIdAsync(user);
            var email = await _userManager.GetEmailAsync(user);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { area = "Identity", userId = userId, code = code },
                protocol: Request.Scheme);

            var viewModel = new UserRegistration() { UserName = user.UserName, Email = user.Email };
            string confirmationUrl = HtmlEncoder.Default.Encode(callbackUrl);
            string body = await new EmailController(_viewRenderService)
                            .FetchBody<UserRegistration>("Emails/Change", viewModel);
            // WARNING 
            // Don't try to render the `confirmationUrl` directly in the body of the email View:
            // it won't be valid 
            body = body.Replace("href=\"#\"", $"href=\"{confirmationUrl}\"", StringComparison.OrdinalIgnoreCase);

            await _emailSender.SendEmailAsync(emailUpdate.NewEmail, "Confirm your new email", body);

            StatusMessage = "Verification email sent. Please check your email.";
            emailUpdate.NewEmail = string.Empty;

            return RedirectToPage();
        }
    }
}
