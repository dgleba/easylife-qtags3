﻿/**
 * RoleController.cs
 * 
 * Role management controller.
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.08: Created
 */

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HoldTagSignOff.Controllers
{
    [Authorize(Roles = "admin")]
    public class RoleController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        // Instantiates a new RoleController from the given UserManager and RoleManager
        public RoleController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        // Retrieves a current listing of available roles
        public ActionResult Index()
        {
            return View(roleManager.Roles.OrderBy(r => r.Name));
        }

        // Retrieves a listing of users grouped by role membership
        public async Task<IActionResult> ListUsers(string roleName)
        {
            roleName ??= Request.Cookies[nameof(roleName)];

            if (string.IsNullOrEmpty(roleName)
                    && string.IsNullOrEmpty(Request.Cookies[nameof(roleName)]))
            {
                TempData["Message"] = "Bad request or missing role name.";
            }
            else if (!await roleManager.RoleExistsAsync(roleName))
            {
                TempData["Message"] = $"Unknown role {roleName}!";

                return RedirectToAction(nameof(Index));
            }

            var roleOwners = await userManager.GetUsersInRoleAsync(roleName);
            var exclusions = userManager.Users
                                        .AsEnumerable()
                                        .Where(usr => !roleOwners.Contains(usr))
                                        .ToList();

            ViewBag.role = roleName;
            ViewBag.exclusions =
                new SelectList(exclusions.OrderBy(usr => usr.UserName), "Id", "UserName");
            Response.Cookies.Append(nameof(roleName), roleName);

            return View(await Task.Run(() => roleOwners.OrderBy(usr => usr.UserName)));
        }

        // Creates a new role with the given name
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(string roleName)
        {
            try
            {
                if (string.IsNullOrEmpty(roleName) || string.IsNullOrWhiteSpace(roleName))
                {
                    TempData["Message"] = "Bad request or missing role name..";
                }
                else
                {
                    roleName = roleName.Trim().ToLowerInvariant();

                    if (await roleManager.RoleExistsAsync(roleName))
                    {
                        TempData["Message"] = $"The role '{roleName}' already exists!";
                    }
                    else
                    {
                        IdentityResult result =
                            await roleManager.CreateAsync(new IdentityRole(roleName));

                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.FirstOrDefault().Description);
                        }

                        TempData["Message"] = $"The role '{roleName}' was added successfully.";
                    }
                }
            }
            catch (Exception exc)
            {
                TempData["Message"] = $"Role creation failed with the message: {exc.Message}";

                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index));
        }

        // Retrieves the role with the given name, if any, and offers it for deletion
        public async Task<IActionResult> Delete(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                TempData["Message"] = "Bad request or unknown role.";
                return RedirectToAction(nameof(Index));
            }
            else if (!await roleManager.RoleExistsAsync(roleName))
            {
                TempData["Message"] = $"Can't delete unknown role {roleName}!";
                return RedirectToAction(nameof(Index));
            }

            var roleToDelete = await roleManager.FindByNameAsync(roleName);

            if (roleToDelete.Name.Equals("administrators"))
            {
                TempData["Message"] = $"The role {roleName} cannot be deleted!";
                return RedirectToAction(nameof(Index));
            }

            var roleOwners = await userManager.GetUsersInRoleAsync(roleToDelete.Name);
            ViewBag.roleOwners = roleOwners.ToList();

            return View(roleToDelete);

        }

        // Deletes the given role, if no users belong to it
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete([Bind("Id,Name")] IdentityRole role)
        {
            try
            {
                if (string.IsNullOrEmpty(role.Id))
                {
                    TempData["Message"] = "Bad request or unknown role.";
                }

                var roleOwners = await userManager.GetUsersInRoleAsync(role.Name);

                if (roleOwners.Any())
                {
                    TempData["Message"] = $"Error: role {role.Name} has users in it!";
                }
                else
                {
                    var roleToDelete = await roleManager.FindByIdAsync(role.Id);

                    if (roleToDelete == null)
                    {
                        throw new Exception($"Error: can't delete unknown role '{role.Name}'.");
                    }

                    IdentityResult result =
                         await roleManager.DeleteAsync(roleToDelete);

                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.FirstOrDefault().Description);
                    }

                    TempData["Message"] = $"The role '{roleToDelete.Name}' was deleted.";
                }
            }
            catch (Exception exc)
            {
                TempData["Message"] = $"Deleting role failed with the message: {exc.Message}";

                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index));
        }

        // Revokes the role with the given name, if any, from the user with the given id, if any
        public async Task<IActionResult> RemoveFromRole(string userId, string roleName)
        {
            try
            {
                if (string.IsNullOrEmpty(userId))
                {
                    TempData["Message"] = "Bad request or unknown user.";
                }
                else if (string.IsNullOrEmpty(roleName))
                {
                    TempData["Message"] = "Bad request or unknown role.";
                }
                else
                {
                    var user = await userManager.FindByIdAsync(userId);

                    if (user == null)
                    {
                        throw new Exception($"Can't remove unknown user from role '{roleName}'!");
                    }

                    IdentityResult result =
                         await userManager.RemoveFromRoleAsync(user, roleName);

                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.FirstOrDefault().Description);
                    }

                    TempData["Message"] = $"User '{user.UserName}' was removed from role '{roleName}'.";
                }
            }
            catch (Exception exc)
            {
                TempData["Message"] = $"Request failed with the message: {exc.Message}";

                return RedirectToAction(nameof(ListUsers), "Role", roleName);
            }

            return RedirectToAction(nameof(ListUsers), "Role", roleName);
        }

        // Grants the role with the given name, if any, to the user with the given id, if any
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddToRole(string userId, string roleName)
        {
            try
            {
                if (string.IsNullOrEmpty(userId))
                {
                    TempData["Message"] = "Bad request or unknown user.";
                }
                else if (string.IsNullOrEmpty(roleName))
                {
                    TempData["Message"] = "Bad request or unknown role.";
                }
                else
                {
                    var userToAdd = await userManager.FindByIdAsync(userId);

                    if (userToAdd == null)
                    {
                        throw new Exception($"Can't add unknown user to role '{roleName}'!");
                    }

                    IdentityResult result =
                         await userManager.AddToRoleAsync(userToAdd, roleName);

                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.FirstOrDefault().Description);
                    }

                    TempData["Message"] = $"User '{userToAdd.UserName}' was added the role '{roleName}'.";
                }

            }
            catch (Exception exc)
            {
                TempData["Message"] = $"Request failed with the message: {exc.Message}";

                return RedirectToAction(nameof(ListUsers), "Role", roleName);
            }

            return RedirectToAction(nameof(ListUsers), "Role", roleName);
        }
    }
}
