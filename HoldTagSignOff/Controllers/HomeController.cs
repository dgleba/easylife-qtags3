﻿/**
 * HomeController.cs
 * 
 * Serves default Actions like the homepage and error page.
 *
 */

using Microsoft.AspNetCore.Mvc;
using HoldTagSignOff.Models;
using System;
using System.Diagnostics;
using System.Linq;

namespace HoldTagSignOff.Controllers
{
    /// <summary>
    /// A base class for an MVC controller with view support.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Serves the application's homepage.
        /// </summary>
        /// <returns>Defines a contract that represents the result of an action method.</returns>
        public IActionResult Index()
        {
            ViewData["PageName"] = "Welcome";
            string referrerPath = Request.Headers["Referer"].FirstOrDefault();

            if (!string.IsNullOrEmpty(referrerPath))
            {
                if (referrerPath.Contains("ForgotPassword", StringComparison.OrdinalIgnoreCase))
                {
                    TempData["Message"] = "Password reset instructions have been sent to your email.";
                }
            }

            return View();
        }

        /// <summary>
        /// Serves the privacy information page.
        /// </summary>
        /// <returns>Defines a contract that represents the result of an action method.</returns>
        public IActionResult Privacy()
        {
            return View();
        }

        /// <summary>
        /// Serves the privacy information page.
        /// </summary>
        /// <returns>Defines a contract that represents the result of an action method.</returns>
        public IActionResult About()
        {
            return View();
        }

        /// <summary>
        /// In production mode, this action serves the error page.
        /// </summary>
        /// <returns>Defines a contract that represents the result of an action method.</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            TempData["Message"] = "Sorry! The page you requested is missing or under construction.";
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
