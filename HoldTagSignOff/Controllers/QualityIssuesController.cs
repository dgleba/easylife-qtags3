﻿/**
 * QualityIssuesController.cs
 * 
 * Provides a CRUD interface for managing Quality Tag domain objects
 *
 * Revision History
 *     Robert Di Pardo, 2020.02.24: Created
 *     Chackaphope Ying Yong, 2020.03.16: Added documentation 
 */

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HoldTagSignOff.Models;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using static HoldTagSignOff.Helpers.UploadHelper;

namespace HoldTagSignOff.Controllers
{
    [Authorize(Roles = "admin, creator")]
    public class QualityIssuesController : Controller
    {
        /// <summary>
        /// Model exposing the underlying database to this Controller
        /// </summary>
        private readonly QualityTagsContext _context;

        /// <summary>
        /// Interface exposing the SMTP client to this Controller
        /// </summary>
        private readonly IEmailSender _emailSender;

        /// <summary>
        ///  Stores filenames that need to persist across requests, in case of failed model validation.
        /// </summary>
        static string _picture01FileName;

        /// <summary>
        ///  See <see cref="_picture01FileName"></see>.
        /// </summary>
        static string _picture02FileName;

        public QualityIssuesController(QualityTagsContext context, IEmailSender emailSender)
        {
            _context = context;
            _emailSender = emailSender;
        }

        /// <summary>
        /// This is the Index action of the controller. It draws the model from the TblQualityIssues and returns a view listing the index
        /// of all the quality tag context
        /// </summary>
        /// <returns> Returns the view of the index view for this controller for TblQualityIssued model</returns>
        public IActionResult Index()
        {
            ViewBag.Tags = new SelectList(_context.TblQualityIssues.OrderByDescending(t => t.UpdatedAt), "Id", "LongId");

            return View();
        }

        /// <summary>
        /// Serves the Hold Tag sign Off form
        /// </summary>
        /// <returns></returns>
        public async Task<ViewResult> SignOffHoldTag()
        {
            InitSignOffHoldTagForm();

            return await Task.Run(() => View(new OpenHoldTag()));
        }

        private void InitSignOffHoldTagForm()
        {
            var openHoldTags = _context.TblQualityIssues.AsEnumerable().
                               Where(t => t.HoldTag.GetValueOrDefault()
                                     && !t.Closed.GetValueOrDefault()
                                     && t.Date.Year > 2011)
                              .Select(t => t);

            ViewBag.OpenHoldTags = openHoldTags;
            var selection = openHoldTags.FirstOrDefault();

            if (selection != null)
            {
                ViewBag.SelectedTag = new TagReport(_context.TblQualityIssues.Find(selection.Id));
                InitReportView(selection.Id, Url.Action(nameof(Index)));
            }
            else
            {
                ViewBag.SelectedTag = new TagReport(new TblQualityIssues() { HoldTag = true });
                InitReportView(0, Url.Action(nameof(Index)));
            }
        }

        /// <summary>
        /// Handles POST request from the Hold Tag Sign Off form.
        /// </summary>
        /// <param name="openHoldTag"> An instance of the <see cref="TblHoldTagSo"/> model.</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignOffHoldTag([Bind("TagNumber,DateClosed,Closed,Qty,HtsoId,OkToProcessBy,SortAmt,SortBy,ReworkAmt,ReworkBy,ScrapAmt,ScrapBy,UseAsIsAmt,UseAsIsBy,AuthorizedBy,Comment,CreatedBy,UpdatedBy,UpdatedAt,CreatedAt")] OpenHoldTag openHoldTag)
        {
            if (ModelState.IsValid)
            {
                var nowClosedTag = await _context.TblQualityIssues.FindAsync(openHoldTag.TagNumber);

                if (nowClosedTag != null)
                {
                    nowClosedTag.Qty = openHoldTag.Qty;
                    nowClosedTag.Comment = openHoldTag.Comment;
                    nowClosedTag.Closed = openHoldTag.Closed.GetValueOrDefault() ? openHoldTag.Closed.GetValueOrDefault() : true;

                    TblHoldTagSo newHoldTagSignOff = new TblHoldTagSo()
                    {
                        HtsoId = openHoldTag.HtsoId,
                        TagNumber = openHoldTag.TagNumber,
                        DateClosed = openHoldTag.DateClosed,
                        OkToProcessBy = openHoldTag.OkToProcessBy,
                        SortAmt = openHoldTag.SortAmt,
                        SortBy = openHoldTag.SortBy,
                        ReworkAmt = openHoldTag.ReworkAmt,
                        ReworkBy = openHoldTag.ReworkBy,
                        ScrapAmt = openHoldTag.ScrapAmt,
                        ScrapBy = openHoldTag.ScrapBy,
                        UseAsIsAmt = openHoldTag.UseAsIsAmt,
                        UseAsIsBy = openHoldTag.UseAsIsBy
                    };
                    _context.Add(newHoldTagSignOff);
                    await _context.SaveChangesAsync();

                    TempData["Message"] = $"Tag {nowClosedTag.LongId} has been signed off and closed.";

                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    TempData["Message"] = $"No such tag with number {(openHoldTag?.TagNumber ?? 0)}. Sign-off failed.";
                }

            }

            InitSignOffHoldTagForm();
            return View(openHoldTag);
        }

        /// <summary>
        /// Lists all external issues, with associated customer details
        /// </summary>
        public async Task<IActionResult> CustomerIssues()
        {
            var customerList = _context.TblCustomers.Select(c =>
                new TblCustomers() { CustomerId = c.CustomerId, CustName = c.CustName, CustLocation = c.CustLocation });

            var customerPartList = from tag in _context.TblQualityIssues
                                   join cust in customerList.DefaultIfEmpty() on tag.CustomerId equals cust.CustomerId
                                   join part in _context.Parts on tag.PartId equals part.PartId
                                   select part;

            var issues = (from tag in _context.TblQualityIssues
                          join cust in customerList on tag.CustomerId equals cust.CustomerId
                          join part in customerPartList on tag.PartId equals part.PartId
                          join plant in _context.TblPlantNames on part.PlantNumber equals plant.PlantId
                          where tag.ProblemType.Equals("EX", StringComparison.OrdinalIgnoreCase)
                          select new CustomerIssue(tag, cust, plant))
                          .AsEnumerable()
                          .OrderByDescending(i => i.Id);

            return View(await Task.Run(() => issues));
        }

        /// <summary>
        /// Serves a single tag as static HTML or an editable form, depending on the request.
        /// </summary>
        /// <param name="id"> Id of tag to show.</param>
        /// <param name="returnUrl"> The value to be passed to the back navigation button.</param>
        /// <param name="editMode"> Whether of not fields should be editable.</param>
        /// <returns> A static page or a form.</returns>
        public async Task<IActionResult> Print(int? id, string returnUrl, bool editMode = false)
        {
            if (id == null)
            {
                return TagNotFound();
            }

            string viewToRender = nameof(Print);
            var tagtoPrint = await _context.TblQualityIssues.FindAsync(id);

            if (tagtoPrint == null)
            {
                return TagNotFound();
            }

            if (editMode)
            {
                tagtoPrint.Date = DateTime.Now;
                // tagtoPrint.LengthOfChange = tagtoPrint.LengthOfChange > 0 ? tagtoPrint.LengthOfChange : 1.0;

                if (tagtoPrint.IsInternalAlert)
                {
                    InitDropDowns_Internal();
                }

                else if (tagtoPrint.IsExternalAlert)
                {
                    InitDropDowns_External(editMode: true);
                }

                viewToRender = nameof(Edit);
            }

            InitReportView(tagtoPrint.Id, returnUrl);

            return View(viewToRender, new TagReport(tagtoPrint));
        }

        /// <summary>
        /// this is the create action for the quality issues model, it will direct the user to the create page for the new tag, and 
        /// will also list the date of creation 
        /// </summary>
        /// <param name="type"> Comes from ProblemType in QualityIssue model</param>
        /// <returns> Returns the user to the create quality tag page</returns>
        public async Task<IActionResult> Create(string type)
        {
            TblQualityIssues newTag;
            type = !string.IsNullOrEmpty((type + "").Trim().ToUpper()) ? type : "IN";
            newTag = new TblQualityIssues { ProblemType = type, Date = DateTime.Now, LengthOfChange = 1.0 };

            var problemType = await _context.TblProblemSource.FindAsync(newTag.ProblemType);

            ViewBag.ProblemSource = problemType.ProblemSource;


            if (newTag.IsInternalAlert)
            {
                newTag.QualityMemo = true;
                InitDropDowns_Internal();
            }
            else if (newTag.IsExternalAlert)
            {
                newTag.QualityAtag = true;
                InitDropDowns_External();
            }

            return View(newTag);
        }

        /// <summary>
        /// this is the post-back method for creating a quality issue, once created it will message the user with a temp data notifying the 
        /// user that the issue is created
        /// </summary>
        /// <param name="tblQualityIssues">this is one of the models in the database</param>
        /// <returns>this will return the user back to the index view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ProblemType,Date,PartId,IssuedBy,Reason,Feature,Changed,Comment,QualityAlert,QualityAlertMemo,Cost,NofPiecesQa,NofPiecesHt,MachineId,CustomerId,CustomerRefNum,DateIssued,DispositionCustomer,DispositionStackpole,LengthOfChange,OkdBy,ModWritten,SpecialInstWritten,HoldTag,TpcTag,CertTag,QualityAtag,Closed,SupplierIssue,ControlPlan,LayeredAudit,QualityMemo,FeatureNumber,CurrentMinSpec,CurrentMaxSpec,TpcMinSpec,TpcMaxSpec,OperationNumber,Body,Operation,Attachments,Qty,ActiveStatus,UpdatedAt,CreatedAt")] TblQualityIssues tblQualityIssues)
        {
            TblProblemSource problemType = await _context.TblProblemSource.FindAsync(tblQualityIssues.ProblemType);
            string tagCreator = Request.Form["tagCreator"].FirstOrDefault();

            if (string.IsNullOrEmpty(_picture01FileName))
            {
                _picture01FileName = Request.Form["photo01"].FirstOrDefault();
            }
            if (string.IsNullOrEmpty(_picture02FileName))
            {
                _picture02FileName = Request.Form["photo02"].FirstOrDefault();
            }

            string tagDescription = string.Empty;
            var picture01 = FindPictureByFileName(_context.Blobs.AsEnumerable(), (_picture01FileName + "").Trim());
            var picture02 = FindPictureByFileName(_context.Blobs.AsEnumerable(), (_picture02FileName + "").Trim());

            if (tagCreator != null)
            {
                tblQualityIssues.CreatedBy = tagCreator;
            }

            if (picture01 != null)
            {
                tblQualityIssues.Picture01 = picture01.Id;
            }

            if (picture02 != null)
            {
                tblQualityIssues.Picture02 = picture02.Id;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (problemType != null)
                    {
                        tagDescription = problemType.ProblemSource.ToLower();
                    }

                    _context.Add(tblQualityIssues);
                    await _context.SaveChangesAsync();

                    _picture01FileName = string.Empty;
                    _picture02FileName = string.Empty;
                    TempData["Message"] = $"New {tagDescription} alert: {tblQualityIssues.LongId} successfully created at {tblQualityIssues.Date}";

                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    while (exc.InnerException != null) exc = exc.InnerException;

                    ModelState.AddModelError("", $"Tag creation failed with the message: {exc.Message}.");
                }
            }

            ViewBag.ProblemSource = problemType.ProblemSource;

            if (tblQualityIssues.IsInternalAlert)
            {
                InitDropDowns_Internal();
            }
            else if (tblQualityIssues.IsExternalAlert)
            {
                InitDropDowns_External();
            }

            TempData["Message"] = "Please fix the errors below and try submitting again";

            return View(tblQualityIssues);
        }


        /// <summary>
        /// this is the edit portion of the controller, it will confirm the id of the controller and 
        /// </summary>
        /// <param name="id"> Id of the quality issue</param>
        /// <returns> A Print view with editable fields.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            string returnUrl = Request.Headers["Referer"].FirstOrDefault();
            return await Task.Run(() => RedirectToAction(nameof(Print), new { id, returnUrl, editMode = true }));
        }

        /// <summary>
        /// This is the post-back method for the edit controller, it will either update and save the correct information, or notify the user
        /// with temp variables pointing to where the user made a mistake
        /// </summary>
        /// <param name="id"> Id of the </param>
        /// <param name="returnUrl"> Id of the </param>
        /// <param name="tblQualityIssues"></param>
        /// <returns> On success, the Index view of this controller</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, string returnUrl, [Bind("Id,ProblemType,Date,PartId,IssuedBy,Reason,Feature,Changed,Comment,QualityAlert,QualityAlertMemo,Cost,NofPiecesQa,NofPiecesHt,MachineId,CustomerId,CustomerRefNum,DateIssued,DispositionCustomer,DispositionStackpole,LengthOfChange,OkdBy,ModWritten,SpecialInstWritten,HoldTag,TpcTag,CertTag,QualityAtag,Closed,SupplierIssue,ControlPlan,LayeredAudit,QualityMemo,FeatureNumber,CurrentMinSpec,CurrentMaxSpec,TpcMinSpec,TpcMaxSpec,OperationNumber,Body,Operation,Attachments,Qty,ActiveStatus,UpdatedAt,CreatedAt")] TblQualityIssues tblQualityIssues)
        {
            if (id != tblQualityIssues.Id)
            {
                return TagNotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblQualityIssues);
                    await _context.SaveChangesAsync();

                    TempData["Message"] = $"Successfully updated tag {tblQualityIssues.LongId}";
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception exc)
                {
                    if (!TblQualityIssuesExists(tblQualityIssues.Id))
                    {
                        return TagNotFound();
                    }
                    else
                    {
                        while (exc.InnerException != null) exc = exc.InnerException;
                        ModelState.AddModelError("", $"Tag update failed with the message: {exc.Message}.");
                    }
                }
            }

            if (tblQualityIssues.IsInternalAlert)
            {
                InitDropDowns_Internal();
            }
            else if (tblQualityIssues.IsExternalAlert)
            {
                InitDropDowns_External(editMode: true);
            }

            InitReportView(tblQualityIssues.Id, returnUrl);

            TempData["Message"] = "Please fix the errors below and try submitting again";

            return View(new TagReport(tblQualityIssues));
        }

        /// <summary>
        /// this is the delete action on the controller, it will first find if the id exist, and once confirmed, it will direct the user to the delete 
        /// view
        /// </summary>
        /// <param name="id"> Id of the quality issue</param>
        /// <returns> The Delete preview page.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return TagNotFound();
            }

            var tblQualityIssues = await _context.TblQualityIssues
                .FirstOrDefaultAsync(m => m.Id == id);
            if (tblQualityIssues == null)
            {
                return TagNotFound();
            }

            return View(tblQualityIssues);
        }

        /// <summary>
        /// This is the post-back method in the delete view. It will Remove and update the index if the item is successfully deleted
        /// </summary>
        /// <param name="id"></param>
        /// <returns> Redirect back to the Index page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblQualityIssues = await _context.TblQualityIssues.FindAsync(id);
            _context.TblQualityIssues.Remove(tblQualityIssues);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Returns true when the id exist for the quality issue
        /// </summary>
        /// <param name="id"> Id of tag to find.</param>
        /// <returns> True if tag exists.</returns>
        private bool TblQualityIssuesExists(int id)
        {
            return _context.TblQualityIssues.Any(e => e.Id == id);
        }

        /// <summary>
        /// This is a method created for error handling, so the user is not taken to a 404 page
        /// </summary>
        /// <returns> Redirect back to the Index page.</returns>
        private IActionResult TagNotFound()
        {
            TempData["Message"] = "Error locating tag. Try searching again.";
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// For the Internal Issues form, populates ViewData fields bound to form input controls.
        /// </summary>
        private void InitDropDowns_Internal()
        {
            ViewBag.PartList = _context.Parts;
            ViewBag.ReasonList = _context.TblHtpcReasons.OrderBy(r => r.Reason);
            ViewBag.ProcessList = _context.Depts.OrderBy(pr => pr.Department);
            ViewBag.MachineList = _context.Machines;
        }

        /// <summary>
        /// For the External Issues form, populates ViewData fields bound to form input controls.
        /// </summary>
        private void InitDropDowns_External(bool editMode = false)
        {
            ViewBag.CustomerList =
                _context.TblCustomers
                .AsEnumerable()
                .Where(c => c.Current.GetValueOrDefault())
                .Select(c =>
                new TblCustomers()
                {
                    CustomerId = c.CustomerId,
                    CustName = $"{c.CustName}{(!string.IsNullOrEmpty(c.CustLocation) ? " \u2013 " : "")}{c.CustLocation}",
                    Current = c.Current
                })
                .OrderBy(c => c.CustName);

            ViewBag.CustomerDispositions =
                _context.TblCustomerDispositions
                .AsEnumerable()
                .Where(c => c.CUSTOMER_DISPOSITION_IDS.Any(d => d == c.Id))
                .OrderBy(c => c.Disposition);

            ViewBag.StackpoleDispositions =
                _context.TblCustomerDispositions
                .AsEnumerable()
                .Where(c => c.STACKPOLE_DISPOSITION_IDS.Any(d => d == c.Id))
                .OrderBy(c => c.Disposition);

            ViewBag.ReasonList = _context.TblHtpcReasons.OrderBy(r => r.Reason);
            ViewBag.ProcessList = _context.Depts.OrderBy(pr => pr.Department);
            ViewBag.MachineList = _context.Machines;

            if (editMode)
            {
                ViewBag.PartList = _context.Parts;
            }
        }

        /// <summary>
        /// For the Print view, queries local storage for attached files and sets the target of the Back navigation button.
        /// </summary>
        /// <param name="tagId"> Id of a tag with pictures or file attachments.</param>
        /// <param name="returnUrl"> Target of the View's back navigation button</param>
        private void InitReportView(int tagId, string returnUrl)
        {
            ViewBag.ReturnURL = string.IsNullOrEmpty(returnUrl)
                ? Request.Headers["Referer"].FirstOrDefault()
                : returnUrl;
            ViewBag.AttachmentList = from dir in _context.StorageLocations
                                     join file in _context.Attachments on dir.Id equals file.Path
                                     join tag in _context.TblQualityIssues on file.Path equals tag.Attachments
                                     where tag.Id == tagId
                                     select Tuple.Create(
                                             Path.Combine(Path.GetFileNameWithoutExtension(dir.Path), file.Key), file.Filename);
            ViewBag.PictureData = (from dir in _context.StorageLocations
                                   join pic in _context.Blobs on dir.Id equals pic.Path
                                   join tag in _context.TblQualityIssues on pic.Id equals tag.Picture01
                                   where tag.Id == tagId
                                   select Tuple.Create(
                                           Path.Combine(Path.GetFileNameWithoutExtension(dir.Path), pic.Key), pic.Filename))
                                 .AsEnumerable()
                                 .Concat(
                                    from dir in _context.StorageLocations
                                    join pic in _context.Blobs on dir.Id equals pic.Path
                                    join tag in _context.TblQualityIssues on pic.Id equals tag.Picture02
                                    where tag.Id == tagId
                                    select Tuple.Create(
                                            Path.Combine(Path.GetFileNameWithoutExtension(dir.Path), pic.Key), pic.Filename));
        }
    }
}
