﻿/**
 * CustomersController.cs
 * 
 * Provides a CRUD interface for managing Customer domain objects.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.15: Created
 *     Xinghua Li, 2020.04.09: Implemented Contacts Action
 *     Bao Khanh Nguyen, 2020.04.10 : Sorters Actions
 */

using HoldTagSignOff.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using static System.String;

namespace HoldTagSignOff.Controllers
{

    public class CustomersController : Controller
    {
        private readonly QualityTagsContext _context;

        public CustomersController(QualityTagsContext context)
        {
            _context = context;
        }


        // Route: Home -> Customer -> Contact Info
        [Authorize(Roles = "admin, office, creator")]
        public async Task<IActionResult> Contacts()
        {
            // a LINQ query for customer information
            // LEFT JOINs are simulated by `<table>.DefaultIfEmpty()`
            // see https://docs.microsoft.com/en-us/dotnet/csharp/linq/perform-left-outer-joins
            var customerContacts = (from customer in _context.TblCustomers
                                    join contact in _context.TblCustomerContacts.DefaultIfEmpty()
                                        on customer.CustomerId equals contact.CustomerId
                                    join salerep in _context.TblCustomerSalesReps.DefaultIfEmpty()
                                        on customer.CustomerId equals salerep.CustomerId
                                    join sorter in _context.TblCustomerSorters.DefaultIfEmpty()
                                        on customer.CustomerId equals sorter.CustomerId
                                    select new CustomerInfo()
                                    {
                                        CustomerId = customer.CustomerId,
                                        CustName = customer.CustName,
                                        CustLocation = customer.CustLocation,
                                        Current = customer.Current,

                                        ContactFirstName = contact.ContactFirstName,
                                        ContactLastName = contact.ContactLastName,
                                        ContactPhoneNumber = contact.PhoneNumber,
                                        ContactCellNumber = contact.CellNumber,

                                        SalesRepFirstName = salerep.SalesRepFirstName,
                                        SalesRepLastName = salerep.SalesRepLastName,
                                        SalesRepPhoneNumber = salerep.SalesRepPhoneNumber,

                                        SortCompanyName = sorter.SortCompanyName,
                                        SortContactFirstName = sorter.SortContactFirstName,
                                        SortContactLastName = sorter.SortContactLastName,
                                        SortPhoneNumber = sorter.SortPhoneNumber
                                    })
                                    .AsEnumerable()
                                    .Where(c => c.Current.GetValueOrDefault() && !IsNullOrEmpty(c.ContactFirstName))
                                    .OrderBy(c => c.CustName)
                                    .ThenBy(c => c.CustLocation)
                                    .GroupBy(c => c.CustName)
                                    .Select(cc => cc)
                                    .ToList();

            return View(await Task.Run(() => customerContacts));
        }

        // Route: Home -> Customer -> Sorter Info
        [Authorize(Roles = "admin, office, creator")]
        public async Task<IActionResult> Sorters()
        {
            var custSorter = (from customer in _context.TblCustomers
                              join contact in _context.TblCustomerContacts
                                  on customer.CustomerId equals contact.CustomerId
                              join salerep in _context.TblCustomerSalesReps
                                  on customer.CustomerId equals salerep.CustomerId
                              join sorter in _context.TblCustomerSorters
                                  on customer.CustomerId equals sorter.CustomerId
                              //  where (customer.Current == true) && (contact.ContactFirstName != null)
                              select new CustSorter()
                              {
                                  CustomerId = customer.CustomerId,
                                  CustName = customer.CustName,
                                  CustLocation = customer.CustLocation,
                                  SortCompanyName = sorter.SortCompanyName,
                                  SortContactFirstName = sorter.SortContactFirstName,
                                  SortContactLastName = sorter.SortContactLastName,
                                  SortPhoneNumber = sorter.SortPhoneNumber,
                                  Current = customer.Current
                              })
                            .AsEnumerable()
                            .Where(c => c.Current.GetValueOrDefault() && !IsNullOrEmpty(c.SortContactFirstName))
                            .OrderBy(c => c.CustName)
                            .ThenBy(c => c.CustLocation)
                            .GroupBy(c => c.CustName)
                            .Select(cc => cc)
                            .ToList();

            return View(await Task.Run(() => custSorter));
        }

        /// <summary>
        /// Lists all external issues, with associated customer details
        /// </summary>
        public async Task<IActionResult> CustomerIssues()
        {
            var customerList = _context.TblCustomers.Select(c =>
                new TblCustomers() { CustomerId = c.CustomerId, CustName = c.CustName, CustLocation = c.CustLocation });

            var customerPartList = from tag in _context.TblQualityIssues
                                   join cust in customerList.DefaultIfEmpty() on tag.CustomerId equals cust.CustomerId
                                   join part in _context.Parts on tag.PartId equals part.PartId
                                   select part;

            var issues = (from tag in _context.TblQualityIssues
                          join cust in customerList on tag.CustomerId equals cust.CustomerId
                          join part in customerPartList on tag.PartId equals part.PartId
                          join plant in _context.TblPlantNames on part.PlantNumber equals plant.PlantId
                          where tag.ProblemType.Equals("EX", StringComparison.OrdinalIgnoreCase)
                          select new CustomerIssue(tag, cust, plant))
                          .AsEnumerable()
                          .OrderByDescending(i => i.DateIssued);

            return View(await Task.Run(() => issues));
        }


        /*
         * ******************************************************************************
         * ****************************** NOT IMPLEMENTTED  *****************************
         * ******************************************************************************
         */
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CustomerId,CustName,CustLocation,Current")] TblCustomers tblCustomers)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblCustomers);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblCustomers);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblCustomers = await _context.TblCustomers.FindAsync(id);
            if (tblCustomers == null)
            {
                return NotFound();
            }
            return View(tblCustomers);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CustomerId,CustName,CustLocation,Current")] TblCustomers tblCustomers)
        {
            if (id != tblCustomers.CustomerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblCustomers);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblCustomersExists(tblCustomers.CustomerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblCustomers);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblCustomers = await _context.TblCustomers
                .FirstOrDefaultAsync(m => m.CustomerId == id);
            if (tblCustomers == null)
            {
                return NotFound();
            }

            return View(tblCustomers);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblCustomers = await _context.TblCustomers.FindAsync(id);
            _context.TblCustomers.Remove(tblCustomers);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblCustomersExists(int id)
        {
            return _context.TblCustomers.Any(e => e.CustomerId == id);
        }
    }
}
