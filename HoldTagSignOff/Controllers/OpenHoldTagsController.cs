﻿/**
 * OpenHoldTagsController.cs
 * 
 * @brief 
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.12: Created
 */

using Microsoft.AspNetCore.Mvc;
using HoldTagSignOff.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoldTagSignOff.Controllers
{
    [Route("api/OpenTagFinder")]
    [ApiController]
    public class OpenHoldTagsController : ControllerBase
    {
        private readonly QualityTagsContext _context;

        /// <summary>
        /// Injects a reference to the underlying database into a new OpenHoldTagsController.
        /// </summary>
        /// <param name="context"></param>
        public OpenHoldTagsController(QualityTagsContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Handles AJAX requests from the form API.
        /// </summary>
        /// <param name="openTag"> A JSON object mapping of a <see cref="TblQualityIssues"/> model instance.</param>
        [HttpPost]
        public async Task<JsonResult> GetOpenTag(TblQualityIssues openTag)
        {
            TblQualityIssues selectedTag = null;

            int tagId = openTag != null ? openTag.Id : 0;

            if (tagId > 0)
            {
                selectedTag = _context.TblQualityIssues.Find(tagId);
            }

            return await Task.Run(() => new JsonResult(selectedTag) { ContentType = "application/json; charset=utf-8" });
        }
    }
}
