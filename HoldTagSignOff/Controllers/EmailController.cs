﻿/**
 * EmailsController.cs
 * 
 * Adapted from Hasan A Yousef and Glorfindel's answer to https://stackoverflow.com/questions/40912375/return-view-as-string-in-net-core
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.02: Created
 */

using HoldTagSignOff.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace HoldTagSignOff.Controllers
{
    [AllowAnonymous]
    public class EmailController : ControllerBase
    {
        private readonly IViewRenderService _viewRenderService;

        public EmailController(IViewRenderService viewRenderService)
        {
            _viewRenderService = viewRenderService;
        }

        public async Task<string> FetchBody<TModel>(string viewName, TModel model)
        {
            return await _viewRenderService.RenderToStringAsync(viewName, model);
        }
    }
}
