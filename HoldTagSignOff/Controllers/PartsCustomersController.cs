﻿/**
 * PartsCustomersController.cs
 * 
 * Utility controller for rapid queries on the PartsCustomers join table
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.18: Created
 *     Chackaphope Ying Yong, 2020.04.10: Added documentation 
 */

using Microsoft.AspNetCore.Mvc;
using HoldTagSignOff.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoldTagSignOff.Controllers
{
    [Route("api/CustomerPartFinder")]
    [ApiController]
    public class PartsCustomersController : ControllerBase
    {
        private readonly QualityTagsContext _context;

        /// <summary>
        /// Injects a reference to the underlying database into a new PartsCustomersController.
        /// </summary>
        /// <param name="context"></param>
        public PartsCustomersController(QualityTagsContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Handles AJAX requests from the form API. It helps join the customer and parts together so both
        /// tables can work together in sync, e.q. customer wants this part...
        /// </summary>
        /// <param name="customer"> A JSON object mapping of a <see cref="TblCustomers"/> model instance.</param>
        [HttpPost]
        public async Task<JsonResult> GetPartsForCustomer(TblCustomers customer)
        {
            StringBuilder dataList = new StringBuilder();
            int custId = customer != null ? customer.CustomerId : 0;

            if (custId > 0)
            {
                var customerParts = _context.TblPartsCustomer
                    .AsEnumerable()
                    .Where(c => c.CustId == custId)
                    .OrderBy(p => p.PartId);

                foreach (TblPartsCustomer part in customerParts)
                {
                    dataList.AppendLine($"<option data-value=\"{part.PartId}\" value=\"{part.PartId}\" />");
                }
            }

            return await Task.Run(() => new JsonResult(dataList.ToString()) { ContentType = "application/json; charset=utf-8" });
        }
    }
}
