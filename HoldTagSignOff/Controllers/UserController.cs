﻿/**
 * UserController.cs
 * 
 * User management controller.
 *
 * Revision History
 *     Robert Di Pardo, 2020.04.07: Created
 */

using HoldTagSignOff.Models;
using HoldTagSignOff.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace HoldTagSignOff.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserController : Controller
    {
        private static readonly string ADMIN_ROLE_NAME = "admin";
        private readonly IEmailSender emailSender;
        private readonly IViewRenderService viewRenderService;
        private readonly UserManager<IdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly List<IdentityUser> admins;
        private readonly List<IdentityUser> office;
        private readonly List<IdentityUser> creators;

        // Instantiates a new UserController from the given UserManager and RoleManager
        public UserController(UserManager<IdentityUser>
            userManager,
            RoleManager<IdentityRole> roleManager,
            IEmailSender emailSender,
            IViewRenderService viewRenderService)
        {
            this.emailSender = emailSender;
            this.viewRenderService = viewRenderService;
            this.userManager = userManager;
            this.roleManager = roleManager;
            admins = userManager.GetUsersInRoleAsync(ADMIN_ROLE_NAME).Result.ToList();
            office = userManager.GetUsersInRoleAsync("office").Result.ToList();
            creators = userManager.GetUsersInRoleAsync("creator").Result.ToList();
        }

        // Retrieves a formatted listing of current user attributes 
        public async Task<IActionResult> Index(int? orderByLocked)
        {
            var users = new List<UserDetail>();

            foreach (var user in userManager.Users)
            {
                users.Add(new UserDetail()
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    Email = user.Email,
                    HasConfirmedEmail = user.EmailConfirmed,
                    IsAdmin = admins.Any(m => m.Id == user.Id),
                    IsOffice = office.Any(m => m.Id == user.Id),
                    IsCreator = creators.Any(m => m.Id == user.Id),
                    IsLockedOut = user.LockoutEnd >= DateTime.UtcNow
                });
            }

            if (!orderByLocked.HasValue)
            {
                users =
                    users.Where(u => u.IsAdmin).OrderBy(u => u.UserName)
                         .Concat(users.Where(u => !u.IsAdmin).OrderBy(u => u.UserName))
                         .ToList();

                // toggle custom ordering
                ViewBag.orderByLocked = 1;
            }
            else
            {
                users =
                    users.Where(u => u.IsLockedOut).OrderBy(u => u.UserName)
                        .Concat(users.Where(u => !u.IsLockedOut).OrderBy(u => u.UserName))
                        .ToList();

                ViewBag.orderByLocked = null;
            }

            return View(await Task.Run(() => users));
        }

        // Lock or unlocks the user with the given id, if any
        public async Task<IActionResult> ChangeLockStatus(string id)
        {
            IdentityResult result;
            var user = await userManager.FindByIdAsync(id);

            if (user == null)
            {
                TempData["Message"] = "An unknown user attempted to change a lockout status!";
                return RedirectToAction(nameof(Index));
            }

            if (await userManager.IsLockedOutAsync(user))
            {
                result =
                    await userManager.SetLockoutEndDateAsync(user, DateTime.Now.ToUniversalTime());
                TempData["Message"] = $"{user.UserName} is now unlocked";
            }
            else
            {
                result =
                    await userManager.SetLockoutEndDateAsync(user, DateTime.Now.AddYears(20).ToUniversalTime());
                TempData["Message"] =
                    $"{user.UserName} will be locked out until " +
                    $"{user.LockoutEnd.Value.ToLocalTime():MMM dd yyyy}";
            }

            if (!result.Succeeded)
            {
                TempData["Message"] =
                    $"Changing lock status failed with the message: " +
                    $"{result.Errors.FirstOrDefault().Description}";
            }

            return RedirectToAction(nameof(Index));
        }

        // Grants the "members" role to the user with the given id, if any
        public async Task<IActionResult> ChangeMembership(string id, string role)
        {
            string targetRole = role?.Trim();

            if (string.IsNullOrEmpty(role))
            {
                TempData["Message"] = "Error: bad request.";
            }
            else
            {
                IdentityResult result;
                var user = await userManager.FindByIdAsync(id);

                if (user == null)
                {
                    TempData["Message"] = "Error: can't change role of unknown user";
                }
                else if (!await roleManager.RoleExistsAsync(targetRole))
                {
                    TempData["Message"] = $"Error: role ${targetRole.ToUpper()} does not exist!";
                }
                else
                {
                    if (await userManager.IsInRoleAsync(user, targetRole))
                    {
                        result = await userManager.RemoveFromRoleAsync(user, targetRole);
                        TempData["Message"] = $"{user.UserName} was removed from {targetRole}.";

                        if (!result.Succeeded)
                        {
                            TempData["Message"] =
                                $"Removing {user.UserName} from {targetRole} failed with the message: " +
                                $"{result.Errors.FirstOrDefault().Description}";
                        }
                    }
                    else
                    {
                        result = await userManager.AddToRoleAsync(user, targetRole);
                        TempData["Message"] = $"{user.UserName} was added to {targetRole}.";

                        if (!result.Succeeded)
                        {
                            TempData["Message"] =
                                $"Adding {user.UserName} to {targetRole} failed with the message:\r\n" +
                                $"{result.Errors.FirstOrDefault().Description}";
                        }
                    }
                }
            }


            return RedirectToAction(nameof(Index));
        }

        // Revokes all roles from the user with the given id, if any, and a deletes the user
        public async Task<IActionResult> Delete(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    TempData["Message"] = "Error: bad request or unknown user.";
                }
                else
                {
                    var user = await userManager.FindByIdAsync(id);

                    if (user == null)
                    {
                        TempData["Message"] = "Error: can't remove unknown user!";
                    }
                    else
                    {
                        var ownedRoles = await userManager.GetRolesAsync(user);

                        IdentityResult result =
                             await userManager.RemoveFromRolesAsync(user, ownedRoles);

                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.FirstOrDefault().Description);
                        }

                        result = await userManager.DeleteAsync(user);

                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.FirstOrDefault().Description);
                        }

                        TempData["Message"] = $"User '{user.UserName}' was deleted.";
                    }
                }
            }
            catch (Exception exc)
            {
                TempData["Message"] = $"Deleting user failed with the message: {exc.Message}";

                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction(nameof(Index));
        }

        // Serves and HTML form into which a new password can be entered for the user with the given id, if any
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string id, string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            var user = await userManager.FindByIdAsync(id);

            if (user != null)
            {
                return View(new PasswordUpdate() { Email = user.Email });
            }

            return View(new PasswordUpdate());
        }

        // Resets the password of the user with the given id, if any
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([Bind("Email,NewPassword,ConfirmNewPassword")] PasswordUpdate user, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (string.IsNullOrEmpty(user.Email))
                    {
                        throw new Exception("Bad request: password was no changed.");
                    }
                    else
                    {
                        var foundUser = await userManager.FindByEmailAsync(user.Email);

                        if (foundUser == null)
                        {
                            throw new Exception("Can't change password of unknown user!");
                        }
                        else
                        {
                            IdentityResult result;

                            if ((result = await userManager.RemovePasswordAsync((IdentityUser)foundUser)).Succeeded)
                            {
                                result = await userManager.AddPasswordAsync((IdentityUser)foundUser, (string)user.NewPassword);
                            }
                            else
                            {
                                throw new Exception(result.Errors.FirstOrDefault().Description);
                            }

                            if (!result.Succeeded)
                            {
                                throw new Exception(result.Errors.FirstOrDefault().Description);
                            }
                        }

                        if (await UrTheBoss())
                        {
                            TempData["Message"] = $"'{foundUser.UserName}''s password was changed successfully";
                        }
                        else
                        {
                            var userId = await userManager.GetUserIdAsync(foundUser);
                            var code = await userManager.GeneratePasswordResetTokenAsync(foundUser);
                            var callbackUrl = Url.Page(
                                            "/Account/ResetPasswordConfirmation",
                                            pageHandler: null,
                                            values: new { area = "Identity" },
                                            protocol: Request.Scheme);
                            string confirmationUrl = HtmlEncoder.Default.Encode(callbackUrl);

                            var viewModel = new UserRegistration() { UserName = foundUser.UserName, Email = foundUser.Email };
                            string body = await new EmailController(viewRenderService)
                                            .FetchBody("Emails/ChangePassword", viewModel);

                            // WARNING 
                            // Don't try to render the `confirmationUrl` directly in the body of the email View:
                            // it won't be valid 
                            body = body.Replace("href=\"#\"", $"href={confirmationUrl}", StringComparison.OrdinalIgnoreCase);

                            await emailSender.SendEmailAsync(foundUser.Email, "Confirm your new password", body);

                            TempData["Message"] = $"Confirmation link to change password sent to {foundUser.Email}.";
                        }

                    }
                }
                catch (Exception exc)
                {
                    TempData["Message"] = $"Changing password failed with the message: {exc.Message}";

                    if (await UrTheBoss())
                    {

                        return RedirectToAction(nameof(Index));
                    }

                }

                if (await UrTheBoss())
                {

                    return RedirectToAction(nameof(Index));
                }
            }


            if (await UrTheBoss())
            {

                return View(user);
            }

            return Redirect(returnUrl);

            async Task<bool> UrTheBoss()
            {
                var whoRU = await userManager.FindByEmailAsync(user.Email);
                return await userManager.IsInRoleAsync(whoRU, ADMIN_ROLE_NAME);
            }
        }
    }
}