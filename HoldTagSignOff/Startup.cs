/**
 * Startup.cs
 * 
 * Global service registration 
 * 
 */

using HoldTagSignOff.Models;
using HoldTagSignOff.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
namespace HoldTagSignOff
{
    public class Startup
    {
        /// <summary>
        /// Instantiates a new <see cref="Startup"/> from the given <see cref="IConfiguration"/>.
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Gets the <see cref="IConfiguration"/> of this <see cref="Startup"/> instance.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        ///  Use this method to add services to the application container.
        /// </summary>
        /// <param name="services">Collects the service descriptors required by the application.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // connect the databases
            services.AddDbContext<QualityTagsContext>(options =>
               options.UseMySql(
                   Configuration.GetConnectionString("DefaultConnection"),
                   options => options.EnableRetryOnFailure(3)));

            services.AddDbContext<UsersContext>(options =>
               options.UseMySql(
                   Configuration.GetConnectionString("IdentityStoresConnection"),
                   options => options.EnableRetryOnFailure(3)));


            // enable storage of session data
            services.AddSession();

            // enable caching of session data
            services.AddDistributedMemoryCache();

            // use Razor templates for all .cshtml (View) files
            services.AddRazorPages();

            // use EntityFramework to build Controllers and Views
            services.AddControllersWithViews()
                .AddRazorRuntimeCompilation(); // enable file watching
                                               // -- requires an extra library as of .NET Core 3.0:
                                               // see https://docs.microsoft.com/en-us/aspnet/core/mvc/views/view-compilation?view=aspnetcore-3.0#runtime-compilation

            // set the global options for new user accounts
            var userOptions = new UserOptions()
            {
                AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_~!@#$%&*.+",
                RequireUniqueEmail = true
            };

            var passwordOptions = new PasswordOptions()
            {
                RequiredLength = 1,
                RequireLowercase = false,
                RequireUppercase = false,
                RequireDigit = false,
                RequireNonAlphanumeric = false,
            };

            var lockoutOptions = new LockoutOptions()
            {
                AllowedForNewUsers = true,
                DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5),
                MaxFailedAccessAttempts = 5
            };

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                options.SignIn.RequireConfirmedAccount = true;
                options.User = userOptions;
                options.Password = passwordOptions;
                options.Lockout = lockoutOptions;
            })
            .AddRoles<IdentityRole>() // create roles for users to belong to
            .AddDefaultTokenProviders()
            .AddEntityFrameworkStores<UsersContext>();  // configure the database where user accounts will be stored

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Identity/Account/Login";
                options.LogoutPath = "/Identity/Account/Logout";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
            });

            // register email service
            services.AddSingleton<ISmtpConfiguration>(Configuration.GetSection("EmailConfiguration").Get<SmtpConfiguration>());
            services.AddTransient<IEmailSender, EmailService>();

            // register support for rendering View content programmatically,
            // i.e., when we need to fill out an e-mail
            services.AddTransient<IViewRenderService, ViewRenderService>();
        }

        /// <summary>
        ///  Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Provides the mechanisms to configure an application's request pipeline.</param>
        /// <param name="env">Provides information about the web hosting environment an application is running in.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else if (env.IsProduction())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. 
                // You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
