﻿/**
 * EmailService.cs
 * 
 * Implementation of the IEmailSender interface.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.13: Created
 *     "    "         , 2020.04.05-09: Injected logger, rewrote #Connect to circumvent any MailKit.Security.SslHandshakeException
 *     Chackaphope Ying Yong, 2020.04.10: Added documentation 
 */

using HoldTagSignOff.Models;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Text;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace HoldTagSignOff.Services
{
    public class EmailService : IEmailSender
    {
        private readonly ISmtpConfiguration _smtpConfiguration;
        private readonly ILogger<EmailService> _logger;
        private readonly EmailAddress _sender;


        /// <summary>
        /// Injects a reference to encapsulated configuration values and a console logger to aid debugging.
        /// </summary>
        /// <param name="smtpConfiguration"> An instance of the <see cref="ISmtpConfiguration"/> interface.</param>
        /// <param name="logger"> An instance of the <see cref="ILogger"/> interface.</param>
        public EmailService(ISmtpConfiguration smtpConfiguration, ILogger<EmailService> logger)
        {
            _logger = logger;
            _smtpConfiguration = smtpConfiguration;
            _sender = new EmailAddress()
            {
                Name = _smtpConfiguration.MailerName,
                Address = _smtpConfiguration.SmtpUsername ?? _smtpConfiguration.MailerAddress
            };
        }


        /// <summary>
        /// Implements the only method provided by the built-in <see cref="IEmailSender" /> interface.
        /// This method is called by every default action defined in the <see cref="Microsoft.AspNetCore.Identity.UI"/> namespace.
        /// It can also be called programmatically.
        /// </summary>
        /// <param name="toAddress"> Email recipient's address.</param>
        /// <param name="subject"> Subject of the email message.</param>
        /// <param name="message"> Content of the email message.</param>
        /// <returns></returns>
        public Task SendEmailAsync(string toAddress, string subject, string message)
        {
            Email emailMessage = new Email()
            {
                Subject = subject,
                Content = message
            };

            emailMessage.SenderList.Add(_smtpConfiguration.MailerAddress);
            emailMessage.RecipientList.Add(toAddress);

            return Task.Run(() => Send(emailMessage));
        }

        /// <summary>
        /// This method runs when the email sender and recipient are confirmed. It will check for 
        /// connection services and send once everything is met.
        /// </summary>
        /// <param name="emailMessage"> Instance of the <see cref="Email"/> data class.</param>
        protected void Send(Email emailMessage)
        {
            var message = new MimeMessage();

            message.From.AddRange(emailMessage.SenderList.Select(_ => new MailboxAddress(_sender.Name, _sender.Address)));
            message.To.AddRange(emailMessage.RecipientList.Select(recipient => new MailboxAddress("", recipient)));
            message.Subject = emailMessage.Subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };

            using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
            {
                // HACK: skip validation of certificates
                emailClient.CheckCertificateRevocation = false;
                emailClient.ServerCertificateValidationCallback = (s, c, h, e) => true;

                try
                {
                    // According to the MailKit maintainers, the purpose of MailKit.Security.SecureSocketOptions.Auto is to:
                    //
                    //     Allow the MailKit.IMailService to decide which SSL or TLS options to use (default).
                    //     If the server does not support SSL or TLS, then the connection will continue
                    //     without any encryption.
                    //
                    emailClient.Connect(host: _smtpConfiguration.SmtpServer, port: _smtpConfiguration.SmtpPort, options: SecureSocketOptions.Auto);
                }
                catch (Exception exc)
                {
                    while (exc.InnerException != null) exc = exc.InnerException;
                    _logger.LogDebug(exc.Message);
                    _logger.LogDebug(exc.StackTrace);
                }
                finally
                {
                    string banner = "\tSMTP client initialized with these properties:\n" +
                                    "\t==============================================\n";
                    _logger.LogDebug(banner);

                    foreach (var prop in emailClient.GetType().GetProperties())
                    {
                        if (prop.Name == "AuthenticationMechanisms")
                            _logger.LogDebug($"Supported authentication schemes: {string.Join(", ", emailClient.AuthenticationMechanisms)}");
                        else
                            _logger.LogDebug($"{prop.Name}: {prop.GetValue(emailClient, null)}");
                    }
                }

                // clients with no authentication configured will no tolerate calls to #Authenticate 
                if (emailClient.Capabilities.HasFlag(SmtpCapabilities.Authentication))
                {
                    // HACK: Remove schemes we don't intend to support 
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                    emailClient.Authenticate(_smtpConfiguration.SmtpUsername, _smtpConfiguration.SmtpPassword);
                }

                if (emailClient.IsConnected)
                {
                    try
                    {
                        emailClient.Send(message);
                        emailClient.Disconnect(true);
                    }
                    catch (SmtpCommandException exc)
                    {
                        string notice = "Connection established, but transmission failed with:\r\n" +
                                        $"{exc.Message}\r\n" +
                                        "Please read the comments in /app/Docker/Dockerfile for guidance on\r\n" +
                                        "opening your SMTP host to requests from your Docker host's IP.";

                        throw new HttpRequestException(notice);
                    }
                }
                else
                {
                    string notice = "Failed to get a secure connection to your SMTP host.\r\n" +
                                    "See the console's debug output for more information.\r\n" +
                                    "Please note that SSL-encrypted connections are currently NOT SUPPORTED by this application.";
                    throw new HttpRequestException(notice);
                }
            }
        }
    }
}
