﻿/**
 * SmtpConfiguration.cs
 * 
 * Encapsulates configuration settings of the SMTP client.
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.13: Created
 *     "    "         , 2020.04.07: Added new properties
 */

namespace HoldTagSignOff.Services
{
    public interface ISmtpConfiguration
    {
        string SmtpServer { get; }
        int SmtpPort { get; }

        /// <summary>
        /// The name identifying e-mails from this mailer
        /// </summary>
        string MailerName { get; }

        /// <summary>
        /// This mailer's "From" address
        /// </summary>
        string MailerAddress { get; }

        /// <summary>
        /// Only used by remote relays that support authentication 
        /// </summary>
        string SmtpUsername { get; }

        /// <summary>
        /// Only used by remote relays that support authentication 
        /// </summary>
        string SmtpPassword { get; }
    }

    public class SmtpConfiguration : ISmtpConfiguration
    {
        public string SmtpServer { get; set; }
        public int SmtpPort { get; set; }
        public string MailerName { get; }
        public string MailerAddress { get; set; }
        public string SmtpUsername { get; set; }
        public string SmtpPassword { get; set; }
    }
}
