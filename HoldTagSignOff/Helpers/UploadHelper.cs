﻿/**
 * UploadHelper.cs
 * 
 * Constants, utility methods for the upload Controllers
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.02: Created
 */

using HoldTagSignOff.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HoldTagSignOff.Helpers
{
    public static class UploadHelper
    {
        public static readonly long MAX_FILE_SIZE = 10 * 1024 * 1024;
        public static readonly string[] ACCEPTED_PICTURE_TYPES = { ".jpg", ".jpeg", ".png", ".gif" };
        public static readonly string[] ACCEPTED_FILE_TYPES = { ".xls", ".xlsx", ".csv", ".pdf", ".doc", ".docx", ".odt", ".rtf", ".htm", ".html", ".xml", ".json", ".yml", ".yaml", ".txt", ".log" };

        public static Blobs FindPictureByFileName(IEnumerable<Blobs> pictures, string filename)
        {
            return pictures.FirstOrDefault(p =>
                            p.Filename
                            .Equals(filename, StringComparison.OrdinalIgnoreCase));
        }

        public static Attachments FindAttachmentByFileName(IEnumerable<Attachments> docs, string filename)
        {
            return docs.FirstOrDefault(d =>
                            d.Filename
                            .Equals(filename, StringComparison.OrdinalIgnoreCase));
        }
    }
}
