# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`namespace `[`HoldTagSignOff`](#namespaceHoldTagSignOff) | 
`namespace `[`HoldTagSignOff::Controllers`](#namespaceHoldTagSignOff_1_1Controllers) | 
`namespace `[`HoldTagSignOff::Helpers`](#namespaceHoldTagSignOff_1_1Helpers) | TagTemplate.cs
`namespace `[`HoldTagSignOff::Models`](#namespaceHoldTagSignOff_1_1Models) | ErrorViewModel.cs
`namespace `[`HoldTagSignOff::Services`](#namespaceHoldTagSignOff_1_1Services) | SmtpConfiguration.cs

# namespace `HoldTagSignOff` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`HoldTagSignOff::Program`](#classHoldTagSignOff_1_1Program) | 
`class `[`HoldTagSignOff::Startup`](#classHoldTagSignOff_1_1Startup) | 

# class `HoldTagSignOff::Program` 

## Members

# class `HoldTagSignOff::Startup` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} IConfiguration `[`Configuration`](#classHoldTagSignOff_1_1Startup_1ad6aa9a5ad2b9c278384a186fd9ad45ad) | Gets the IConfiguration of this [Startup](#classHoldTagSignOff_1_1Startup) instance.
`public inline  `[`Startup`](#classHoldTagSignOff_1_1Startup_1a06fb194d9cc6981bbfdc07f8d44e3931)`(IConfiguration configuration)` | Instantiates a new [Startup](#classHoldTagSignOff_1_1Startup) from the given IConfiguration.
`public inline void `[`ConfigureServices`](#classHoldTagSignOff_1_1Startup_1acef76b131c14a25b6954df1434cd45cc)`(IServiceCollection services)` | Use this method to add services to the application container.
`public inline void `[`Configure`](#classHoldTagSignOff_1_1Startup_1a66f849a29f5b6121fbc467e57e4b5fb2)`(IApplicationBuilder app,IWebHostEnvironment env)` | Use this method to configure the HTTP request pipeline.

## Members

#### `{property} IConfiguration `[`Configuration`](#classHoldTagSignOff_1_1Startup_1ad6aa9a5ad2b9c278384a186fd9ad45ad) 

Gets the IConfiguration of this [Startup](#classHoldTagSignOff_1_1Startup) instance.

#### `public inline  `[`Startup`](#classHoldTagSignOff_1_1Startup_1a06fb194d9cc6981bbfdc07f8d44e3931)`(IConfiguration configuration)` 

Instantiates a new [Startup](#classHoldTagSignOff_1_1Startup) from the given IConfiguration.

#### Parameters
* `configuration`

#### `public inline void `[`ConfigureServices`](#classHoldTagSignOff_1_1Startup_1acef76b131c14a25b6954df1434cd45cc)`(IServiceCollection services)` 

Use this method to add services to the application container.

#### Parameters
* `services` Collects the service descriptors required by the application.

#### `public inline void `[`Configure`](#classHoldTagSignOff_1_1Startup_1a66f849a29f5b6121fbc467e57e4b5fb2)`(IApplicationBuilder app,IWebHostEnvironment env)` 

Use this method to configure the HTTP request pipeline.

#### Parameters
* `app` Provides the mechanisms to configure an application's request pipeline.

* `env` Provides information about the web hosting environment an application is running in.

# namespace `HoldTagSignOff::Controllers` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`HoldTagSignOff::Controllers::AttachmentsController`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController) | 
`class `[`HoldTagSignOff::Controllers::BlobsController`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController) | 
`class `[`HoldTagSignOff::Controllers::CustomersController`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController) | 
`class `[`HoldTagSignOff::Controllers::HomeController`](#classHoldTagSignOff_1_1Controllers_1_1HomeController) | A base class for an MVC controller with view support.
`class `[`HoldTagSignOff::Controllers::PartsController`](#classHoldTagSignOff_1_1Controllers_1_1PartsController) | 
`class `[`HoldTagSignOff::Controllers::PartsCustomersController`](#classHoldTagSignOff_1_1Controllers_1_1PartsCustomersController) | 
`class `[`HoldTagSignOff::Controllers::QualityIssuesController`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController) | 

# class `HoldTagSignOff::Controllers::AttachmentsController` 

```
class HoldTagSignOff::Controllers::AttachmentsController
  : public Controller
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`AttachmentsController`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1a5a0bc26589fa18c4b96800a891f381aa)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context,IWebHostEnvironment host)` | Instantiates a new [AttachmentsController](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController) and initializes the save directory and upload paths
`public inline async Task< JsonResult > `[`Upload`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1a834f7f3271bf83f1e5648e20fcb81151)`()` | Handles an AJAX request containing a collection of file paths to files
`public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1af79500ca2efc2b51e6024e625bee68ba)`()` | This is the Index proportion of the controller. It draws the model from the qualityTagContext and returns a view listing the index of all the quality tag context
`public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1ad690839c85dfa0449c79895528fd3c6f)`(int? id)` | this is the details portion of the controller which sends the user to the details of the specific part they want to see the details on
`public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1a515320d04dc827fb49f6bb4762c44b62)`(int? id)` | this is the delete portion of the controller, it will find the id of the attachment and will bring the user to the delete view
`public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1afef67376085b72dca66ce7877853b8cc)`(int id)` | this is the post-back method used when the user clicks the delete button, it will remove the attachment and send the user back to the index view

## Members

#### `public inline  `[`AttachmentsController`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1a5a0bc26589fa18c4b96800a891f381aa)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context,IWebHostEnvironment host)` 

Instantiates a new [AttachmentsController](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController) and initializes the save directory and upload paths

#### Parameters
* `context` context comes from Quality tag contexts model

* `host` part of the web hosting interface

#### `public inline async Task< JsonResult > `[`Upload`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1a834f7f3271bf83f1e5648e20fcb81151)`()` 

Handles an AJAX request containing a collection of file paths to files

#### `public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1af79500ca2efc2b51e6024e625bee68ba)`()` 

This is the Index proportion of the controller. It draws the model from the qualityTagContext and returns a view listing the index of all the quality tag context

#### Returns
returns the view of the index view for this controller

#### `public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1ad690839c85dfa0449c79895528fd3c6f)`(int? id)` 

this is the details portion of the controller which sends the user to the details of the specific part they want to see the details on

#### Parameters
* `id` this is the id of the specific quality tag in the qualityTagContexts

#### Returns
view of the detail of the part they selected to view

#### `public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1a515320d04dc827fb49f6bb4762c44b62)`(int? id)` 

this is the delete portion of the controller, it will find the id of the attachment and will bring the user to the delete view

#### Parameters
* `id` id of the attachment

#### Returns
View showing details of model to be deleted.

#### `public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1AttachmentsController_1afef67376085b72dca66ce7877853b8cc)`(int id)` 

this is the post-back method used when the user clicks the delete button, it will remove the attachment and send the user back to the index view

#### Parameters
* `id` id of the attachment

# class `HoldTagSignOff::Controllers::BlobsController` 

```
class HoldTagSignOff::Controllers::BlobsController
  : public Controller
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`BlobsController`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a0ae12d776f4f3079ed0f525eae293ab3)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context,IWebHostEnvironment host)` | Instantiates a new [BlobsController](#classHoldTagSignOff_1_1Controllers_1_1BlobsController) and initializes the save directory and upload paths
`public inline async Task< JsonResult > `[`Upload`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a1a643342ed1304b8f5a83199bf88bce0)`()` | Handles an AJAX request containing a collection of file paths to photos
`public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1ad03b9e66a8814a133c4bce153ed2b3ab)`()` | This is the Index action of the controller. It draws the model from the qualityTagContext and returns a view listing the index of all the quality tag context
`public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a289c593f1e3d917e71715c9307490d25)`(int? id)` | this is the details portion of the controller which sends the user to the details of the specific blobs they want to see the details on
`public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a2dfc046ec6bee1ce40cdd85166c728af)`(int? id)` | The delete action of the controller, it will find the id of the picture and will bring the user to the delete view
`public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a757ccc7ed4b33ee1e462f960c45494e5)`(int id)` | this is the post-back method used when the user clicks the delete button, it will remove the picture and send the user back to the index view

## Members

#### `public inline  `[`BlobsController`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a0ae12d776f4f3079ed0f525eae293ab3)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context,IWebHostEnvironment host)` 

Instantiates a new [BlobsController](#classHoldTagSignOff_1_1Controllers_1_1BlobsController) and initializes the save directory and upload paths

#### Parameters
* `context` context comes from Quality tag contexts model

* `host` part of the web hosting interface

#### `public inline async Task< JsonResult > `[`Upload`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a1a643342ed1304b8f5a83199bf88bce0)`()` 

Handles an AJAX request containing a collection of file paths to photos

#### `public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1ad03b9e66a8814a133c4bce153ed2b3ab)`()` 

This is the Index action of the controller. It draws the model from the qualityTagContext and returns a view listing the index of all the quality tag context

#### Returns
returns the view of the index view for this controller

#### `public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a289c593f1e3d917e71715c9307490d25)`(int? id)` 

this is the details portion of the controller which sends the user to the details of the specific blobs they want to see the details on

#### Parameters
* `id` Id of the specific quality tag in the qualityTagContexts

#### Returns
View of the detail of the part they selected to view

#### `public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a2dfc046ec6bee1ce40cdd85166c728af)`(int? id)` 

The delete action of the controller, it will find the id of the picture and will bring the user to the delete view

#### Parameters
* `id` Id of the picture

#### Returns
View showing details of model to be deleted.

#### `public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1BlobsController_1a757ccc7ed4b33ee1e462f960c45494e5)`(int id)` 

this is the post-back method used when the user clicks the delete button, it will remove the picture and send the user back to the index view

#### Parameters
* `id` id of the picture

# class `HoldTagSignOff::Controllers::CustomersController` 

```
class HoldTagSignOff::Controllers::CustomersController
  : public Controller
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`CustomersController`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a6b061c65a2942bfdb6722533f501b61c)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context)` | 
`public inline async Task< IActionResult > `[`Sorters`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a62b0b340e382fa9f1ef459c3bda28ad1)`()` | 
`public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a0a9102cefde7e2cb5d64443578896a0d)`()` | 
`public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1ae16b9cc9e1b2fe01eee91ec204d6d9e9)`(int? id)` | 
`public inline IActionResult `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1ab23a10accba8e06049bf201bc5f89359)`()` | 
`public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1aeab34849a477f541c4ee237c5d3d288b)`(`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` tblCustomers)` | 
`public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a442a72a59355f441400de62ec4d0728a)`(int? id)` | 
`public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1aeed7df19ccba0e1f4a5904a829d0126f)`(int id,`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` tblCustomers)` | 
`public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1abe3886cac6f3e27b008f62bfd7472268)`(int? id)` | 
`public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a8775a4655cbf0bbb19526ed56dd63281)`(int id)` | 

## Members

#### `public inline  `[`CustomersController`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a6b061c65a2942bfdb6722533f501b61c)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context)` 

#### `public inline async Task< IActionResult > `[`Sorters`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a62b0b340e382fa9f1ef459c3bda28ad1)`()` 

#### `public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a0a9102cefde7e2cb5d64443578896a0d)`()` 

#### `public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1ae16b9cc9e1b2fe01eee91ec204d6d9e9)`(int? id)` 

#### `public inline IActionResult `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1ab23a10accba8e06049bf201bc5f89359)`()` 

#### `public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1aeab34849a477f541c4ee237c5d3d288b)`(`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` tblCustomers)` 

#### `public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a442a72a59355f441400de62ec4d0728a)`(int? id)` 

#### `public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1aeed7df19ccba0e1f4a5904a829d0126f)`(int id,`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` tblCustomers)` 

#### `public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1abe3886cac6f3e27b008f62bfd7472268)`(int? id)` 

#### `public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1CustomersController_1a8775a4655cbf0bbb19526ed56dd63281)`(int id)` 

# class `HoldTagSignOff::Controllers::HomeController` 

```
class HoldTagSignOff::Controllers::HomeController
  : public Controller
```  

A base class for an MVC controller with view support.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`HomeController`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1acfd77a84bb9b85d781e736db297531f1)`(ILogger< `[`HomeController`](#classHoldTagSignOff_1_1Controllers_1_1HomeController)` > logger)` | Instantiates a new [HomeController](#classHoldTagSignOff_1_1Controllers_1_1HomeController) from the given ILogger
`public inline IActionResult `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a3ec02f1495ad94ab8f80c8ad0e396c41)`()` | Serves the application's homepage.
`public inline IActionResult `[`Privacy`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a88e55fd01234c015389e2f7e9d7ba40c)`()` | Serves the privacy information page.
`public inline IActionResult `[`About`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a797fad889d8b19f3f5a61312c6d8f87a)`()` | Serves the privacy information page.
`public inline IActionResult `[`Error`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a88daa7567aa138066f69d30532e9afd4)`()` | In production mode, this action serves the error page.

## Members

#### `public inline  `[`HomeController`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1acfd77a84bb9b85d781e736db297531f1)`(ILogger< `[`HomeController`](#classHoldTagSignOff_1_1Controllers_1_1HomeController)` > logger)` 

Instantiates a new [HomeController](#classHoldTagSignOff_1_1Controllers_1_1HomeController) from the given ILogger

#### Parameters
* `logger` A generic interface for logging where the category name is derived from the specified  type name. Generally used to enable activation of a named ILogger from dependency injection.

#### `public inline IActionResult `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a3ec02f1495ad94ab8f80c8ad0e396c41)`()` 

Serves the application's homepage.

#### Returns
Defines a contract that represents the result of an action method.

#### `public inline IActionResult `[`Privacy`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a88e55fd01234c015389e2f7e9d7ba40c)`()` 

Serves the privacy information page.

#### Returns
Defines a contract that represents the result of an action method.

#### `public inline IActionResult `[`About`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a797fad889d8b19f3f5a61312c6d8f87a)`()` 

Serves the privacy information page.

#### Returns
Defines a contract that represents the result of an action method.

#### `public inline IActionResult `[`Error`](#classHoldTagSignOff_1_1Controllers_1_1HomeController_1a88daa7567aa138066f69d30532e9afd4)`()` 

In production mode, this action serves the error page.

#### Returns
Defines a contract that represents the result of an action method.

# class `HoldTagSignOff::Controllers::PartsController` 

```
class HoldTagSignOff::Controllers::PartsController
  : public Controller
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`PartsController`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a17a487c7f214957dbcceb14a83a7c3ed)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context)` | 
`public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a0d5d33229a32086ed5553a4af93a0376)`()` | 
`public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1aa202418dcaeff7b6a851377e16a32961)`(string id)` | 
`public inline IActionResult `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a33b94cfece081bebc3db075315852943)`()` | 
`public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1aba4cacf4fbfc5d417cd55baf2bbe3dc2)`(`[`Parts`](#classHoldTagSignOff_1_1Models_1_1Parts)` parts)` | 
`public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a0dcf87d65f8eebcf55d09827551b5c46)`(string id)` | 
`public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a81ee7090f01b0e9a178727003460b064)`(string id,`[`Parts`](#classHoldTagSignOff_1_1Models_1_1Parts)` parts)` | 
`public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1ad96ea3161500b4a9ae4297e506e5b4b2)`(string id)` | 
`public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a6997e837ccd0b06690a3e3cf1577f463)`(string id)` | 

## Members

#### `public inline  `[`PartsController`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a17a487c7f214957dbcceb14a83a7c3ed)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context)` 

#### `public inline async Task< IActionResult > `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a0d5d33229a32086ed5553a4af93a0376)`()` 

#### `public inline async Task< IActionResult > `[`Details`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1aa202418dcaeff7b6a851377e16a32961)`(string id)` 

#### `public inline IActionResult `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a33b94cfece081bebc3db075315852943)`()` 

#### `public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1aba4cacf4fbfc5d417cd55baf2bbe3dc2)`(`[`Parts`](#classHoldTagSignOff_1_1Models_1_1Parts)` parts)` 

#### `public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a0dcf87d65f8eebcf55d09827551b5c46)`(string id)` 

#### `public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a81ee7090f01b0e9a178727003460b064)`(string id,`[`Parts`](#classHoldTagSignOff_1_1Models_1_1Parts)` parts)` 

#### `public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1ad96ea3161500b4a9ae4297e506e5b4b2)`(string id)` 

#### `public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1PartsController_1a6997e837ccd0b06690a3e3cf1577f463)`(string id)` 

# class `HoldTagSignOff::Controllers::PartsCustomersController` 

```
class HoldTagSignOff::Controllers::PartsCustomersController
  : public ControllerBase
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`PartsCustomersController`](#classHoldTagSignOff_1_1Controllers_1_1PartsCustomersController_1ab2580a337e172e3f0dd8618e096f4b86)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context)` | 
`public inline async Task< JsonResult > `[`GetPartsForCustomer`](#classHoldTagSignOff_1_1Controllers_1_1PartsCustomersController_1a5b9530a6e8933286f5fdb9e739b1763e)`(`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` customer)` | 

## Members

#### `public inline  `[`PartsCustomersController`](#classHoldTagSignOff_1_1Controllers_1_1PartsCustomersController_1ab2580a337e172e3f0dd8618e096f4b86)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context)` 

#### `public inline async Task< JsonResult > `[`GetPartsForCustomer`](#classHoldTagSignOff_1_1Controllers_1_1PartsCustomersController_1a5b9530a6e8933286f5fdb9e739b1763e)`(`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` customer)` 

# class `HoldTagSignOff::Controllers::QualityIssuesController` 

```
class HoldTagSignOff::Controllers::QualityIssuesController
  : public Controller
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`QualityIssuesController`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1abb1b9fccdb55406f39d91d229252b30f)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context,IEmailSender emailSender)` | 
`public inline IActionResult `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a5cfabca16e42b8b440c8fac3986cb0d7)`()` | This is the Index action of the controller. It draws the model from the TblQualityIssues and returns a view listing the index of all the quality tag context
`public inline async Task< IActionResult > `[`CustomerIssues`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1ab195c937d7cf862efc62b2f5674bd6f2)`()` | Lists all external issues, with associated customer details
`public inline async Task< IActionResult > `[`Print`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a1ce15f339eb37ecc9d027955b4c965e0)`(int? id,string returnUrl,bool editMode)` | Serves a single tag as static HTML or an editable form, depending on the request.
`public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a8683f11e5521fb6f328219a0e624a96b)`(string type)` | this is the create action for the quality issues model, it will direct the user to the create page for the new tag, and will also list the date of creation
`public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a55c49d5e084ba8835a3e72dde4c22fd7)`(`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tblQualityIssues)` | this is the post-back method for creating a quality issue, once created it will message the user with a temp data notifying the user that the issue is created
`public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a7b420fce8a88a2e2d82bb9fe6b85e8e1)`(int? id)` | this is the edit portion of the controller, it will confirm the id of the controller and
`public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a44eecdb19394f83d3cccdc538d43dd7c)`(int id,string returnUrl,`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tblQualityIssues)` | This is the post-back method for the edit controller, it will either update and save the correct information, or notify the user with temp variables pointing to where the user made a mistake
`public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1adedb579bca67071ae0c7b17ad38804b7)`(int? id)` | this is the delete action on the controller, it will first find if the id exist, and once confirmed, it will direct the user to the delete view
`public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1aa87e8693061ae11d40224dc22afa7dd8)`(int id)` | This is the post-back method in the delete view. It will Remove and update the index if the item is successfully deleted

## Members

#### `public inline  `[`QualityIssuesController`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1abb1b9fccdb55406f39d91d229252b30f)`(`[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` context,IEmailSender emailSender)` 

#### `public inline IActionResult `[`Index`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a5cfabca16e42b8b440c8fac3986cb0d7)`()` 

This is the Index action of the controller. It draws the model from the TblQualityIssues and returns a view listing the index of all the quality tag context

#### Returns
Returns the view of the index view for this controller for TblQualityIssued model

#### `public inline async Task< IActionResult > `[`CustomerIssues`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1ab195c937d7cf862efc62b2f5674bd6f2)`()` 

Lists all external issues, with associated customer details

#### `public inline async Task< IActionResult > `[`Print`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a1ce15f339eb37ecc9d027955b4c965e0)`(int? id,string returnUrl,bool editMode)` 

Serves a single tag as static HTML or an editable form, depending on the request.

#### Parameters
* `id` Id of tag to show.

* `returnUrl` The value to be passed to the back navigation button.

* `editMode` Whether of not fields should be editable.

#### Returns
A static page or a form.

#### `public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a8683f11e5521fb6f328219a0e624a96b)`(string type)` 

this is the create action for the quality issues model, it will direct the user to the create page for the new tag, and will also list the date of creation

#### Parameters
* `type` Comes from ProblemType in QualityIssue model

#### Returns
Returns the user to the create quality tag page

#### `public inline async Task< IActionResult > `[`Create`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a55c49d5e084ba8835a3e72dde4c22fd7)`(`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tblQualityIssues)` 

this is the post-back method for creating a quality issue, once created it will message the user with a temp data notifying the user that the issue is created

#### Parameters
* `tblQualityIssues` this is one of the models in the database

#### Returns
this will return the user back to the index view

#### `public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a7b420fce8a88a2e2d82bb9fe6b85e8e1)`(int? id)` 

this is the edit portion of the controller, it will confirm the id of the controller and

#### Parameters
* `id` Id of the quality issue

#### Returns
A Print view with editable fields.

#### `public inline async Task< IActionResult > `[`Edit`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1a44eecdb19394f83d3cccdc538d43dd7c)`(int id,string returnUrl,`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tblQualityIssues)` 

This is the post-back method for the edit controller, it will either update and save the correct information, or notify the user with temp variables pointing to where the user made a mistake

#### Parameters
* `id` Id of the 

* `returnUrl` Id of the 

* `tblQualityIssues` 

#### Returns
On success, the Index view of this controller

#### `public inline async Task< IActionResult > `[`Delete`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1adedb579bca67071ae0c7b17ad38804b7)`(int? id)` 

this is the delete action on the controller, it will first find if the id exist, and once confirmed, it will direct the user to the delete view

#### Parameters
* `id` Id of the quality issue

#### Returns
The Delete preview page.

#### `public inline async Task< IActionResult > `[`DeleteConfirmed`](#classHoldTagSignOff_1_1Controllers_1_1QualityIssuesController_1aa87e8693061ae11d40224dc22afa7dd8)`(int id)` 

This is the post-back method in the delete view. It will Remove and update the index if the item is successfully deleted

#### Parameters
* `id` 

#### Returns
Redirect back to the Index page.

# namespace `HoldTagSignOff::Helpers` 

TagTemplate.cs

Enumerates tag categories for simpler template selection

Revision History Robert Di Pardo, 2020.03.16: Created

TextInputExtensions.cs

Text input validation helpers

Revision History Robert Di Pardo, 2020.03.20: Created

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`enum `[`TagTemplate`](#namespaceHoldTagSignOff_1_1Helpers_1ad8c99101fcfc753e521feb61c78db519)            | 
`class `[`HoldTagSignOff::Helpers::TextInputExtensions`](#classHoldTagSignOff_1_1Helpers_1_1TextInputExtensions) | 

## Members

#### `enum `[`TagTemplate`](#namespaceHoldTagSignOff_1_1Helpers_1ad8c99101fcfc753e521feb61c78db519) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
None            | 
QualityAlertInternal            | 
QualityAlertExternal            | 
Hold            | 
TPC            | 
Special            | 

# class `HoldTagSignOff::Helpers::TextInputExtensions` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------

## Members

# namespace `HoldTagSignOff::Models` 

ErrorViewModel.cs

Default encapsulation scheme for error messages

EmailAddress.cs

Encapsulates the details of an e-mail sender or recipient.

Revision History Robert Di Pardo, 2020.03.13: Created

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`HoldTagSignOff::Models::Attachments`](#classHoldTagSignOff_1_1Models_1_1Attachments) | 
`class `[`HoldTagSignOff::Models::Blobs`](#classHoldTagSignOff_1_1Models_1_1Blobs) | 
`class `[`HoldTagSignOff::Models::CustomerIssue`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue) | 
`class `[`HoldTagSignOff::Models::Depts`](#classHoldTagSignOff_1_1Models_1_1Depts) | 
`class `[`HoldTagSignOff::Models::Email`](#classHoldTagSignOff_1_1Models_1_1Email) | 
`class `[`HoldTagSignOff::Models::EmailAddress`](#classHoldTagSignOff_1_1Models_1_1EmailAddress) | 
`class `[`HoldTagSignOff::Models::ErrorViewModel`](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel) | Provides a customized View for reporting errors to the user.
`class `[`HoldTagSignOff::Models::Machines`](#classHoldTagSignOff_1_1Models_1_1Machines) | 
`class `[`HoldTagSignOff::Models::Parts`](#classHoldTagSignOff_1_1Models_1_1Parts) | 
`class `[`HoldTagSignOff::Models::QualityIssuesMetadata`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata) | 
`class `[`HoldTagSignOff::Models::QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext) | 
`class `[`HoldTagSignOff::Models::StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations) | 
`class `[`HoldTagSignOff::Models::TagReport`](#classHoldTagSignOff_1_1Models_1_1TagReport) | 
`class `[`HoldTagSignOff::Models::TblCustomerDispositions`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions) | 
`class `[`HoldTagSignOff::Models::TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers) | 
`class `[`HoldTagSignOff::Models::TblHtpcReasons`](#classHoldTagSignOff_1_1Models_1_1TblHtpcReasons) | 
`class `[`HoldTagSignOff::Models::TblPartsCustomer`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer) | 
`class `[`HoldTagSignOff::Models::TblPlantNames`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames) | 
`class `[`HoldTagSignOff::Models::TblProblemSource`](#classHoldTagSignOff_1_1Models_1_1TblProblemSource) | 
`class `[`HoldTagSignOff::Models::TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues) | 
`class `[`HoldTagSignOff::Models::UsersContext`](#classHoldTagSignOff_1_1Models_1_1UsersContext) | Context class for the user storage database.

# class `HoldTagSignOff::Models::Attachments` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a6f3cf363022b9ba8ff8a47d4cf5af1e6) | 
`{property} int `[`Path`](#classHoldTagSignOff_1_1Models_1_1Attachments_1aab1fde4add462e7e695da0b076d6b86e) | 
`{property} string `[`Key`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a228f459211f01df0f96209458ac96a7a) | 
`{property} string `[`Filename`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a26c8cc77e4c34fc8cb4f4ae1801c17ef) | 
`{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a178f043f61e8637aad4bd3c85d4030a6) | 
`{property} `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations)` `[`PathNavigation`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a9b407d913cea4135424867591cf6b2cb) | 

## Members

#### `{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a6f3cf363022b9ba8ff8a47d4cf5af1e6) 

#### `{property} int `[`Path`](#classHoldTagSignOff_1_1Models_1_1Attachments_1aab1fde4add462e7e695da0b076d6b86e) 

#### `{property} string `[`Key`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a228f459211f01df0f96209458ac96a7a) 

#### `{property} string `[`Filename`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a26c8cc77e4c34fc8cb4f4ae1801c17ef) 

#### `{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a178f043f61e8637aad4bd3c85d4030a6) 

#### `{property} `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations)` `[`PathNavigation`](#classHoldTagSignOff_1_1Models_1_1Attachments_1a9b407d913cea4135424867591cf6b2cb) 

# class `HoldTagSignOff::Models::Blobs` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a9bb097595fd28b12f641b12d2d176b91) | 
`{property} int `[`Path`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a517e8f2cb96def42d0d33fba5dfdcc6f) | 
`{property} string `[`Key`](#classHoldTagSignOff_1_1Models_1_1Blobs_1ae2cda28d479db6a817e2730de1ff40d4) | 
`{property} string `[`Filename`](#classHoldTagSignOff_1_1Models_1_1Blobs_1afd29bf0dbc6852bb7911770da83ac97b) | 
`{property} string `[`ContentType`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a6298891ef5371a9cfe9d83166f53c4c2) | 
`{property} long `[`Bytes`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a7d15f9971927e96680c90205d4fba6da) | 
`{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a7dc72d507facf4c213b15ad288ea1ffe) | 
`{property} `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations)` `[`PathNavigation`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a2d9b37ed1b8cae68f4b5c8efec894cfe) | 

## Members

#### `{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a9bb097595fd28b12f641b12d2d176b91) 

#### `{property} int `[`Path`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a517e8f2cb96def42d0d33fba5dfdcc6f) 

#### `{property} string `[`Key`](#classHoldTagSignOff_1_1Models_1_1Blobs_1ae2cda28d479db6a817e2730de1ff40d4) 

#### `{property} string `[`Filename`](#classHoldTagSignOff_1_1Models_1_1Blobs_1afd29bf0dbc6852bb7911770da83ac97b) 

#### `{property} string `[`ContentType`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a6298891ef5371a9cfe9d83166f53c4c2) 

#### `{property} long `[`Bytes`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a7d15f9971927e96680c90205d4fba6da) 

#### `{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a7dc72d507facf4c213b15ad288ea1ffe) 

#### `{property} `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations)` `[`PathNavigation`](#classHoldTagSignOff_1_1Models_1_1Blobs_1a2d9b37ed1b8cae68f4b5c8efec894cfe) 

# class `HoldTagSignOff::Models::CustomerIssue` 

```
class HoldTagSignOff::Models::CustomerIssue
  : public HoldTagSignOff.Models.TblQualityIssues
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} new DateTime `[`Date`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a521389860174ceea1015b29f213823ae) | 
`{property} new bool `[`SpecialInstWritten`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a7f5b0305d8c6137ee298c7cd476e83ca) | 
`{property} new bool `[`QualityAtag`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1af9e536f5b0bf042fb196e8dae8cfaea0) | 
`{property} new bool `[`HoldTag`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a591d68e121a1312b8833d2be820f3ca6) | 
`{property} new bool `[`TpcTag`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a7b79386c7b5b261243666dca5856e540) | 
`{property} new bool `[`QualityMemo`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1af56c2642994d8908862e5dc0ee3cbff7) | 
`{property} string `[`CustName`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a903501602fc8652e9622e819576e40e3) | 
`{property} string `[`CustLocation`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a45e181e0d3cf571e6a4ea00377482a01) | 
`{property} string `[`PlantName`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a474c1f2d7b39c1e01eb3f95e4ba9579a) | 
`public inline  `[`CustomerIssue`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a34118e4216dce9d378d7cfa1f60652c4)`(`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tag,`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` customer,`[`TblPlantNames`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames)` plant)` | 

## Members

#### `{property} new DateTime `[`Date`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a521389860174ceea1015b29f213823ae) 

#### `{property} new bool `[`SpecialInstWritten`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a7f5b0305d8c6137ee298c7cd476e83ca) 

#### `{property} new bool `[`QualityAtag`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1af9e536f5b0bf042fb196e8dae8cfaea0) 

#### `{property} new bool `[`HoldTag`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a591d68e121a1312b8833d2be820f3ca6) 

#### `{property} new bool `[`TpcTag`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a7b79386c7b5b261243666dca5856e540) 

#### `{property} new bool `[`QualityMemo`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1af56c2642994d8908862e5dc0ee3cbff7) 

#### `{property} string `[`CustName`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a903501602fc8652e9622e819576e40e3) 

#### `{property} string `[`CustLocation`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a45e181e0d3cf571e6a4ea00377482a01) 

#### `{property} string `[`PlantName`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a474c1f2d7b39c1e01eb3f95e4ba9579a) 

#### `public inline  `[`CustomerIssue`](#classHoldTagSignOff_1_1Models_1_1CustomerIssue_1a34118e4216dce9d378d7cfa1f60652c4)`(`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tag,`[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` customer,`[`TblPlantNames`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames)` plant)` 

# class `HoldTagSignOff::Models::Depts` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`DeptId`](#classHoldTagSignOff_1_1Models_1_1Depts_1a5dcaaf78ca9a98ce7ab5712de26f5bed) | 
`{property} string `[`Department`](#classHoldTagSignOff_1_1Models_1_1Depts_1aa71fa5b8fa6281382fc9c6b7a58bafc5) | 
`{property} string `[`DepartmentState`](#classHoldTagSignOff_1_1Models_1_1Depts_1a54ac155d1cd5aed8398a4148a4b5d08e) | 

## Members

#### `{property} int `[`DeptId`](#classHoldTagSignOff_1_1Models_1_1Depts_1a5dcaaf78ca9a98ce7ab5712de26f5bed) 

#### `{property} string `[`Department`](#classHoldTagSignOff_1_1Models_1_1Depts_1aa71fa5b8fa6281382fc9c6b7a58bafc5) 

#### `{property} string `[`DepartmentState`](#classHoldTagSignOff_1_1Models_1_1Depts_1a54ac155d1cd5aed8398a4148a4b5d08e) 

# class `HoldTagSignOff::Models::Email` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} List< string > `[`RecipientList`](#classHoldTagSignOff_1_1Models_1_1Email_1a0b3d8cb0fff84eb9fa9b039a22796b0d) | 
`{property} List< string > `[`SenderList`](#classHoldTagSignOff_1_1Models_1_1Email_1a5b8562d72934158e80fd9e156e9df3d7) | 
`{property} string `[`Subject`](#classHoldTagSignOff_1_1Models_1_1Email_1ab7143bff747012210f2d2d3d01ef9434) | 
`{property} string `[`Content`](#classHoldTagSignOff_1_1Models_1_1Email_1ace70dd7d7037924451421997925742a1) | 
`public inline  `[`Email`](#classHoldTagSignOff_1_1Models_1_1Email_1afdd1c517c986e1958d3c9aa09b61f959)`()` | 

## Members

#### `{property} List< string > `[`RecipientList`](#classHoldTagSignOff_1_1Models_1_1Email_1a0b3d8cb0fff84eb9fa9b039a22796b0d) 

#### `{property} List< string > `[`SenderList`](#classHoldTagSignOff_1_1Models_1_1Email_1a5b8562d72934158e80fd9e156e9df3d7) 

#### `{property} string `[`Subject`](#classHoldTagSignOff_1_1Models_1_1Email_1ab7143bff747012210f2d2d3d01ef9434) 

#### `{property} string `[`Content`](#classHoldTagSignOff_1_1Models_1_1Email_1ace70dd7d7037924451421997925742a1) 

#### `public inline  `[`Email`](#classHoldTagSignOff_1_1Models_1_1Email_1afdd1c517c986e1958d3c9aa09b61f959)`()` 

# class `HoldTagSignOff::Models::EmailAddress` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} string `[`Name`](#classHoldTagSignOff_1_1Models_1_1EmailAddress_1a5d7cefbf7f597eeb446603fdb76ad40c) | 
`{property} string `[`Address`](#classHoldTagSignOff_1_1Models_1_1EmailAddress_1a8ca5f60bcc95de5e963d9febe773c7a3) | 

## Members

#### `{property} string `[`Name`](#classHoldTagSignOff_1_1Models_1_1EmailAddress_1a5d7cefbf7f597eeb446603fdb76ad40c) 

#### `{property} string `[`Address`](#classHoldTagSignOff_1_1Models_1_1EmailAddress_1a8ca5f60bcc95de5e963d9febe773c7a3) 

# class `HoldTagSignOff::Models::ErrorViewModel` 

Provides a customized View for reporting errors to the user.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} string `[`RequestId`](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel_1ad22e06fe292dfb70825a7d8e378df8da) | Gets or sets the id of the request that was referred to the [ErrorViewModel](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel).
`public bool `[`ShowRequestId`](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel_1ae3b321415df327214ca794258c67afd1) | Returns true if the [RequestId](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel_1ad22e06fe292dfb70825a7d8e378df8da) property of this [ErrorViewModel](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel) is not [. ](#)

## Members

#### `{property} string `[`RequestId`](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel_1ad22e06fe292dfb70825a7d8e378df8da) 

Gets or sets the id of the request that was referred to the [ErrorViewModel](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel).

#### `public bool `[`ShowRequestId`](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel_1ae3b321415df327214ca794258c67afd1) 

Returns true if the [RequestId](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel_1ad22e06fe292dfb70825a7d8e378df8da) property of this [ErrorViewModel](#classHoldTagSignOff_1_1Models_1_1ErrorViewModel) is not [. ](#)

# class `HoldTagSignOff::Models::Machines` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`MachineId`](#classHoldTagSignOff_1_1Models_1_1Machines_1a2365f2245f860d5d49bee16caddfdb61) | 
`{property} int `[`DeptId`](#classHoldTagSignOff_1_1Models_1_1Machines_1ae89a0654f14bbf9ca0e66363b2b0665b) | 
`{property} string `[`MachineDesc`](#classHoldTagSignOff_1_1Models_1_1Machines_1a1ab6a2ce5c570ffeffc58fb090e4c26e) | 
`{property} int `[`PlantId`](#classHoldTagSignOff_1_1Models_1_1Machines_1a060a9cc8f4389fbdfc0c94e72fe13526) | 
`{property} string `[`Stamp`](#classHoldTagSignOff_1_1Models_1_1Machines_1a5ab040919681914039101e6be03b7bdf) | 

## Members

#### `{property} int `[`MachineId`](#classHoldTagSignOff_1_1Models_1_1Machines_1a2365f2245f860d5d49bee16caddfdb61) 

#### `{property} int `[`DeptId`](#classHoldTagSignOff_1_1Models_1_1Machines_1ae89a0654f14bbf9ca0e66363b2b0665b) 

#### `{property} string `[`MachineDesc`](#classHoldTagSignOff_1_1Models_1_1Machines_1a1ab6a2ce5c570ffeffc58fb090e4c26e) 

#### `{property} int `[`PlantId`](#classHoldTagSignOff_1_1Models_1_1Machines_1a060a9cc8f4389fbdfc0c94e72fe13526) 

#### `{property} string `[`Stamp`](#classHoldTagSignOff_1_1Models_1_1Machines_1a5ab040919681914039101e6be03b7bdf) 

# class `HoldTagSignOff::Models::Parts` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1Parts_1aa3dc78848817f3e5aab831f20a192045) | 
`{property} string `[`Description`](#classHoldTagSignOff_1_1Models_1_1Parts_1af4afe5e4f6a41347ac39f7daad7a652b) | 
`{property} bool `[`RptScrap`](#classHoldTagSignOff_1_1Models_1_1Parts_1a1a31507a92d22e2a353d1ddb6b96df6f) | 
`{property} int `[`PlantNumber`](#classHoldTagSignOff_1_1Models_1_1Parts_1a9a301199f0da21a880ff0257587a0395) | 

## Members

#### `{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1Parts_1aa3dc78848817f3e5aab831f20a192045) 

#### `{property} string `[`Description`](#classHoldTagSignOff_1_1Models_1_1Parts_1af4afe5e4f6a41347ac39f7daad7a652b) 

#### `{property} bool `[`RptScrap`](#classHoldTagSignOff_1_1Models_1_1Parts_1a1a31507a92d22e2a353d1ddb6b96df6f) 

#### `{property} int `[`PlantNumber`](#classHoldTagSignOff_1_1Models_1_1Parts_1a9a301199f0da21a880ff0257587a0395) 

# class `HoldTagSignOff::Models::QualityIssuesMetadata` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} string `[`LongId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ad6c40bc7f3480f4890f0de66be1c9be9) | 
`{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a43ba07ef93effbcd4eb3568f1beadf9c) | 
`{property} string `[`Reason`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a615f73c23c64155c2b6489850df739cf) | 
`{property} string `[`IssuedBy`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ada0c665e800c08efd6312ec95a973725) | 
`{property} string `[`Operation`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af8c68b011a189921254d8128334f241a) | 
`{property} int `[`MachineId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1aed8f4fe1b267a2cb641592ae1b8c6a8d) | 
`{property} DateTime `[`Date`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af7ee7a4b233f121e09c0968797030b3f) | 
`{property} DateTime `[`DateIssued`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a3adb1905986aed5838b2a45b3869fc03) | 
`{property} double `[`LengthOfChange`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ae29de8b2b5eab65422da735cf00c16fd) | 
`{property} DateTime `[`ExpiresOn`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ab54ba24afa742e7b1cdd53e18094bb1b) | 
`{property} int `[`CustomerId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a9e183d9d66e5779ba9157d608859fd13) | 
`{property} decimal `[`Cost`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1afcc4db96f8200ca603721524eb6bc541) | 
`{property} int `[`Qty`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af16231b8e571aceb257800ed452f6156) | 
`{property} int `[`NofPiecesQa`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a7fa1536e9ad366d698b5930f27c562ff) | 
`{property} string `[`Body`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a0bc17255cfa51922f93c5582648538fa) | 
`{property} bool `[`QualityAtag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a632cfa1cc280384b5c68148eb0895295) | 
`{property} bool `[`HoldTag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a59cd9715a0c46b30ae3bfe459ae488c7) | 
`{property} bool `[`TpcTag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af8a21fba1dd1357ec8916aabdca90a98) | 
`{property} bool `[`SpecialInstWritten`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a4d7e97728c75cef9b45c1ddadf9bcfe4) | 
`{property} bool `[`QualityMemo`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a1ff654d6713cff8887497030ae6faa09) | 
`{property} bool `[`ModWritten`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ade2d991494b2ff2e390da2bfba3fee09) | 
`{property} bool `[`CertTag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a89dd88065706c6d438a2c52ec6b73497) | 
`{property} bool `[`Closed`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a2d0e5bdf41d95c6c7f870485bd35e982) | 
`{property} string `[`DispositionCustomer`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a44ab1ff82824f66634e0c11c6fcde563) | 
`{property} string `[`DispositionStackpole`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a495aab5cef4ff24f18c4b99422609597) | 
`{property} string `[`CustomerRefNum`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1aa4f79764fc4f1e1e0363dc827df3c7f6) | 
`{property} bool `[`SupplierIssue`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a8216e8343e1cf2054868eb8d84980bd0) | 
`{property} bool `[`ControlPlan`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1aa71f39077a67ed2e2a490fd17c14b478) | 
`{property} bool `[`LayeredAudit`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a827dd5c94a54dd61038fda05cb4c03b1) | 
`{property} string `[`OkdBy`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a3d5769c2bb898de8ec23d9f8c8dc4f9f) | 

## Members

#### `{property} string `[`LongId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ad6c40bc7f3480f4890f0de66be1c9be9) 

#### `{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a43ba07ef93effbcd4eb3568f1beadf9c) 

#### `{property} string `[`Reason`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a615f73c23c64155c2b6489850df739cf) 

#### `{property} string `[`IssuedBy`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ada0c665e800c08efd6312ec95a973725) 

#### `{property} string `[`Operation`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af8c68b011a189921254d8128334f241a) 

#### `{property} int `[`MachineId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1aed8f4fe1b267a2cb641592ae1b8c6a8d) 

#### `{property} DateTime `[`Date`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af7ee7a4b233f121e09c0968797030b3f) 

#### `{property} DateTime `[`DateIssued`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a3adb1905986aed5838b2a45b3869fc03) 

#### `{property} double `[`LengthOfChange`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ae29de8b2b5eab65422da735cf00c16fd) 

#### `{property} DateTime `[`ExpiresOn`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ab54ba24afa742e7b1cdd53e18094bb1b) 

#### `{property} int `[`CustomerId`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a9e183d9d66e5779ba9157d608859fd13) 

#### `{property} decimal `[`Cost`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1afcc4db96f8200ca603721524eb6bc541) 

#### `{property} int `[`Qty`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af16231b8e571aceb257800ed452f6156) 

#### `{property} int `[`NofPiecesQa`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a7fa1536e9ad366d698b5930f27c562ff) 

#### `{property} string `[`Body`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a0bc17255cfa51922f93c5582648538fa) 

#### `{property} bool `[`QualityAtag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a632cfa1cc280384b5c68148eb0895295) 

#### `{property} bool `[`HoldTag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a59cd9715a0c46b30ae3bfe459ae488c7) 

#### `{property} bool `[`TpcTag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1af8a21fba1dd1357ec8916aabdca90a98) 

#### `{property} bool `[`SpecialInstWritten`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a4d7e97728c75cef9b45c1ddadf9bcfe4) 

#### `{property} bool `[`QualityMemo`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a1ff654d6713cff8887497030ae6faa09) 

#### `{property} bool `[`ModWritten`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1ade2d991494b2ff2e390da2bfba3fee09) 

#### `{property} bool `[`CertTag`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a89dd88065706c6d438a2c52ec6b73497) 

#### `{property} bool `[`Closed`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a2d0e5bdf41d95c6c7f870485bd35e982) 

#### `{property} string `[`DispositionCustomer`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a44ab1ff82824f66634e0c11c6fcde563) 

#### `{property} string `[`DispositionStackpole`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a495aab5cef4ff24f18c4b99422609597) 

#### `{property} string `[`CustomerRefNum`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1aa4f79764fc4f1e1e0363dc827df3c7f6) 

#### `{property} bool `[`SupplierIssue`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a8216e8343e1cf2054868eb8d84980bd0) 

#### `{property} bool `[`ControlPlan`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1aa71f39077a67ed2e2a490fd17c14b478) 

#### `{property} bool `[`LayeredAudit`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a827dd5c94a54dd61038fda05cb4c03b1) 

#### `{property} string `[`OkdBy`](#classHoldTagSignOff_1_1Models_1_1QualityIssuesMetadata_1a3d5769c2bb898de8ec23d9f8c8dc4f9f) 

# class `HoldTagSignOff::Models::QualityTagsContext` 

```
class HoldTagSignOff::Models::QualityTagsContext
  : public DbContext
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} DbSet< `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1Attachments)` > `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a148f10d1bbb735c490270500c4611598) | 
`{property} DbSet< `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1Blobs)` > `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a7786c6641f14b0bc5d68d6625ce35653) | 
`{property} DbSet< `[`Depts`](#classHoldTagSignOff_1_1Models_1_1Depts)` > `[`Depts`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a7d1676de1a9a976ec7477d22040295f4) | 
`{property} DbSet< `[`Machines`](#classHoldTagSignOff_1_1Models_1_1Machines)` > `[`Machines`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a0e55cb2865f98359d6d98df9d1ea9cb4) | 
`{property} DbSet< `[`Parts`](#classHoldTagSignOff_1_1Models_1_1Parts)` > `[`Parts`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a7f7013486f67b6624afb66eefddfe966) | 
`{property} DbSet< `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations)` > `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1af6d6d9c521fa8164c1c442bb5e895808) | 
`{property} DbSet< `[`TblCustomerDispositions`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions)` > `[`TblCustomerDispositions`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1af6a337b21a3f2a9b791083a16249a261) | 
`{property} DbSet< `[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` > `[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1ad3fc9a7e1a87d2c0d2a8201abfccde82) | 
`{property} DbSet< `[`TblHtpcReasons`](#classHoldTagSignOff_1_1Models_1_1TblHtpcReasons)` > `[`TblHtpcReasons`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a776cf9ddeff744b6fba084db3138243c) | 
`{property} DbSet< `[`TblPartsCustomer`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer)` > `[`TblPartsCustomer`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a63cc2407e85ffe8e4394f8eb985e5b3f) | 
`{property} DbSet< `[`TblPlantNames`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames)` > `[`TblPlantNames`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a008ffee0afa01458eab5c0cba90b3fa4) | 
`{property} DbSet< `[`TblProblemSource`](#classHoldTagSignOff_1_1Models_1_1TblProblemSource)` > `[`TblProblemSource`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a8cff222040c0612d4d92673a024605c8) | 
`{property} DbSet< `[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` > `[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a8befa84e1980450e3db8d4566309490d) | 
`public inline  `[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a27525cfcf5aa0ebed785cbdf58f3f896)`()` | 
`public inline  `[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a44c7ae1dde51e82c08d2c3d2e500d83d)`(DbContextOptions< `[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` > options)` | 
`protected inline override void `[`OnConfiguring`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1adc44b60411f6434d55f990ecc2c9ff4e)`(DbContextOptionsBuilder optionsBuilder)` | 
`protected inline override void `[`OnModelCreating`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a144c960e389f3789bbaca120f846da74)`(ModelBuilder modelBuilder)` | 

## Members

#### `{property} DbSet< `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1Attachments)` > `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a148f10d1bbb735c490270500c4611598) 

#### `{property} DbSet< `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1Blobs)` > `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a7786c6641f14b0bc5d68d6625ce35653) 

#### `{property} DbSet< `[`Depts`](#classHoldTagSignOff_1_1Models_1_1Depts)` > `[`Depts`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a7d1676de1a9a976ec7477d22040295f4) 

#### `{property} DbSet< `[`Machines`](#classHoldTagSignOff_1_1Models_1_1Machines)` > `[`Machines`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a0e55cb2865f98359d6d98df9d1ea9cb4) 

#### `{property} DbSet< `[`Parts`](#classHoldTagSignOff_1_1Models_1_1Parts)` > `[`Parts`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a7f7013486f67b6624afb66eefddfe966) 

#### `{property} DbSet< `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations)` > `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1af6d6d9c521fa8164c1c442bb5e895808) 

#### `{property} DbSet< `[`TblCustomerDispositions`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions)` > `[`TblCustomerDispositions`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1af6a337b21a3f2a9b791083a16249a261) 

#### `{property} DbSet< `[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1TblCustomers)` > `[`TblCustomers`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1ad3fc9a7e1a87d2c0d2a8201abfccde82) 

#### `{property} DbSet< `[`TblHtpcReasons`](#classHoldTagSignOff_1_1Models_1_1TblHtpcReasons)` > `[`TblHtpcReasons`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a776cf9ddeff744b6fba084db3138243c) 

#### `{property} DbSet< `[`TblPartsCustomer`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer)` > `[`TblPartsCustomer`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a63cc2407e85ffe8e4394f8eb985e5b3f) 

#### `{property} DbSet< `[`TblPlantNames`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames)` > `[`TblPlantNames`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a008ffee0afa01458eab5c0cba90b3fa4) 

#### `{property} DbSet< `[`TblProblemSource`](#classHoldTagSignOff_1_1Models_1_1TblProblemSource)` > `[`TblProblemSource`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a8cff222040c0612d4d92673a024605c8) 

#### `{property} DbSet< `[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` > `[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a8befa84e1980450e3db8d4566309490d) 

#### `public inline  `[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a27525cfcf5aa0ebed785cbdf58f3f896)`()` 

#### `public inline  `[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a44c7ae1dde51e82c08d2c3d2e500d83d)`(DbContextOptions< `[`QualityTagsContext`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext)` > options)` 

#### `protected inline override void `[`OnConfiguring`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1adc44b60411f6434d55f990ecc2c9ff4e)`(DbContextOptionsBuilder optionsBuilder)` 

#### `protected inline override void `[`OnModelCreating`](#classHoldTagSignOff_1_1Models_1_1QualityTagsContext_1a144c960e389f3789bbaca120f846da74)`(ModelBuilder modelBuilder)` 

# class `HoldTagSignOff::Models::StorageLocations` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a609cc473141916a562c51f59979581b2) | 
`{property} string `[`Path`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a6d6569e8d3ca49d60dfef1c216f8c3cd) | 
`{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1aa51e5c6e3fbbde5194fad10caff2463d) | 
`{property} ICollection< `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1Attachments)` > `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a2e77570e65f675616aa7cb71c7b1b765) | 
`{property} ICollection< `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1Blobs)` > `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a524d9ff1b3810ff475b225c1a68021bb) | 
`public inline  `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a20eb362d6a621aaa9b09dc4c13f151c7)`()` | 

## Members

#### `{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a609cc473141916a562c51f59979581b2) 

#### `{property} string `[`Path`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a6d6569e8d3ca49d60dfef1c216f8c3cd) 

#### `{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1aa51e5c6e3fbbde5194fad10caff2463d) 

#### `{property} ICollection< `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1Attachments)` > `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a2e77570e65f675616aa7cb71c7b1b765) 

#### `{property} ICollection< `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1Blobs)` > `[`Blobs`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a524d9ff1b3810ff475b225c1a68021bb) 

#### `public inline  `[`StorageLocations`](#classHoldTagSignOff_1_1Models_1_1StorageLocations_1a20eb362d6a621aaa9b09dc4c13f151c7)`()` 

# class `HoldTagSignOff::Models::TagReport` 

```
class HoldTagSignOff::Models::TagReport
  : public HoldTagSignOff.Models.TblQualityIssues
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} TagTemplate `[`Template`](#classHoldTagSignOff_1_1Models_1_1TagReport_1a805fa22555e06f075344927086c552fa) | 
`{property} HtmlString `[`Title`](#classHoldTagSignOff_1_1Models_1_1TagReport_1a093204b5d07feb3cc87245026fafbf06) | 
`public inline  `[`TagReport`](#classHoldTagSignOff_1_1Models_1_1TagReport_1ae7d8d30fd4a5e4eb8b19bc3621bc2426)`(`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tag)` | 

## Members

#### `{property} TagTemplate `[`Template`](#classHoldTagSignOff_1_1Models_1_1TagReport_1a805fa22555e06f075344927086c552fa) 

#### `{property} HtmlString `[`Title`](#classHoldTagSignOff_1_1Models_1_1TagReport_1a093204b5d07feb3cc87245026fafbf06) 

#### `public inline  `[`TagReport`](#classHoldTagSignOff_1_1Models_1_1TagReport_1ae7d8d30fd4a5e4eb8b19bc3621bc2426)`(`[`TblQualityIssues`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues)` tag)` 

# class `HoldTagSignOff::Models::TblCustomerDispositions` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1a6b99f5feeedbb3de0dcf4c9dc9bb1910) | 
`{property} string `[`Disposition`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1a298c2df0e866aad0a01cd221571407e6) | 
`public readonly int [] `[`STACKPOLE_DISPOSITION_IDS`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1ae0aab0f09ccc0dc9ddc223ce440de298) | 
`public readonly int [] `[`CUSTOMER_DISPOSITION_IDS`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1a75f9faa0577e4d36245d0a703a662cf3) | 

## Members

#### `{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1a6b99f5feeedbb3de0dcf4c9dc9bb1910) 

#### `{property} string `[`Disposition`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1a298c2df0e866aad0a01cd221571407e6) 

#### `public readonly int [] `[`STACKPOLE_DISPOSITION_IDS`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1ae0aab0f09ccc0dc9ddc223ce440de298) 

#### `public readonly int [] `[`CUSTOMER_DISPOSITION_IDS`](#classHoldTagSignOff_1_1Models_1_1TblCustomerDispositions_1a75f9faa0577e4d36245d0a703a662cf3) 

# class `HoldTagSignOff::Models::TblCustomers` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`CustomerId`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a2d443a0062bbc7798df1798b23fa5def) | 
`{property} string `[`CustName`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a0d9432d678f458a90d24c68c7bacad7f) | 
`{property} string `[`CustLocation`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a1a79db78504207b7fd95d47e8f1bbada) | 
`{property} bool `[`Current`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a060ad3d41a6c47f5884fc498cf13e40a) | 

## Members

#### `{property} int `[`CustomerId`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a2d443a0062bbc7798df1798b23fa5def) 

#### `{property} string `[`CustName`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a0d9432d678f458a90d24c68c7bacad7f) 

#### `{property} string `[`CustLocation`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a1a79db78504207b7fd95d47e8f1bbada) 

#### `{property} bool `[`Current`](#classHoldTagSignOff_1_1Models_1_1TblCustomers_1a060ad3d41a6c47f5884fc498cf13e40a) 

# class `HoldTagSignOff::Models::TblHtpcReasons` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1TblHtpcReasons_1a585bf556d66a53197ae87ac9168e7e43) | 
`{property} string `[`Reason`](#classHoldTagSignOff_1_1Models_1_1TblHtpcReasons_1a4bf6ccae1a745c85b6a5647d1b6308ca) | 

## Members

#### `{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1TblHtpcReasons_1a585bf556d66a53197ae87ac9168e7e43) 

#### `{property} string `[`Reason`](#classHoldTagSignOff_1_1Models_1_1TblHtpcReasons_1a4bf6ccae1a745c85b6a5647d1b6308ca) 

# class `HoldTagSignOff::Models::TblPartsCustomer` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`PartCustId`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer_1a4559875064b7625bd6b5aed35ee7c31e) | 
`{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer_1a698372f29c94115b68d7ed061b36a642) | 
`{property} int `[`CustId`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer_1a738d417a0f639a1632d31c2aa6ede7a9) | 

## Members

#### `{property} int `[`PartCustId`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer_1a4559875064b7625bd6b5aed35ee7c31e) 

#### `{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer_1a698372f29c94115b68d7ed061b36a642) 

#### `{property} int `[`CustId`](#classHoldTagSignOff_1_1Models_1_1TblPartsCustomer_1a738d417a0f639a1632d31c2aa6ede7a9) 

# class `HoldTagSignOff::Models::TblPlantNames` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`PlantId`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1a2771440363781d33910a16400041c751) | 
`{property} string `[`PlantName`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1ae6932d287534bdddcb47ed439c3e0566) | 
`{property} string `[`PlantDescrpition`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1ae621f93f0439cb13c160e6872f1bd557) | 
`{property} int `[`Area`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1a397c70a83e003ca5002e77e6980a4218) | 

## Members

#### `{property} int `[`PlantId`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1a2771440363781d33910a16400041c751) 

#### `{property} string `[`PlantName`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1ae6932d287534bdddcb47ed439c3e0566) 

#### `{property} string `[`PlantDescrpition`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1ae621f93f0439cb13c160e6872f1bd557) 

#### `{property} int `[`Area`](#classHoldTagSignOff_1_1Models_1_1TblPlantNames_1a397c70a83e003ca5002e77e6980a4218) 

# class `HoldTagSignOff::Models::TblProblemSource` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} string `[`ProblemSourceCode`](#classHoldTagSignOff_1_1Models_1_1TblProblemSource_1a290d7c3f31ce67b80e9c22447e8a7364) | 
`{property} string `[`ProblemSource`](#classHoldTagSignOff_1_1Models_1_1TblProblemSource_1a34a54b105be604ca5985f456979e8576) | 

## Members

#### `{property} string `[`ProblemSourceCode`](#classHoldTagSignOff_1_1Models_1_1TblProblemSource_1a290d7c3f31ce67b80e9c22447e8a7364) 

#### `{property} string `[`ProblemSource`](#classHoldTagSignOff_1_1Models_1_1TblProblemSource_1a34a54b105be604ca5985f456979e8576) 

# class `HoldTagSignOff::Models::TblQualityIssues` 

```
class HoldTagSignOff::Models::TblQualityIssues
  : public IValidatableObject
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1acab0f0a13fe02babe01fe850389d7c5a) | 
`{property} string `[`ProblemType`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a72cadfe54b1096d0332a016be7fd7865) | 
`{property} int `[`Year`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a96b1cb6203c3554488289f02115e450b) | 
`{property} DateTime `[`Date`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a20d576529cb67bfb4cf8773904a51e7d) | 
`{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a7015f4bca3e65bf002e49df59a475cea) | 
`{property} string `[`IssuedBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a4e98c0cf6f0f9cbc3ea973aa44e95329) | 
`{property} string `[`OpertionHd`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a1e4d1b1f1d0dfda9afc21ab30c7d5ddc) | 
`{property} string `[`OpertionTp`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae036cccb0d158a4b3cc88018aeb2caf9) | 
`{property} string `[`OpertionSp`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a8e42818ad40178a74d055c623cda6635) | 
`{property} string `[`OpertionQa`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a1fc333794c4af8f069d930c6a7bcae59) | 
`{property} string `[`Reason`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1acc2e8d1c5038a0b0ab8dc03a68ce7636) | 
`{property} string `[`ReasonNote`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ad88ce635b454049f17610dc79e744476) | 
`{property} string `[`Feature`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a6ac31dc62563382b57c309d7b933d3ef) | 
`{property} string `[`Changed`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae05a3d45c79a8e0073280bd332057f70) | 
`{property} string `[`Comment`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a95b6faf50b1db369304c3d5196a63c5f) | 
`{property} string `[`SpecialInst`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a3dd1c1c5e28fbdd3a2c33d55453a394a) | 
`{property} string `[`QualityAlert`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ade42c36fcbcd1b2d183357901adc5603) | 
`{property} string `[`QualityAlertMemo`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1afcc6efcc1322077df5d7cf3af7f899cd) | 
`{property} decimal `[`Cost`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a20eed17e92d8cd6d11e939c475bae763) | 
`{property} int `[`NofPiecesQa`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a9023af85b8341161f7f9e38af2952193) | 
`{property} int `[`NofPiecesHt`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1af47a66cb58830ac0c687d0ff4c746790) | 
`{property} int `[`MachineId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a56268595faed19da4ddfd8d959d4b58a) | 
`{property} int `[`CustomerId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1abcb7868a8a97ba28f914738ffd93b258) | 
`{property} string `[`CustomerRefNum`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a89060a0eaa9c0ccfb1701d8970f7006a) | 
`{property} DateTime `[`DateIssued`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab602432357be8e7d6892fd947de41eb4) | 
`{property} string `[`DispositionCustomer`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ad4f0b9372c07c029d0cc05d1c091d85b) | 
`{property} string `[`DispositionStackpole`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a6543dfcff94613a3e804880b51307542) | 
`{property} double `[`LengthOfChange`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab1b0ac56f1e1ceb7ab84bbe1e61bb028) | 
`{property} string `[`OkdBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab0aa74da936e53d4ac4417fbb9320027) | 
`{property} bool `[`ModWritten`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1affbfaabf5fa559508a1b0e9503ebd008) | 
`{property} bool `[`SpecialInstWritten`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a5661a462c23107618c06e4c91b82cd5e) | 
`{property} bool `[`HoldTag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a96ca345f94b4dd18f0b6c169ddded6e2) | 
`{property} bool `[`TpcTag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a4b3a44a228990e4a7f78f13cf0fe5562) | 
`{property} bool `[`CertTag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a2c14212c88403f88a677a1f2a07c7da2) | 
`{property} bool `[`QualityAtag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab29a13391db5da2efbc4e6dab638e21b) | 
`{property} bool `[`Closed`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab9c4e76be8ef930e0bbc34ff6d7dbd88) | 
`{property} bool `[`SupplierIssue`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a0cb0b358798d6e59c94118bc7172b02e) | 
`{property} bool `[`ControlPlan`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1af704947479a9ae4224c2800cc8026497) | 
`{property} bool `[`LayeredAudit`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae7c35abd17dc91a3313bcdbe81bd41d3) | 
`{property} bool `[`QualityMemo`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a1ca6b5a5a2b6481e2fd5c57dd858d9f4) | 
`{property} string `[`FeatureNumber`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1afec03e1263fbdd693ddc2486082249aa) | 
`{property} double `[`CurrentMinSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae4a097ab5c8b83ae6939ec52314afbf6) | 
`{property} double `[`CurrentMaxSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a41a433d8427e2d03c259c69a9fcf8b61) | 
`{property} double `[`TpcMinSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a521b273ec9450c5419c299bc4414e926) | 
`{property} double `[`TpcMaxSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a23877328ae435cc64d32e9f0622ae95e) | 
`{property} string `[`OperationNumber`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a0ae1511c29de020d8ef1c9365d6d8ce4) | 
`{property} string `[`Body`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a2949ffd485c1e5cfe3818f537650627d) | 
`{property} string `[`Operation`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a6bb0ff71bf0b8194e354058a0a7b2a1d) | 
`{property} byte [] `[`Picture`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a24a93dfc4cd97c7e9a0737e5896efe81) | 
`{property} int `[`Picture01`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ac420471d9687c416e3613086890df1d6) | 
`{property} int `[`Picture02`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ac27baf642d0ebad43e037af6a35ad9e5) | 
`{property} int `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a4752f9f9e1ac278348732df9d0728b52) | 
`{property} int `[`Qty`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1af26d56d7652a8cabc6eee4d004194f3a) | 
`{property} int `[`ActiveStatus`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a7991c35836cd6fc43a7424daa2a0e868) | 
`{property} string `[`CreatedBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a2b9971c9602f4ba4e5be56df1017b5b4) | 
`{property} string `[`UpdatedBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a99998a1447569710f824a2f3bd4d19b3) | 
`{property} DateTime `[`UpdatedAt`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1afede99b65c0f28a2d0013db5256248d9) | 
`{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1aaccbab7555b2b432b3bda65c935ceb50) | 
`public string `[`LongId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a481f8835a82b3ddcddac86a8b46eb8de) | 
`public DateTime `[`ExpiresOn`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a67e9eb36c429fcedb869decf9a1577f4) | 
`public bool `[`IsExternalAlert`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a96de80909b3b126cb39b28fb23f62679) | 
`public bool `[`IsInternalAlert`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae531cd81841cc2ea57c4511c976e0397) | 
`public inline IEnumerable< ValidationResult > `[`Validate`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a271ab36b6c9fd5bab8754dd80a9dd24f)`(ValidationContext validationContext)` | 

## Members

#### `{property} int `[`Id`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1acab0f0a13fe02babe01fe850389d7c5a) 

#### `{property} string `[`ProblemType`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a72cadfe54b1096d0332a016be7fd7865) 

#### `{property} int `[`Year`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a96b1cb6203c3554488289f02115e450b) 

#### `{property} DateTime `[`Date`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a20d576529cb67bfb4cf8773904a51e7d) 

#### `{property} string `[`PartId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a7015f4bca3e65bf002e49df59a475cea) 

#### `{property} string `[`IssuedBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a4e98c0cf6f0f9cbc3ea973aa44e95329) 

#### `{property} string `[`OpertionHd`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a1e4d1b1f1d0dfda9afc21ab30c7d5ddc) 

#### `{property} string `[`OpertionTp`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae036cccb0d158a4b3cc88018aeb2caf9) 

#### `{property} string `[`OpertionSp`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a8e42818ad40178a74d055c623cda6635) 

#### `{property} string `[`OpertionQa`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a1fc333794c4af8f069d930c6a7bcae59) 

#### `{property} string `[`Reason`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1acc2e8d1c5038a0b0ab8dc03a68ce7636) 

#### `{property} string `[`ReasonNote`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ad88ce635b454049f17610dc79e744476) 

#### `{property} string `[`Feature`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a6ac31dc62563382b57c309d7b933d3ef) 

#### `{property} string `[`Changed`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae05a3d45c79a8e0073280bd332057f70) 

#### `{property} string `[`Comment`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a95b6faf50b1db369304c3d5196a63c5f) 

#### `{property} string `[`SpecialInst`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a3dd1c1c5e28fbdd3a2c33d55453a394a) 

#### `{property} string `[`QualityAlert`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ade42c36fcbcd1b2d183357901adc5603) 

#### `{property} string `[`QualityAlertMemo`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1afcc6efcc1322077df5d7cf3af7f899cd) 

#### `{property} decimal `[`Cost`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a20eed17e92d8cd6d11e939c475bae763) 

#### `{property} int `[`NofPiecesQa`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a9023af85b8341161f7f9e38af2952193) 

#### `{property} int `[`NofPiecesHt`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1af47a66cb58830ac0c687d0ff4c746790) 

#### `{property} int `[`MachineId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a56268595faed19da4ddfd8d959d4b58a) 

#### `{property} int `[`CustomerId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1abcb7868a8a97ba28f914738ffd93b258) 

#### `{property} string `[`CustomerRefNum`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a89060a0eaa9c0ccfb1701d8970f7006a) 

#### `{property} DateTime `[`DateIssued`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab602432357be8e7d6892fd947de41eb4) 

#### `{property} string `[`DispositionCustomer`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ad4f0b9372c07c029d0cc05d1c091d85b) 

#### `{property} string `[`DispositionStackpole`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a6543dfcff94613a3e804880b51307542) 

#### `{property} double `[`LengthOfChange`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab1b0ac56f1e1ceb7ab84bbe1e61bb028) 

#### `{property} string `[`OkdBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab0aa74da936e53d4ac4417fbb9320027) 

#### `{property} bool `[`ModWritten`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1affbfaabf5fa559508a1b0e9503ebd008) 

#### `{property} bool `[`SpecialInstWritten`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a5661a462c23107618c06e4c91b82cd5e) 

#### `{property} bool `[`HoldTag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a96ca345f94b4dd18f0b6c169ddded6e2) 

#### `{property} bool `[`TpcTag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a4b3a44a228990e4a7f78f13cf0fe5562) 

#### `{property} bool `[`CertTag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a2c14212c88403f88a677a1f2a07c7da2) 

#### `{property} bool `[`QualityAtag`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab29a13391db5da2efbc4e6dab638e21b) 

#### `{property} bool `[`Closed`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ab9c4e76be8ef930e0bbc34ff6d7dbd88) 

#### `{property} bool `[`SupplierIssue`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a0cb0b358798d6e59c94118bc7172b02e) 

#### `{property} bool `[`ControlPlan`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1af704947479a9ae4224c2800cc8026497) 

#### `{property} bool `[`LayeredAudit`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae7c35abd17dc91a3313bcdbe81bd41d3) 

#### `{property} bool `[`QualityMemo`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a1ca6b5a5a2b6481e2fd5c57dd858d9f4) 

#### `{property} string `[`FeatureNumber`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1afec03e1263fbdd693ddc2486082249aa) 

#### `{property} double `[`CurrentMinSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae4a097ab5c8b83ae6939ec52314afbf6) 

#### `{property} double `[`CurrentMaxSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a41a433d8427e2d03c259c69a9fcf8b61) 

#### `{property} double `[`TpcMinSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a521b273ec9450c5419c299bc4414e926) 

#### `{property} double `[`TpcMaxSpec`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a23877328ae435cc64d32e9f0622ae95e) 

#### `{property} string `[`OperationNumber`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a0ae1511c29de020d8ef1c9365d6d8ce4) 

#### `{property} string `[`Body`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a2949ffd485c1e5cfe3818f537650627d) 

#### `{property} string `[`Operation`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a6bb0ff71bf0b8194e354058a0a7b2a1d) 

#### `{property} byte [] `[`Picture`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a24a93dfc4cd97c7e9a0737e5896efe81) 

#### `{property} int `[`Picture01`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ac420471d9687c416e3613086890df1d6) 

#### `{property} int `[`Picture02`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ac27baf642d0ebad43e037af6a35ad9e5) 

#### `{property} int `[`Attachments`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a4752f9f9e1ac278348732df9d0728b52) 

#### `{property} int `[`Qty`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1af26d56d7652a8cabc6eee4d004194f3a) 

#### `{property} int `[`ActiveStatus`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a7991c35836cd6fc43a7424daa2a0e868) 

#### `{property} string `[`CreatedBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a2b9971c9602f4ba4e5be56df1017b5b4) 

#### `{property} string `[`UpdatedBy`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a99998a1447569710f824a2f3bd4d19b3) 

#### `{property} DateTime `[`UpdatedAt`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1afede99b65c0f28a2d0013db5256248d9) 

#### `{property} DateTime `[`CreatedAt`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1aaccbab7555b2b432b3bda65c935ceb50) 

#### `public string `[`LongId`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a481f8835a82b3ddcddac86a8b46eb8de) 

#### `public DateTime `[`ExpiresOn`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a67e9eb36c429fcedb869decf9a1577f4) 

#### `public bool `[`IsExternalAlert`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a96de80909b3b126cb39b28fb23f62679) 

#### `public bool `[`IsInternalAlert`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1ae531cd81841cc2ea57c4511c976e0397) 

#### `public inline IEnumerable< ValidationResult > `[`Validate`](#classHoldTagSignOff_1_1Models_1_1TblQualityIssues_1a271ab36b6c9fd5bab8754dd80a9dd24f)`(ValidationContext validationContext)` 

# class `HoldTagSignOff::Models::UsersContext` 

```
class HoldTagSignOff::Models::UsersContext
  : public IdentityDbContext
```  

Context class for the user storage database.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`UsersContext`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1a7eacba0034b1fbbaed84902cccaba41d)`()` | Instantiates a new [UsersContext](#classHoldTagSignOff_1_1Models_1_1UsersContext).
`public inline  `[`UsersContext`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1a13038296358450eab05b9fc2f8d88184)`(DbContextOptions< `[`UsersContext`](#classHoldTagSignOff_1_1Models_1_1UsersContext)` > options)` | Instantiates a new [UsersContext](#classHoldTagSignOff_1_1Models_1_1UsersContext) from the given DbContextOptions instance.
`protected inline override void `[`OnConfiguring`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1ae3829e5aa54f848f3526cf4d1bf755ee)`(DbContextOptionsBuilder optionsBuilder)` | Override this method to configure the database (and other options) to be used for this context. This method is called for each instance of the context that is created. The base implementation does nothing. 
`protected inline override void `[`OnModelCreating`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1a00660d17d513f2e50a866af0e868f80e)`(ModelBuilder modelBuilder)` | Configures the schemas that will be created by this context.

## Members

#### `public inline  `[`UsersContext`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1a7eacba0034b1fbbaed84902cccaba41d)`()` 

Instantiates a new [UsersContext](#classHoldTagSignOff_1_1Models_1_1UsersContext).

#### `public inline  `[`UsersContext`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1a13038296358450eab05b9fc2f8d88184)`(DbContextOptions< `[`UsersContext`](#classHoldTagSignOff_1_1Models_1_1UsersContext)` > options)` 

Instantiates a new [UsersContext](#classHoldTagSignOff_1_1Models_1_1UsersContext) from the given DbContextOptions instance.

#### Parameters
* `options`

#### `protected inline override void `[`OnConfiguring`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1ae3829e5aa54f848f3526cf4d1bf755ee)`(DbContextOptionsBuilder optionsBuilder)` 

Override this method to configure the database (and other options) to be used for this context. This method is called for each instance of the context that is created. The base implementation does nothing. 

In situations where an instance of DbContextOptions may or may not have been passed to the constructor, you can use DbContextOptionsBuilder.IsConfigured to determine if the options have already been set, and skip some or all of the logic in DbContext.OnConfiguring(DbContextOptionsBuilder). 

#### Parameters
* `optionsBuilder` A builder used to create or modify options for this context.

#### `protected inline override void `[`OnModelCreating`](#classHoldTagSignOff_1_1Models_1_1UsersContext_1a00660d17d513f2e50a866af0e868f80e)`(ModelBuilder modelBuilder)` 

Configures the schemas that will be created by this context.

#### Parameters
* `modelBuilder` The builder being used to construct the model for this context.

# namespace `HoldTagSignOff::Services` 

SmtpConfiguration.cs

Encapsulates configuration settings of an SMTP client.

Revision History Robert Di Pardo, 2020.03.13: Created

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`HoldTagSignOff::Services::EmailService`](#classHoldTagSignOff_1_1Services_1_1EmailService) | 
`class `[`HoldTagSignOff::Services::SmtpConfiguration`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration) | 

# class `HoldTagSignOff::Services::EmailService` 

```
class HoldTagSignOff::Services::EmailService
  : public IEmailSender
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`EmailService`](#classHoldTagSignOff_1_1Services_1_1EmailService_1ad0f3831279e5142e652879ca757c2c67)`(`[`ISmtpConfiguration`](#interfaceHoldTagSignOff_1_1Services_1_1ISmtpConfiguration)` smtpConfiguration)` | 
`public inline Task `[`SendEmailAsync`](#classHoldTagSignOff_1_1Services_1_1EmailService_1ae0a1d7d11a5f1a70406c17837ed5135b)`(string email,string subject,string message)` | 
`protected inline void `[`Send`](#classHoldTagSignOff_1_1Services_1_1EmailService_1a3f176eb34dd0a1a15851fdf7649c4eff)`(`[`Email`](#classHoldTagSignOff_1_1Models_1_1Email)` emailMessage)` | 

## Members

#### `public inline  `[`EmailService`](#classHoldTagSignOff_1_1Services_1_1EmailService_1ad0f3831279e5142e652879ca757c2c67)`(`[`ISmtpConfiguration`](#interfaceHoldTagSignOff_1_1Services_1_1ISmtpConfiguration)` smtpConfiguration)` 

#### `public inline Task `[`SendEmailAsync`](#classHoldTagSignOff_1_1Services_1_1EmailService_1ae0a1d7d11a5f1a70406c17837ed5135b)`(string email,string subject,string message)` 

#### `protected inline void `[`Send`](#classHoldTagSignOff_1_1Services_1_1EmailService_1a3f176eb34dd0a1a15851fdf7649c4eff)`(`[`Email`](#classHoldTagSignOff_1_1Models_1_1Email)` emailMessage)` 

# class `HoldTagSignOff::Services::SmtpConfiguration` 

```
class HoldTagSignOff::Services::SmtpConfiguration
  : public HoldTagSignOff.Services.ISmtpConfiguration
```  

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`{property} string `[`SmtpServer`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1a956779eb384a59fd0e9fdf1b591310be) | 
`{property} int `[`SmtpPort`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1a0bb2a6ab80511502ae841779ac98f30b) | 
`{property} bool `[`UsingSsl`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1a5419fa465d016a3f31d9e1609761f429) | 
`{property} string `[`SmtpUsername`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1ab41f1ffd200d6c8818afc940090f5bfd) | 
`{property} string `[`SmtpPassword`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1abce5b04313dd1a1a1cf431cad5425c81) | 

## Members

#### `{property} string `[`SmtpServer`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1a956779eb384a59fd0e9fdf1b591310be) 

#### `{property} int `[`SmtpPort`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1a0bb2a6ab80511502ae841779ac98f30b) 

#### `{property} bool `[`UsingSsl`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1a5419fa465d016a3f31d9e1609761f429) 

#### `{property} string `[`SmtpUsername`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1ab41f1ffd200d6c8818afc940090f5bfd) 

#### `{property} string `[`SmtpPassword`](#classHoldTagSignOff_1_1Services_1_1SmtpConfiguration_1abce5b04313dd1a1a1cf431cad5425c81) 

Generated by [Moxygen](https://sourcey.com/moxygen)