﻿/**
 * QualityIssuesControllerTest.cs
 * 
 * Unit tests
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.20: Created
 */

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Moq;
using NUnit.Framework;
using HoldTagSignOff.Controllers;
using HoldTagSignOff.Helpers;
using HoldTagSignOff.Models;
using HoldTagSignOff.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HoldTagSignOff.Tests
{
    [TestFixture]
    class QualityIssuesControllerTest
    {
        private QualityIssuesController _controllerUnderTest;
        private readonly int tagId_1 = 1001;
        private readonly int tagId_2 = 1002;

				/// <summary>
        /// Initializes affected models, controllers, services, etc.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            var emailService = new EmailService(
							new Mock<SmtpConfiguration>().Object, 
							new Mock<ILogger<EmailService>>().Object);
            var httpContext = new Mock<HttpContext>();
            var request = new Mock<HttpRequest>();
            var dbContext = new Mock<QualityTagsContext>();
            var tagsData = new MockDbSet<TblQualityIssues>();
            var problemSources = new MockDbSet<TblProblemSource>();
            var partsData = new MockDbSet<Parts>();
            var reasonsData = new MockDbSet<TblHtpcReasons>();
            var processData = new MockDbSet<Depts>();
            var machinesData = new MockDbSet<Machines>();
            var customerData = new MockDbSet<TblCustomers>();
            var plantsData = new MockDbSet<TblPlantNames>();
            var dispositionsData = new MockDbSet<TblCustomerDispositions>();
            var pictureData = new MockDbSet<Blobs>();
            var attachmentData = new MockDbSet<Attachments>();
            var storageLocationData = new MockDbSet<StorageLocations>();

            dbContext.SetupGet(c => c.TblQualityIssues).Returns(tagsData);
            dbContext.SetupGet(c => c.TblProblemSource).Returns(problemSources);
            dbContext.SetupGet(c => c.Parts).Returns(partsData);
            dbContext.SetupGet(c => c.TblHtpcReasons).Returns(reasonsData);
            dbContext.SetupGet(c => c.Depts).Returns(processData);
            dbContext.SetupGet(c => c.Machines).Returns(machinesData);
            dbContext.SetupGet(c => c.TblCustomers).Returns(customerData);
            dbContext.SetupGet(c => c.TblPlantNames).Returns(plantsData);
            dbContext.SetupGet(c => c.TblCustomerDispositions).Returns(dispositionsData);
            dbContext.SetupGet(c => c.Blobs).Returns(pictureData);
            dbContext.SetupGet(c => c.Attachments).Returns(attachmentData);
            dbContext.SetupGet(c => c.StorageLocations).Returns(storageLocationData);

            request.SetupGet(r => r.Method).Returns("POST");
            request.SetupGet(x => x.Headers).Returns(
                new HeaderDictionary { { "X-Requested-With", "XMLHttpRequest" } });
            request.SetupGet(x => x.ContentType).Returns("multipart/form-data");

            var form = new Dictionary<string, StringValues>()
            {
                {"tagCreator", new StringValues(new Guid().ToString()) },
                {"photo01", new StringValues(Path.Combine(Path.GetTempPath(), "dummy.png") )},
                {"photo02", new StringValues(Path.Combine(Path.GetTempPath(), "dummy.jpeg")) }
            };

            request.SetupGet(r => r.Form).Returns(new FormCollection(form));
            httpContext.SetupGet(x => x.Request).Returns(request.Object);

            _controllerUnderTest = new QualityIssuesController(dbContext.Object, emailService)
            {
                TempData = new MockTempDataDictionary(),
                ControllerContext = new ControllerContext()
                {
                    HttpContext = httpContext.Object,

                }
            };

            problemSources.AddRange(new[]
            {
                new TblProblemSource()
                {
                    ProblemSourceCode = "IN",
                    ProblemSource = "Internal"
                },
                new TblProblemSource()
                {
                    ProblemSourceCode = "EX",
                    ProblemSource = "External"
                },
            });

            tagsData.AddRange(new[]
            {
                new TblQualityIssues()
                {
                    Id = tagId_1,
                    ProblemType = "EX",
                    Date = DateTime.Now,
                    IssuedBy = "moi-m\u00CAme",
                    PartId = "11-456",
                    Reason = "none",
                    Operation = "Scrap",
                    CustomerId = 100,
                    QualityAtag = true
                },
                new TblQualityIssues()
                {
                    Id = tagId_2,
                    ProblemType = "EX",
                    Date = DateTime.Now,
                    PartId = "50-0455-FM",
                    Reason = "Missed Operation",
                    Operation = "GP12",
                    IssuedBy = "vous-m\u00CAme",
                    TpcTag = true
                }
            });

            customerData.Add(new TblCustomers()
            {
                CustomerId = 100,
                CustName = "Goeman's",
                CustLocation = "Windsor"
            });

            partsData.Add(new Parts()
            {
                PartId = "11-456",
                PlantNumber = 11,
                Description = "Part 1"
            });


            plantsData.Add(new TblPlantNames()
            {
                PlantId = 11,
                PlantDescrpition = "Big plant"
            });
        }
				
        /// <summary>
        /// this is the test that is performed to show that the index returns the tag list
        /// </summary>
        [Test]
        public void Index_Action_Returns_Tags_List()
        {
            var result = _controllerUnderTest.Index() as ViewResult;

            Assert.IsTrue(result.ViewData["Tags"] is SelectList);
        }

        /// <summary>
        /// this is a test method that checks if the customer issues page returns the view for
        /// customer issues
        /// </summary>
        [Test]
        public void CustomerIssues_Action_Returns_View_Model()
        {
            var result = _controllerUnderTest.CustomerIssues().Result as ViewResult;
            var viewModel = result.ViewData.Model as IOrderedEnumerable<CustomerIssue>;

            Assert.IsTrue(viewModel.OfType<CustomerIssue>().FirstOrDefault() is CustomerIssue);
        }

        /// <summary>
        /// this test id the print button will return the correct tag view
        /// </summary>
        [Test]
        public void Print_Action_Serves_Correct_Tag_View()
        {
            var result = _controllerUnderTest.Print(tagId_1, null).Result as ViewResult;
            var viewModel = (TagReport)result.ViewData.Model;

            Assert.IsTrue(viewModel.Id == tagId_1);
            Assert.IsTrue(viewModel.Template == TagTemplate.QualityAlertExternal);
        }

        /// <summary>
        /// this will test if the the create method wil pull the right attribute from the controller's
        /// get set attribute
        /// </summary>
        [Test]
        public void Create_Action_Responds_To_Get_Method_With_Correct_Model_Type()
        {
            var result = _controllerUnderTest.Create("IN").Result as ViewResult;
            var viewModel = (TblQualityIssues)result.ViewData.Model;

            StringAssert.AreEqualIgnoringCase("IN", viewModel.ProblemType);
        }

        /// <summary>
        /// this will test if the program runs correctly into the catch method if the model state is invalid
        /// </summary>
        [Test]
        public void Create_When_Model_Is_Invalid_Returns_ModelState()
        {
            var tag = new TblQualityIssues() { Id = 1, ProblemType = "IN" };
            _controllerUnderTest.ModelState.AddModelError("PartId", "No part selected!");

            var result = _controllerUnderTest.Create(tag).Result as ViewResult;

            Assert.AreEqual(1, result.ViewData.ModelState.ErrorCount);
        }

        /// <summary>
        /// Test if the create method will return to the index view when the issue is successfully created
        /// </summary>
        [Test]
        public void Create_When_Model_Is_Valid_Redirects_To_Index_View()
        {
            var result = _controllerUnderTest.Create(new TblQualityIssues()).Result as RedirectToActionResult;

            Assert.IsTrue(_controllerUnderTest.ModelState.IsValid);
            StringAssert.AreEqualIgnoringCase("Index", result.ActionName);
        }

        /// <summary>
        /// Test if the edit will get the id from the controller, then redirect the user to print screen
        /// </summary>
        [Test]
        public void Edit_Responds_To_Get_Method_With_Redirect_To_Print()
        {
            var result = _controllerUnderTest.Edit(tagId_2).Result as RedirectToActionResult;

            StringAssert.AreEqualIgnoringCase("Print", result.ActionName);
            Assert.AreEqual(tagId_2, result.RouteValues["id"]);
        }

        /// <summary>
        /// Test if the edit method will not continue to work if the model is invalid
        /// </summary>
        [Test]
        public void Edit_When_Model_Is_Invalid_Returns_ModelState()
        {
            string returnUrl = "/QualityIssues";
            var tagDb = new MockDbSet<TblQualityIssues>();
            tagDb.Add(new TblQualityIssues() { Id = 999, ProblemType = "EX" });

            _controllerUnderTest.ModelState.AddModelError("IssuedBy", "'Issued by is empty!'");
            _controllerUnderTest.ModelState.AddModelError("Operation", "No operation selected!");

            var result = _controllerUnderTest.Edit(999, returnUrl, tagDb.Find(999)).Result as ViewResult;
            var errors = result.ViewData.ModelState.ErrorCount;

            Assert.AreEqual(2, errors);
        }
				
        /// <summary>
        /// Test if a successful edit model state is valid and will continue to run through the code properly
        /// </summary>
        [Test]
        public void Edit_When_Model_Is_Valid_Returns_ModelState()
        {
            string returnUrl = "/QualityIssues";

            var result = _controllerUnderTest.Edit(tagId_1, returnUrl, new TblQualityIssues()).Result as RedirectToActionResult;

            Assert.IsTrue(_controllerUnderTest.ModelState.IsValid);
            StringAssert.AreEqualIgnoringCase("Index", result.ActionName);
        }
    }
}
