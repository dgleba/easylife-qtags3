﻿/**
 * PartsCustomersControllerTest.cs
 * 
 * Unit tests
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.20: Created
 */

using Moq;
using NUnit.Framework;
using HoldTagSignOff.Controllers;
using HoldTagSignOff.Models;


namespace HoldTagSignOff.Tests
{
    [TestFixture]
    class PartsCustomersControllerTest
    {
        private Mock<QualityTagsContext> _context;
				
				/// <summary>
        /// Initializes affected tables so there's at least one record to select
        /// </summary>
        [SetUp]
        public void Setup()
        {
            _context = new Mock<QualityTagsContext>();
            var dummyData = new MockDbSet<TblPartsCustomer>();
            dummyData.Add(new TblPartsCustomer() { PartCustId = 1, CustId = 24, PartId = "12-345" });
            _context.SetupGet(c => c.TblPartsCustomer).Returns(dummyData);
        }

        /// <summary>
        /// Test if Parts for controller will respond to post method with JSON response
        /// </summary>
        [Test]
        public void GetPartsForCustomer_Responds_To_Post_Method_With_Json()
        {
            var customer = new TblCustomers() { CustomerId = 24, CustName = "Nobody", CustLocation = "Nowhere" };
            var testController = new PartsCustomersController(_context.Object);
            var result = testController.GetPartsForCustomer(customer).Result;

            Assert.IsNotNull(result.Value);
            StringAssert.AreEqualIgnoringCase("application/json; charset=utf-8", result.ContentType);
        }
    }
}
