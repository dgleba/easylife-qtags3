﻿/**
 * DummyTempDataDictionary.cs
 * 
 * Ad hoc fill-in for a real TempDataDictionary: see https://docs.microsoft.com/en-us/dotnet/api/system.web.mvc.tempdatadictionary?view=aspnet-mvc-5.2
 *
 * Revision History
 *     Robert Di Pardo, 2020.03.21: Created
 */

using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace HoldTagSignOff.Tests
{
    /// <summary>
    ///  Provides in-memory serialization of assorted View data. 
    /// </summary>
    class MockTempDataDictionary :
        ITempDataDictionary,
        ICollection<KeyValuePair<string, object>>,
        IDictionary<string, object>,
        IEnumerable<KeyValuePair<string, object>>
    {
        private readonly Dictionary<string, object> _entries;

        public MockTempDataDictionary()
        {
            _entries = new Dictionary<string, object>();
        }
        public object this[string key]
        {
            get => _entries.TryGetValue(key, out object val) ? val : null;
            set => _entries.TryAdd(key, value);
        }
        public int Count => _entries.Count;

        public bool IsReadOnly => false;

        public ICollection<string> Keys => _entries.Keys;

        public ICollection<object> Values => _entries.Values;
        public void Add(KeyValuePair<string, object> item)
        {
            if (_entries.TryAdd(item.Key, item.Value))
            {
                _entries.Add(item.Key, item.Value);
            }
        }
        public void Add(string key, object value)
        {
            if (_entries.TryAdd(key, value))
            {
                _entries.Add(key, value);
            }
        }

        public void Clear() => _entries.Clear();

        public bool Contains(KeyValuePair<string, object> item) => _entries.ContainsKey(item.Key) && _entries.ContainsValue(item.Value);


        public bool ContainsKey(string key) => _entries.ContainsKey(key);

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex) => _entries.Values.CopyTo(array.Select(e => e.Value).ToArray(), arrayIndex);

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator() => _entries.AsEnumerable().GetEnumerator();

        public bool Remove(KeyValuePair<string, object> item)
        {
            if (_entries.ContainsKey(item.Key))
            {
                return _entries.Remove(item.Key);
            }
            return false;
        }

        public bool Remove(string key)
        {
            if (_entries.ContainsKey(key))
            {
                return _entries.Remove(key);
            }
            return false;
        }

        public bool TryGetValue(string key, [MaybeNullWhen(false)] out object value) => _entries.TryGetValue(key, out value);

        IEnumerator IEnumerable.GetEnumerator() => _entries.GetEnumerator();

        /**
         * TODO: implement these 
         */
        public void Keep()
        {
            throw new NotImplementedException();
        }

        public void Keep(string key)
        {
            throw new NotImplementedException();
        }

        public void Load()
        {
            throw new NotImplementedException();
        }

        public object Peek(string key)
        {
            throw new NotImplementedException();
        }
        public void Save()
        {
            throw new NotImplementedException();
        }
    }
}
